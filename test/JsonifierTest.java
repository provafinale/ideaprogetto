import Game.Actions.EngageHelper;
import Game.Actions.GenericAction;
import Game.GameCreator;
import Miscellaneous.Jsonifier;
import Server.Comunicazione.Signal;
import org.junit.Test;

/**
 * Created by edoar on 31/05/2016.
 */
public class JsonifierTest {
    @Test
    public void serialize() throws Exception {

        GenericAction action = new EngageHelper();
        System.out.println(action);
        Jsonifier.serialize(action);

    }

    @Test
    public void deserializeSignal() throws Exception {
        GameCreator gameCreator = new GameCreator();
        Signal signal = new Signal("SALUTI", Jsonifier.serialize(gameCreator));
        System.out.println(signal);
    }

    @Test
    public void deserializePayload() throws Exception {

        Signal signal = new Signal(Signal.IS_ACTION, Jsonifier.serialize(new EngageHelper()));
        GenericAction action = (GenericAction) Jsonifier.deserializePayload(signal);
        System.out.println(action.getClass());
        System.out.println(action);
    }

    @Test
    public void AdapterTest() throws Exception {
        String serialized = Jsonifier.serialize(new EngageHelper());
        Signal signal = new Signal(Signal.IS_ACTION, serialized);
        System.out.println("Signal created: "+signal);
        System.out.println("starting deserialize...");
        GenericAction genericAction = (GenericAction) Jsonifier.deserializePayload(signal);
        System.out.println(genericAction);

    }
}