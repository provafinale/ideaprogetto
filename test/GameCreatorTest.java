import Game.Bank;
import Game.Cards.BuildingDeck;
import Game.Cards.PoliticCard;
import Game.Game;
import Game.GameCreator;
import Game.Places.GameMap;
import Game.Places.Region;
import Game.Stuff.Councilor;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by edoar on 25/05/2016.
 */
public class GameCreatorTest {


    private GameCreator gameCreator = new GameCreator();
    private Game game;



    @Test
    public void generateAllBuildingDeck(){
        GameMap map= gameCreator.generateMap();
        ArrayList<Region> regions = map.getRegions();
        HashMap<String, BuildingDeck> allBuildingDeck = gameCreator.generateAllBuildingDeck(regions);
        System.out.println(allBuildingDeck);
    }
    @Test
    public void generateBank() throws Exception {
        Bank bank = gameCreator.generateBank();
        System.out.println(bank.toString());

        for(Map.Entry<String, List<Councilor>> entry : bank.getCouncilors().entrySet()) { //PRINT COUNSILORS
            String key = entry.getKey();
            List<Councilor> value = entry.getValue();
            System.out.println(key);
            System.out.println(value.size());
            for (Councilor councilor: value) {
                System.out.println("Value:"+councilor);
            }
        }

        }

    @Test   //TESTATO E FUNZIONANTE
    public void createPoliticDeck() throws Exception {
        ArrayList<PoliticCard> politicCards;
        politicCards = gameCreator.createPoliticDeck();
        System.out.println("politicCards: "+politicCards);
        System.out.println(politicCards.size());
    }

    @Test
    public void createPaws() throws Exception {

    }

    @Test
    public void generateMap() throws Exception {
    }

    @Test
    public void testEquals() throws Exception {



    }

}