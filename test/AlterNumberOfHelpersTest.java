import Game.GameLogic;
import Game.User;
import Miscellaneous.Exceptions.PoorUserException;
import Server.ServerMatch;
import org.junit.Test;

/**
 * Created by Samuele on 02/06/2016.
 */
public class AlterNumberOfHelpersTest {


    @Test public void testAlterHelpers(){
        ServerMatch serverMatch = new ServerMatch();
        GameLogic gameLogic = serverMatch.getGameLogic();
        User user = new User("UserTest");
        user.setHelpers(48);
        System.out.println("****PRIMA DEL TEST****");
        System.out.println("Helpers User: "+user.getHelpers());
        System.out.println("Helpers Bank: "+gameLogic.getBank().getNumberOfHelper());


        try {
            gameLogic.alterNumberOfHelpers(-3,false,user);
        } catch (PoorUserException e) {
            e.printStackTrace();
        }


        System.out.println("****DOPO IL TEST****");
        System.out.println("Helpers User: "+user.getHelpers());
        System.out.println("Helpers Bank: "+gameLogic.getBank().getNumberOfHelper());



    }
}

