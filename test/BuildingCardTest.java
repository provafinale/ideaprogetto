import Game.Cards.BuildingCard;
import Game.GameCreator;
import Game.Places.City;
import Game.Places.GameMap;
import Game.Places.Region;
import Miscellaneous.Randomizer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;

/**
 * Created by edoar on 26/05/2016.
 */
public class BuildingCardTest {

    @Test public void ConstructorTest() {
        for (int i = 0; i < 50; i++) {
            BuildingCard buildingCard = new BuildingCard();
            //System.out.println(buildingCard);
        }
    }

    @Test public void Rainbow(){
      //  PoliticCard politicCard = new PoliticCard(ColorPoliticCard.RAINBOW);
      //  System.out.println(politicCard.getColor());
    }

    @Test public void Random() {
        for (int i = 0; i < 10000000; i++) {
            int num = Randomizer.randomNumberOFRewards();
            assertFalse(num>2);
        }
    }
    @Test public void CityTest(){
        GameCreator gameCreator = new GameCreator();
        GameMap map = gameCreator.generateMap();
        ArrayList<Region> regions;
        regions = map.getRegions();
        ArrayList<City> allcities = null;
        for (Region region: regions
             ) {
            HashMap<String, City> cities;
                cities=region.getCities();
            for (City city: cities.values()
                 ) {
                assertFalse(city.getRewards().isEmpty());
            }

        }
    }
}