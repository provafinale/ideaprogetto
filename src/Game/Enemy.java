package Game;

import Game.Cards.BuildingCard;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by edoar on 18/05/2016.
 */
public class Enemy implements Serializable{ //E' una versione più sicura e con meno campi dello USer. Da inviare nello snapshot di reazione alla Action.

    private String userName;
    private int numHelpers;
    private int numPoliticCards;
    private ArrayList<BuildingCard> enemyBuildingCard; //non richiedono offuscamenti di sicureza, sono scoperte.
    private int posVictoryPawn;
    private int posNobilityPawn;
    private int posGoldPawn;

    public Enemy(String userName, int numHelpers, int numPoliticCards, ArrayList<BuildingCard> enemyBuildingCard, int posVictoryPawn, int posNobilityPawn, int posGoldPawn) {
        this.userName = userName;
        this.numHelpers = numHelpers;
        this.numPoliticCards = numPoliticCards;
        this.enemyBuildingCard = enemyBuildingCard;
        this.posVictoryPawn = posVictoryPawn;
        this.posNobilityPawn = posNobilityPawn;
        this.posGoldPawn = posGoldPawn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getNumHelpers() {
        return numHelpers;
    }

    public void setNumHelpers(int numHelpers) {
        this.numHelpers = numHelpers;
    }

    public int getNumPoliticCards() {
        return numPoliticCards;
    }

    public void setNumPoliticCards(int numPoliticCards) {
        this.numPoliticCards = numPoliticCards;
    }

    public ArrayList<BuildingCard> getEnemyBuildingCard() {
        return enemyBuildingCard;
    }

    public void setEnemyBuildingCard(ArrayList<BuildingCard> enemyBuildingCard) {
        this.enemyBuildingCard = enemyBuildingCard;
    }

    public int getPosVictoryPawn() {
        return posVictoryPawn;
    }

    public void setPosVictoryPawn(int posVictoryPawn) {
        this.posVictoryPawn = posVictoryPawn;
    }

    public int getPosNobilityPawn() {
        return posNobilityPawn;
    }

    public void setPosNobilityPawn(int posNobilityPawn) {
        this.posNobilityPawn = posNobilityPawn;
    }

    public int getPosGoldPawn() {
        return posGoldPawn;
    }

    public void setPosGoldPawn(int posGoldPawn) {
        this.posGoldPawn = posGoldPawn;
    }

    @Override
    public String toString() {
        return "Enemy{" +
                "userName='" + userName + '\'' +
                ", numHelpers=" + numHelpers +
                ", numPoliticCards=" + numPoliticCards +
                ", enemyBuildingCard=" + enemyBuildingCard +
                ", posVictoryPawn=" + posVictoryPawn +
                ", posNobilityPawn=" + posNobilityPawn +
                ", posGoldPawn=" + posGoldPawn +
                '}';
    }
}
