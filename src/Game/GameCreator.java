package Game;

import Game.Cards.BuildingCard;
import Game.Cards.BuildingDeck;
import Game.Cards.ColorPoliticCard;
import Game.Cards.PoliticCard;
import Game.Pawn.NobilityPawn;
import Game.Places.Balcony.KingBalcony;
import Game.Places.Balcony.RegionBalcony;
import Game.Places.*;
import Game.Places.Tracks.GoldTrack;
import Game.Places.Tracks.NobilityTrack;
import Game.Places.Tracks.VictoryTrack;
import Game.Recompenses.BonusCards.BonusCard;
import Game.Recompenses.BonusCards.CityBonusCard;
import Game.Recompenses.BonusCards.RegionBonusCard;
import Game.Recompenses.Rewards.Reward;
import Game.Stuff.Councilor;
import Game.Stuff.CouncilorColor;
import Game.Stuff.Empory;
import Miscellaneous.Constant;
import Miscellaneous.Randomizer;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by Samuele on 24/05/2016.
 */

/**Metodi che creano gli oggetti di gioco.*/
public class GameCreator {

    /**____BANCA____*/

    /**Creo e inserisco i councilors, helpers, empories nella banca*/
    public Bank generateBank(){
        HashMap councilors = createCouncilors();
        int helpers = Constant.NUM_HELPERS;
        HashMap empories = createEmpories();

        return new Bank(councilors,helpers,empories);
    }

    /** Genera HashMap<Colore,Consigliere> dei Consiglieri */
    private HashMap<String,List<Councilor>> createCouncilors (){
        HashMap<String,List<Councilor>> councilors = new HashMap<>();
        List<Councilor> userCouncilors = new ArrayList<>();

        for (CouncilorColor color: (CouncilorColor.values())) {
            for (int i=0; i<4; i++){
                userCouncilors.add(new Councilor(color));
            }
            List<Councilor> myCouncilors = userCouncilors;
            councilors.put(color.name(),myCouncilors);
            userCouncilors = new ArrayList<>();
        }
        return councilors; //ritorna i consiglieri generati
    }

    /**Genera HashMap<Colore giocatore,Emporio>  degli Empori*/
    private HashMap<String,List<Empory>> createEmpories(){
        HashMap<String,List<Empory>> empories = new HashMap<>();
        List<Empory> userEmpories = new ArrayList<>();

        for (UserColor color: (UserColor.values())){
            for (int i=0; i<10; i++){
                userEmpories.add(new Empory(color,null));
            }
            empories.put(color.name(),userEmpories); //Creo 10 empori per ogni colore, con città a null.
            userEmpories = new ArrayList<>();
        }
        return empories;
    }




    /**____CARTE____*/

    /**Genera le Carte Politiche*/
    public ArrayList<PoliticCard> createPoliticDeck (){
        ArrayList<PoliticCard> politicDeck = new ArrayList<>();
        for(ColorPoliticCard color: (ColorPoliticCard.values())){
            for (int i=0; i<13; i++){
                politicDeck.add(new PoliticCard(color, i));
            }
        }
        politicDeck.remove(politicDeck.size()-1); //Rimuovo l'ultima carta Rainbow dato che sono 12.
        Collections.shuffle(politicDeck);
        return politicDeck;
    }


    /**Genera HashMap<Colore,Pedina> di Pedine*/
    public HashMap<String,List<NobilityPawn>> createPaws(){
        HashMap<String,List<NobilityPawn>> paws = new HashMap<>();
        List<NobilityPawn> userPawns = new ArrayList<>();

        for (UserColor color : (UserColor.values())){
            for (int i=0; i<3; i++){
                userPawns.add(new NobilityPawn(color)); //Creo Pedina a cui associo il colore.
            }
            paws.put(color.name(),userPawns);
            userPawns = new ArrayList<>();
        }
        return paws;
    }


    /**GENERO BONUS*/
    private ArrayList<Reward> createCityBonus(){
        ArrayList<Reward> rewards = new ArrayList<>();
        int numOfReward = Randomizer.randomNumberOFRewards(); //return 1 o 2
        for (int i=0; i<numOfReward; i++){
            Reward reward = Reward.returnRandomTypeOfReward();
            rewards.add(reward);
        }
        return rewards;
    }


    /**Genero oggetti di gioco **/
    public GameMap generateMap(){
        VictoryTrack victoryTrack = new VictoryTrack();
        GoldTrack goldTrack = new GoldTrack();
        NobilityTrack nobilityTrack = new NobilityTrack();
        ArrayList<Region> regions = generateRegions();
        KingGangplank kingGangPlant = generateKingGangPlant();
        return new GameMap(regions,kingGangPlant,victoryTrack,nobilityTrack,goldTrack);
    }


    private KingGangplank generateKingGangPlant(){
        KingGangplank kingGangPlant;
        KingBalcony kingBalcony = new KingBalcony();
        Stack<BonusCard> bonusCards = generateBonusCard();
        ArrayList<CityBonusCard> citiesBonusCard = generateCityBonusCard();

        kingGangPlant = new KingGangplank(kingBalcony,citiesBonusCard,bonusCards); //TODO: IN ATTESA, USO QUESTO COSTRUTTORE PIù EASY. Manca la costruzione delle bonus card!!!
        return kingGangPlant;
    }

    private ArrayList<CityBonusCard> generateCityBonusCard(){
        ArrayList<CityBonusCard> cityBonusCards = new ArrayList<>();
        for (CityColor color:CityColor.values()) {
            cityBonusCards.add(new CityBonusCard(color));
        }
        cityBonusCards.remove(cityBonusCards.size()-1); //Rimuovo la bonus card della città viola.
        return cityBonusCards;
    }

    private Stack<BonusCard> generateBonusCard(){
        Stack<BonusCard> bonusCards = new Stack<>();
        bonusCards.push(new BonusCard(Constant.FIFTH__KING_BONUS_CARD));
        bonusCards.push(new BonusCard(Constant.FOURTH__KING_BONUS_CARD));
        bonusCards.push(new BonusCard(Constant.THIRD__KING_BONUS_CARD));
        bonusCards.push(new BonusCard(Constant.SECOND_KING_BONUS_CARD));
        bonusCards.push(new BonusCard(Constant.FIRST_KING_BONUS_CARD));
        return bonusCards;
    }

    /**Ogni Region ha come costruttore: il nome, le città, la regionBonusCard e il balcony */
    private ArrayList<Region> generateRegions(){
        ArrayList<Region> regions = new ArrayList<> ();
        Stack<City> cities = generateCities(); //Get delle 15 città che andranno assegnate alle regioni

        for (RegionName regionName : RegionName.values()) {
            //Informazioni della regione: Città, RegionBonusCard e RegionBalcony.
            HashMap<String,City> regionCity = getRegionCities(cities);
            RegionBonusCard regionBonusCard = new RegionBonusCard();
            RegionBalcony regionBalcony = new RegionBalcony();

            Region newRegion = new Region(regionName,regionCity,regionBonusCard,regionBalcony);
            regionBalcony.setRegion(newRegion);
            regionBalcony.setRegionName(regionName.name());
            regionBonusCard.setRegion(newRegion);

            regions.add(newRegion);
        }

        return regions;
    }

    /**Come argomento ha l'insieme delle 15 città generate. Trova quante città vanno assegnate per regione
      e col metodo pop le toglie dallo stack, e le aggiunge all'array delle città di ogni singola regione.*/
    private HashMap<String,City> getRegionCities(Stack<City> cities){
        HashMap<String,City> regionCities = new HashMap<>();
        int numOfCitiesRegion = Constant.TOT_NUM_CITY/Constant.NUM_REGION; //15/3 = 5 città a regione.

        for (int i = 0; i<numOfCitiesRegion; i++){
            City city = cities.pop();
            String nameOfCity = city.getName().name();
            regionCities.put(nameOfCity,city);
        }
        return regionCities;
    }

    /**Genera le città, con relativo Bonus.*/
    private Stack<City> generateCities(){
        Stack<City> cities = new Stack<>();
        for (CitiesName cityname: CitiesName.values()) {
            ArrayList<Reward> cityBonus = createCityBonus();
            City newCity = new City(cityname,cityBonus);
            cities.push(newCity);
        }
        return cities;
    }

    public HashMap<String,BuildingDeck> generateAllBuildingDeck(ArrayList<Region> regions){
        HashMap<String,BuildingDeck> regBuildingDeck = new HashMap<>();
        for (Region region : regions) {
            BuildingDeck buildingDeck = generateOneBuildingDeck(region);
            regBuildingDeck.put(region.getRegionName(),buildingDeck);
        }
        return regBuildingDeck;
    }

    private BuildingDeck generateOneBuildingDeck (Region region){
        BuildingDeck buildingDeck;
        Stack<BuildingCard> buildingCards = new Stack<>();
        switch (region.getRegionName()){
            case "BEACH": {
                City arkon = region.getCityFromName(CitiesName.ARKON.name());
                City burgen = region.getCityFromName(CitiesName.BURGEN.name());
                City castrum = region.getCityFromName(CitiesName.CASTRUM.name());
                City dorful = region.getCityFromName(CitiesName.DORFUL.name());
                City esti = region.getCityFromName(CitiesName.ESTI.name());

                buildingCards.push(new BuildingCard(arkon));
                buildingCards.push(new BuildingCard(arkon,esti));
                buildingCards.push(new BuildingCard(esti));
                buildingCards.push(new BuildingCard(castrum));
                buildingCards.push(new BuildingCard(castrum,dorful,esti));
                buildingCards.push(new BuildingCard(burgen));
                buildingCards.push(new BuildingCard(dorful));
                buildingCards.push(new BuildingCard(castrum,dorful));
                buildingCards.push(new BuildingCard(burgen,castrum));
                buildingCards.push(new BuildingCard(arkon,burgen));
                buildingCards.push(new BuildingCard(arkon,burgen,esti));
                buildingCards.push(new BuildingCard(arkon,dorful,esti));
                buildingCards.push(new BuildingCard(dorful,esti));
                buildingCards.push(new BuildingCard(burgen,castrum,dorful));
                buildingCards.push(new BuildingCard(arkon,burgen,castrum));
            }break;
            case "HILL":{
                City framek = region.getCityFromName(CitiesName.FRAMEK.name());
                City indur = region.getCityFromName(CitiesName.INDUR.name());
                City graden = region.getCityFromName(CitiesName.GRADEN.name());
                City juvelar = region.getCityFromName(CitiesName.JUVELAR.name());
                City hellar = region.getCityFromName(CitiesName.HELLAR.name());

                buildingCards.push(new BuildingCard(indur,juvelar));
                buildingCards.push(new BuildingCard(framek));
                buildingCards.push(new BuildingCard(framek,graden));
                buildingCards.push(new BuildingCard(hellar));
                buildingCards.push(new BuildingCard(framek,juvelar));
                buildingCards.push(new BuildingCard(framek,graden,hellar));
                buildingCards.push(new BuildingCard(framek,indur,juvelar));
                buildingCards.push(new BuildingCard(graden,hellar,indur));
                buildingCards.push(new BuildingCard(hellar,graden));
                buildingCards.push(new BuildingCard(indur,juvelar,hellar));
                buildingCards.push(new BuildingCard(framek,graden,juvelar));
                buildingCards.push(new BuildingCard(indur));
                buildingCards.push(new BuildingCard(hellar,indur));
                buildingCards.push(new BuildingCard(juvelar));
                buildingCards.push(new BuildingCard(graden));
            }break;
            case "MOUNTAIN":{

                City kultos = region.getCityFromName(CitiesName.KULTOS.name());
                City naris = region.getCityFromName(CitiesName.NARIS.name());
                City lyram = region.getCityFromName(CitiesName.LYRAM.name());
                City osium = region.getCityFromName(CitiesName.OSIUM.name());
                City merkatim = region.getCityFromName(CitiesName.MERKATIM.name());

                buildingCards.push(new BuildingCard(merkatim));
                buildingCards.push(new BuildingCard(kultos,naris,osium));
                buildingCards.push(new BuildingCard(lyram));
                buildingCards.push(new BuildingCard(naris));
                buildingCards.push(new BuildingCard(kultos,lyram,osium));
                buildingCards.push(new BuildingCard(kultos,lyram,merkatim));
                buildingCards.push(new BuildingCard(osium,kultos));
                buildingCards.push(new BuildingCard(osium));
                buildingCards.push(new BuildingCard(kultos,lyram));
                buildingCards.push(new BuildingCard(kultos));
                buildingCards.push(new BuildingCard(merkatim,naris));
                buildingCards.push(new BuildingCard(merkatim,lyram));
                buildingCards.push(new BuildingCard(naris,osium));
                buildingCards.push(new BuildingCard(merkatim,naris,osium));
                buildingCards.push(new BuildingCard(lyram,merkatim,naris));
            }
        }
        Collections.shuffle(buildingCards);
        Queue<BuildingCard> buildingCardQueue = new ArrayBlockingQueue<BuildingCard>(15);
        buildingCardQueue.addAll(buildingCards);
        buildingDeck = new BuildingDeck(buildingCardQueue,region);
        return buildingDeck;
    }




}
