package Game.Places;

import java.io.Serializable;

/**
 * Created by Samuele on 24/05/2016.
 */
public enum RegionName implements Serializable {
    MOUNTAIN,
    HILL,
    BEACH;
}
