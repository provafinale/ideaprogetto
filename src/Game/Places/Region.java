package Game.Places;

import Game.Places.Balcony.RegionBalcony;
import Game.Recompenses.BonusCards.RegionBonusCard;
import Game.Stuff.Councilor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Queue;

/**
 * Created by edoar on 13/05/2016.
 */
/** Una delle tre regioni */
public class Region implements Serializable {
    private RegionName regionName;
    private HashMap<String,City> cities;
    private RegionBalcony balcony;
    private RegionBonusCard bonusCard;

    public Region(RegionName regionName, HashMap<String,City> cities,RegionBonusCard bonusCard, RegionBalcony balcony) {
        this.regionName = regionName;
        this.cities = cities;
        this.bonusCard = bonusCard;
        this.balcony = balcony;
    }

    public RegionBalcony getBalcony() {
        return balcony;
    }
    public void setBalconyCouncilors(Queue<Councilor> councilors){
        balcony.setBalconyConcilors(councilors);
    }

    public HashMap<String,City> getCities() {
        return cities;
    }

    public City getCityFromName(String cityName){
        return  cities.get(cityName);
    }

    public String getRegionName(){
        return regionName.name();
    }

    @Override
    public String toString() {
        return "Region{" +
                "regionName=" + regionName +
                ", cities=" + cities +
                ", balcony=" + balcony +
                ", bonusCard=" + bonusCard +
                '}';
    }

    public String print(){
        return "Regione: " +regionName + " " +
                "città: " +cities +
                ", balcone: " +balcony.print() +
                " , carta bonus: " + bonusCard +"\n";
    }
}
