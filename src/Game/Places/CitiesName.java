package Game.Places;

import java.io.Serializable;

/**
 * Created by Samuele on 24/05/2016.
 */
public enum CitiesName implements Serializable {
    ARKON,
    CASTRUM,
    BURGEN,
    DORFUL,
    ESTI,

    FRAMEK,
    INDUR,
    GRADEN,
    JUVELAR,
    HELLAR,

    KULTOS,
    NARIS,
    LYRAM,
    OSIUM,
    MERKATIM;
}
