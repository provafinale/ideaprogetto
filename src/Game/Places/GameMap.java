package Game.Places;

import Game.Places.Balcony.Balcony;
import Game.Places.Tracks.GoldTrack;
import Game.Places.Tracks.NobilityTrack;
import Game.Places.Tracks.VictoryTrack;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edoar on 13/05/2016.
 */
/** Contiene regioni, percorsi e plancia del re*/
public class GameMap implements Serializable{
    //Ogni mappa possiede 3 Regioni e 1 Plancia del re
    private ArrayList<Region> regions;
    private KingGangplank kingGangplank;

    //TRACKS
    private VictoryTrack victoryTrack;
    private NobilityTrack nobilityTrack;
    private GoldTrack goldTrack;


    public GameMap(ArrayList<Region> regions, KingGangplank kingGangplank, VictoryTrack victoryTrack,
                   NobilityTrack nobilityTrack, GoldTrack goldTrack) {
        this.regions = regions;
        this.kingGangplank = kingGangplank;
        this.victoryTrack = victoryTrack;
        this.nobilityTrack = nobilityTrack;
        this.goldTrack = goldTrack;
    }

    public ArrayList<Region> getRegions(){
        return this.regions;
    }

    public City getCityFromName(CitiesName cityName){
        String cityString = cityName.name();

        for (Region region : regions) {
            if (region.getCityFromName(cityString) != null)
                return region.getCityFromName(cityString);
        }
        return null;
    }
    /** Comodo metodo che dato il nome di un balcone ne ritorna il vero oggetto
     * @param  name il nome univoco del balcone*/
    public Balcony getBalconyFromName(String name){
        for (Region region: regions) {
            if (region.getRegionName().equalsIgnoreCase(name)) {
                return region.getBalcony();
            }
        }
        return null;
    }

    public KingGangplank getKingGangplank(){
        return kingGangplank;}

    public NobilityTrack getNobilityTrack() {
        return nobilityTrack;
    }

    public VictoryTrack getVictoryTrack() {
        return victoryTrack;
    }

    public GoldTrack getGoldTrack() {
        return goldTrack;
    }

    public String print(){
        ArrayList<String> printedList = new ArrayList<>();
        for (Region region: regions) {
            printedList.add(region.print());
        }
        return printedList+" Plancia del re: "+ kingGangplank +" "
                +"Percorso vittoria: "+victoryTrack +"" +
                ", Percorso ricchezza: " +goldTrack;
    }

    /** Ritorna tutte le città della mappa*/
    public ArrayList<City> getAllCities(){
        ArrayList<City> allCities = new ArrayList<>();
        Map<String, City> cityMap = new HashMap<>();
        for (Region region: regions) {
            cityMap = region.getCities();
            allCities.addAll(cityMap.values());
        }
        return allCities;
    }

    @Override
    public String toString() {
        return "GameMap{" +
                "regions=" + regions +
                ", kingGangplank=" + kingGangplank +
                ", victoryTrack=" + victoryTrack +
                ", nobilityTrack=" + nobilityTrack +
                ", goldTrack=" + goldTrack +
                '}';
    }
}


