package Game.Places;

import java.io.Serializable;

/**
 * Created by Samuele on 20/05/2016.
 */
public enum  CityColor implements Serializable {
    BLUE,
    ORANGE,
    GREY,
    YELLOW,
    VIOLET;

    public static CityColor getCityColorFromString(String cityColor){
        CityColor color = null;
        switch (cityColor){
            case "BLUE":{
                color = CityColor.BLUE;
            }break;
            case "ORANGE":{
                color = CityColor.ORANGE;
            }break;
            case "GREY":{
                color = CityColor.GREY;
            }break;
            case "YELLOW":{
                color = CityColor.YELLOW;
            }break;
            case "VIOLET":{
                color = CityColor.VIOLET;
            }break;
        }
        return color;
    }
}
