package Game.Places;

import Game.Recompenses.Rewards.Reward;
import Game.Stuff.Empory;
import Miscellaneous.Exceptions.EmporyException;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by edoar on 13/05/2016.
 */
public class City implements Serializable {
    //Ogni città possiede un nome, un colore e zero o più rewards.
    private CitiesName name;
    private CityColor color;
    private ArrayList<Reward> rewards;
    private transient ArrayList<City> nearCities = new ArrayList<>();
    private ArrayList<Empory> cityEmpories = new ArrayList<>();
    private boolean isVisited = false;

    public City(CitiesName name, ArrayList<Reward> rewards) {
        this.name = name;
        this.rewards = rewards;
    }

    public Character getInitial(){ //ritorna l'iniziale della città per riempire la tessera premio di costruzione
        return name.name().charAt(0);
    }

    public String getColorName(){
        return color.name();}

    public ArrayList<Reward> getRewards() {
        return rewards;
    }

    public void setRewards(ArrayList<Reward> rewards) {
        this.rewards = rewards;
    }

    public void setColor(CityColor color) {
        this.color = color;
    }

    public CitiesName getName() {
        return name;
    }

    public void addNearCity(City city){
        nearCities.add(city);}

    public ArrayList<Empory> getCityEmpories() {
        return cityEmpories;
    }

    /**Aggiunge emporio alla città
     * @param emporyToAdd l'emporio da aggiungere
     * @param userNick username dell'utente detentore
     * @throws EmporyException in caso non sia possibile aggiungere l'emporio per le regole definita dalla specifica */
    public void addEmpory(Empory emporyToAdd, String userNick) throws EmporyException {
        if (cityEmpories.size()<4){
            if (cityEmpories.contains(emporyToAdd)){//Se ci sono empori dello stesso colore:
                throw new EmporyException(userNick, this.getName().name()); //fira un'exc
            }
            cityEmpories.add(emporyToAdd); //altrimenti aggiungilo
        } else throw new EmporyException(userNick, this.getName().name(), "Non ci sono più posti disponibili per gli empori!");
    }
    public ArrayList<City> getNearCities() {
        return nearCities;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    @Override
    public String toString() {
        return "City{" +
                "name=" + name +
                ", color=" + color +
                ", rewards=" + rewards +",Num Empories: "
                +cityEmpories.size()+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (isVisited != city.isVisited) return false;
        if (name != city.name) return false;
        if (color != city.color) return false;
        if (rewards != null ? !rewards.equals(city.rewards) : city.rewards != null) return false;
        if (nearCities != null ? !nearCities.equals(city.nearCities) : city.nearCities != null) return false;
        return cityEmpories != null ? cityEmpories.equals(city.cityEmpories) : city.cityEmpories == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (rewards != null ? rewards.hashCode() : 0);
        result = 31 * result + (nearCities != null ? nearCities.hashCode() : 0);
        result = 31 * result + (cityEmpories != null ? cityEmpories.hashCode() : 0);
        result = 31 * result + (isVisited ? 1 : 0);
        return result;
    }
}
