package Game.Places.Balcony;

import Game.Places.Region;

/**
 * Created by edoar on 14/05/2016.
 */
/** Il balcone della regione, contiene la regione di appartenenza*/
public class RegionBalcony extends Balcony {

    //Ogni RegionBalcony ha una regione associata e una coda di consiglieri.
    private transient Region region; //La regione di appartenza


    public RegionBalcony() {

    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    /** Utility printing*/
    @Override
    public String print() {
        return "Balcone della regione "+ regionName + " , Consiglieri: "+
                balconyConcilors;
    }
}
