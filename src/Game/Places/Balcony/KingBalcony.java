package Game.Places.Balcony;

import Game.Stuff.Councilor;

import java.util.Queue;

/**
 * Created by edoar on 14/05/2016.
 */
//Differente dal Balcony stock perché non ha una regione di appartenenza.
public class KingBalcony extends Balcony {
    //Ogni KingBalcony ha solo una coda di consiglieri.
    public KingBalcony(){
        regionName="KING";
    }

    public void setBalconyConcilors(Queue<Councilor> balconyConcilors) {
        this.balconyConcilors = balconyConcilors;
    }

    @Override
    public String print() {
        return "Balcone del re, con Consiglieri:  "+balconyConcilors;
    }


    public String getRegionName() {
        return regionName;
    }


}
