package Game.Places.Balcony;

/**
 * Created by edoar on 13/05/2016.
 */

import Game.Stuff.Councilor;

import java.io.Serializable;
import java.util.Queue;

/** IL GENERICO BALCONE, contiene il nome della regione e i consiglieri in quel momento presenti*/

public abstract class Balcony implements Serializable {
    protected String regionName; //Per aggirare il transient della regione!
    protected Queue<Councilor> balconyConcilors;

    public void setBalconyConcilors(Queue<Councilor> balconyConcilors) {
        this.balconyConcilors = balconyConcilors;
    }

    /** Aggiunge un consigliere al balcone scalzandone uno
     * @param  councilor il consigliere da eleggere*/
    public void addCouncilor(Councilor councilor){
        balconyConcilors.add(councilor);
    }

    public Councilor removeCouncilor(){
        return balconyConcilors.remove();
    }

    public String getRegionName() {
        return regionName;
    }

    public Queue<Councilor> getBalconyConcilors() {
        return balconyConcilors;
    }
    public String print(){
        return null;
    }
}
