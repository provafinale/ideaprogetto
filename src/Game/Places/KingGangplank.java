package Game.Places;

import Game.Places.Balcony.KingBalcony;
import Game.Recompenses.BonusCards.BonusCard;
import Game.Recompenses.BonusCards.CityBonusCard;
import Game.Stuff.Councilor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by edoar on 13/05/2016.
 */
/** La plancia del re*/
public class KingGangplank implements Serializable {
    //Ogni Plancia del re contiene il nobility track e un balcone del re
    //Costruisco la KingGanplant dopo aver costruito tutti i suoi oggetti costituenti.
    private KingBalcony kingBalcony;
    private ArrayList<CityBonusCard> cityBonusCards; //le tessere che vinci conquistando tutte le città del colore
    private Stack<BonusCard> bonusCards; //le viola piccole


    /** @param createdKingBalcony il balcone del re appena creato
     * @param createdCityBonusCards i bonus del re appena creati
     * @param  createdBonusCards le carte bonus appena create*/
    public KingGangplank(KingBalcony createdKingBalcony, ArrayList<CityBonusCard> createdCityBonusCards, Stack<BonusCard> createdBonusCards) {
        this.kingBalcony = createdKingBalcony;
        this.cityBonusCards = createdCityBonusCards;
        this.bonusCards = createdBonusCards;
    }

    public KingBalcony getKingBalcony() {
        return kingBalcony;
    }

    public void setBalconyCouncilors(Queue<Councilor> councilors){
        kingBalcony.setBalconyConcilors(councilors);
    }

    @Override
    public String toString() {
        return "KingGangplank{" +
                "kingBalcony=" + kingBalcony +
                ", cityBonusCards=" + cityBonusCards +
                ", bonusCards=" + bonusCards +
                '}';
    }
}
