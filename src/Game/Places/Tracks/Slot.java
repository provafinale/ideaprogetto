package Game.Places.Tracks;

import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */
public class Slot implements Serializable {
    protected int number;

    public Slot(int number) {
        this.number = number;
    }

    //GETTERS AND SETTERS

    public int getNumber(){
        return this.number;
    }
}
