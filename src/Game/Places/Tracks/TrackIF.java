package Game.Places.Tracks;

import Game.User;
import Miscellaneous.Exceptions.MoveNotPossibleException;

import java.io.Serializable;

/**
 * Created by edoar on 28/05/2016.
 */
public interface TrackIF extends Serializable{

    /** @param  user l'utente da spostare
     * @param diff di quanto va spostato*/
    void go(User user, int diff) throws MoveNotPossibleException;
}
