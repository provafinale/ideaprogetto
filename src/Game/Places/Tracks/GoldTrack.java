package Game.Places.Tracks;

import Game.User;
import Miscellaneous.Exceptions.MoveNotPossibleException;

/**
 * Created by edoar on 13/05/2016.
 */
public class GoldTrack implements TrackIF{
    Slot[] slots = new Slot[21]; //I 21 coin dei giocatori

    public GoldTrack() {
        for (int i=0; i<slots.length; i++){
            slots[i] = new Slot(i);
        }
    }


    public Slot getSlotFromNumber(int num){
        return slots[num];
    }

    @Override
    public void go(User user, int diff) throws MoveNotPossibleException{
        int newPos= user.getPosGoldPawn()+diff;
    if (newPos<0) throw new MoveNotPossibleException(this.getClass().getName(), user.getName());
    else if (newPos <21) {
        Slot slot = getSlotFromNumber(newPos);
        user.getGoldPawn().setSlot(slot);
    }
        else { //Se sei full-soldi
        Slot slot = getSlotFromNumber(20);
        user.getGoldPawn().setSlot(slot);
    }
    }
}
