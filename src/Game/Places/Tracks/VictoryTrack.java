package Game.Places.Tracks;

import Game.User;
import Miscellaneous.Exceptions.MoveNotPossibleException;

/**
 * Created by edoar on 13/05/2016.
 */
public class VictoryTrack implements TrackIF{
    Slot[] slots = new Slot[100];

    public VictoryTrack(){
        for (int i=0; i<slots.length; i++){
            slots[i] = new Slot(i);}

    }

    /** Ritorna lo slot dato il numero */
    public Slot getSlotFromNumber(int num){
        return slots[num];
    }

    /** sposta la pedina utente sul percorso della vittoria*/
    @Override
    public void go(User user, int diff) throws MoveNotPossibleException {
        int newPos = user.getPosVictoryPawn()+diff;
        if (newPos<0) throw new MoveNotPossibleException(this.getClass().getName(), user.getName());
        else if (newPos < 100) {
            Slot slot = getSlotFromNumber(newPos);
            user.getVictoryPawn().setSlot(slot); //Sposta pedina
        }
        else{
            Slot slot = getSlotFromNumber(99);
            user.getVictoryPawn().setSlot(slot);
        }
    }
}
