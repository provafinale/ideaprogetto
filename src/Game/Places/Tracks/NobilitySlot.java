package Game.Places.Tracks;

import Game.Recompenses.Rewards.Reward;

import java.util.ArrayList;

/**
 * Created by edoar on 13/05/2016.
 */
public class NobilitySlot extends Slot { //Gli slot del percorso della nobiltà, possono avere dei rewards.
    ArrayList<Reward> rewards;

    public NobilitySlot(int number, ArrayList<Reward> rewards) {
        super(number);
        this.rewards = rewards;
    }

}
