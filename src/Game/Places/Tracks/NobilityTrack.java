package Game.Places.Tracks;

import Game.Recompenses.Rewards.Reward;
import Game.User;
import Miscellaneous.Exceptions.MoveNotPossibleException;
import Miscellaneous.Randomizer;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by edoar on 13/05/2016.
 */
public class NobilityTrack implements Serializable {

    NobilitySlot[] nobilitySlots = new NobilitySlot[21];

    /**Costruttore del Nobility Track che genera bonus random per ogni Slot.*/
    public NobilityTrack(){
        for (int i=0; i < nobilitySlots.length; i++){
            ArrayList<Reward> rewards = new ArrayList<>();

            int numOfRewards = Randomizer.randomNumberOfNobilityRewards();
            for (int j=0; j < numOfRewards; j++) {
                rewards.add(Reward.returnRandomTypeOfReward());
            }
            nobilitySlots[i] = new NobilitySlot(i,rewards);
        }
    }

    public NobilitySlot getNobilitySlotFromNum(int num){
        return nobilitySlots[num];
    }

    public void go(User user, int diff) throws MoveNotPossibleException{
        int newPos = user.getNobilityPawn().getPosition()+diff;

        if (newPos<0) throw new MoveNotPossibleException(this.getClass().getName(), user.getName());
        else if (newPos<21){
            NobilitySlot nobilitySlot = getNobilitySlotFromNum(newPos);
            user.getNobilityPawn().setSlot(nobilitySlot);}
        else{
            NobilitySlot nobilitySlot = getNobilitySlotFromNum(20);
            user.getNobilityPawn().setSlot(nobilitySlot);
        }
    }
}
