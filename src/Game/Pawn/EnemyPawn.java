package Game.Pawn;

/**
 * Created by edoar on 18/05/2016.
 */

import Game.UserColor;

import java.io.Serializable;

/** Le finte pedine da inviare negli snapshot. Ad ogni giro del costruisciSnapshot, ci
 * sarà un solo "Vero User", gli altri sono nemici e delle loro pedine posso vedere solo la posizione nei tracciati.
 */
public class EnemyPawn implements Serializable {
    private UserColor color;
    private int position;
}
