package Game.Pawn;

import Game.Places.Tracks.Slot;
import Game.User;
import Game.UserColor;

import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */
/** La generica pedina per il giocatore*/
public class Pawn implements Serializable{ //Le pedine, 4 x giocatore

    protected User user;
    protected UserColor color;
    protected Slot slot;


    //Per spostare la pedina, basterà un metodo setSlot(Slot newSlot), dove newSlot sarà la posizione finale.

    public Pawn(UserColor color) {
        this.color=color;
    }

    public Pawn() {
    }

    public void setUser (User user){
        this.user=user;
    }

    public Slot getSlot() {
        return slot;
    }

    public String getColorName(){
        return color.name();
    }

    public UserColor getColor(){
        return color;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
    public int getPosition(){
        return this.getSlot().getNumber();
    }
}
