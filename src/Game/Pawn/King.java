package Game.Pawn;

import Game.Places.City;

import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */
/** Singleton-pattern Re*/
public class King implements Serializable { //Singleton-Pattern King
    private static King king;
    private City city;
    private King(){}

    /** Singleton
     * @return nuovo re se non ne esiste nessuno, riferimento al vecchio re qualora esista.*/
    public static King createKing(){
        if (king == null)  king = new King();
         return king;
    }

    public void setCity(City city){
        this.city = city;
    }
    public City getCity(){
        return city;
    }
}
