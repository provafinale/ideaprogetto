package Game.Recompenses.BonusCards;
import Game.Places.Region;
import Game.Recompenses.Rewards.VictoryReward;
import Game.User;
import Miscellaneous.Constant;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by edoar on 14/05/2016.
 */
//Le tessere premio quando hai tutta la regione. Non vengono mai eliminate.

public class RegionBonusCard extends BonusCard implements Serializable {
    //Tengo in memoria chi ha ricevuto il bonus per evitare di darglielo più di una volta nella stessa regione.
    private transient Region region;
    private ArrayList<User> winnerUsers; //ooloro che hanno già vinto la singola RegionBonusCard

    public RegionBonusCard(){
        this.reward = new VictoryReward(Constant.BONUS_REGION_VALUE);
    }

    public void setRegion(Region region){
        this.region=region;
    }

    //Aggiunto all'arraylist dei vicintori della carta, l'user che l'ha vinta. Richiede un controllo preventivo!!
    public void setNewWinner(User user){
        winnerUsers.add(user);
    }

    //Controlla se all'interno dell'arraylist dei vincitori della cartaBonus, è presente il giocatore su cui viene invocata.
    public boolean isValidWinner(User user, Region region){
        for (User winnerUser: winnerUsers) { //forEach
            if (user.equals(winnerUser)) {
                System.out.println("WinnerUser: " + winnerUser);
                return false;
            }
        }
        return true;
    }
}
