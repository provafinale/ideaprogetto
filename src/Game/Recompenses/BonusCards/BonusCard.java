package Game.Recompenses.BonusCards;

import Game.Recompenses.Rewards.Reward;
import Game.Recompenses.Rewards.VictoryReward;

import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */
//Le tessere premio del re (le viola).

public  class BonusCard implements Serializable {
    //Ogni BonusCard possiede come attributo un solo Bonus.
    protected Reward reward;
    public BonusCard(){}


public Reward getReward(){
    return this.reward;
}
public void setReward(Reward reward){
    this.reward=reward;
}

    public BonusCard(int rewardValue){  //costruisci la tessera premio del re con il valore del bonus passato
        this.reward = new VictoryReward(rewardValue);
    }
}