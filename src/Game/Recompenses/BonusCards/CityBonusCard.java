package Game.Recompenses.BonusCards;

import Game.Places.City;
import Game.Places.CityColor;
import Game.Recompenses.Rewards.VictoryReward;
import Miscellaneous.Constant;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by edoar on 14/05/2016.
 */
//Le tessere premio quando hai tutte le città di un colore (Quelle di fianco alle tessere premio del re)
public class CityBonusCard extends BonusCard implements Serializable {
    //Oltre al Rewards, le CityBonusCard possiedono l'attributo città con un determinato colore uguale.
    protected CityColor cityColor; //Tranne il viola
    protected ArrayList<City> cities;


    public void setCities(ArrayList<City> cities) {
        this.cities = cities;
    }

    public CityBonusCard(CityColor cityColor){ //costruisce le cityBonusCard con il giusto VictoryReward
        this.cityColor = cityColor;
        switch (cityColor.name()){
            case "BLUE": {
                this.reward = new VictoryReward(Constant.BLUE_CITY_REWARD);
            }break;
            case "ORANGE": {
                this.reward = new VictoryReward(Constant.ORANGE_CITY_REWARD);
            }break;
            case "GREY": {
                this.reward = new VictoryReward(Constant.GREY_CITY_REWARD);
            }break;
            case "YELLOW": {
                this.reward = new VictoryReward(Constant.YELLOW_CITY_REWARD);
            }
        }
    }
}
