package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;

/**
 * Created by Samuele on 27/05/2016.
 */
public class MainActionReward extends Reward {
    private int mainActionNumber;

    public MainActionReward() {
        mainActionNumber=1;
    }

    @Override
    public void takeReward(User userForReward, GameLogic gameLogic) {
        gameLogic.alterNumberMainAction(userForReward, mainActionNumber);
    }

    @Override
    public String typeOfReward() {
        return "mainAction";
    }

    @Override
    public int numOfReward() {
        return mainActionNumber;
    }

    @Override
    public String print() {
        return mainActionNumber + " Azioni Principali";
    }
}
