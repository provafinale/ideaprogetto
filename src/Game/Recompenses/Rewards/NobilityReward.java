package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.PoorUserException;
import Miscellaneous.Randomizer;

/**
 * Created by edoar on 24/05/2016.
 */
public class NobilityReward extends Reward {

    private int nobilityNumber;
    public NobilityReward(){
        this.nobilityNumber = Randomizer.getRandomSingleReward(Constant.MIN_NOBILITY_BONUS, Constant.MAX_NOBILITY_BONUS);
    }
    @Override
    public void takeReward(User userForReward, GameLogic logic) {
        try {
            logic.alterNobility(nobilityNumber, userForReward);
        } catch (PoorUserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String typeOfReward() {
        return "nobility";
    }

    @Override
    public int numOfReward() {
        return nobilityNumber;
    }

    @Override
    public String print() {
        return nobilityNumber + " passi sul percorso nobiltà";
    }

    @Override
    public String toString() {
        return "NobilityReward{" +
                "nobilityNumber=" + nobilityNumber +
                '}';
    }
}
