package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Randomizer;

/**
 * Created by edoar on 24/05/2016.
 */
public class PoliticReward extends Reward{

    int politicNumber;

    public PoliticReward(){
        this.politicNumber = Randomizer.getRandomSingleReward(Constant.MIN_POLITIC_BONUS, Constant.MAX_POLITIC_BONUS);
    }

    @Override
    public void takeReward(User userForReward, GameLogic gameLogic) {
        gameLogic.alterPolitic(politicNumber, userForReward);
    }

    @Override
    public String typeOfReward() {
        return "politic";
    }

    @Override
    public int numOfReward() {
        return politicNumber;
    }

    @Override
    public String print() {
        return politicNumber + " Carte Politiche";
    }

    @Override
    public String toString() {
        return "PoliticReward{" +
                "politicNumber=" + politicNumber +
                '}';
    }

}
