package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.PoorUserException;
import Miscellaneous.Randomizer;

/**
 * Created by edobe on 23/05/2016.
 */


/**   UN ESEMPIO DI REWARD: calcola numero casuale di helpers e modifica lo user che ha richiesto il bonus.*/
public class HelpersReward extends Reward {

        private int helpersNumber;

    public HelpersReward(){
        helpersNumber = Randomizer.getRandomSingleReward(Constant.MIN_HELPERS_REWARD, Constant.MAX_HELPERS_REWARD);
    }
    @Override
    public void takeReward(User userForReward, GameLogic gameLogic) {
        try {
            System.out.println("DEBUG TAKEREWARD HELPERS: " +userForReward.getName() + helpersNumber);
            gameLogic.alterNumberOfHelpers(helpersNumber, false, userForReward);
        } catch (PoorUserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String typeOfReward() {
        return "helpers";
    }

    @Override
    public int numOfReward() {
        return helpersNumber;
    }

    @Override
    public String print() {
        return helpersNumber + " aiutanti ";
    }

    @Override
    public String toString() {
        return "HelpersReward{" +
                "helpersNumber=" + helpersNumber +
                '}';
    }
}
