package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.PoorUserException;
import Miscellaneous.Randomizer;

/**
 * Created by edobe on 23/05/2016.
 */
public class CoinsReward extends Reward {

    private int goldTaken;

    public CoinsReward(){
        goldTaken = Randomizer.getRandomSingleReward(Constant.MIN_COINS_BONUS, Constant.MAX_COINS_BONUS);
    }

    @Override
    public void takeReward(User userForReward, GameLogic gameLogic) {
        try {
            gameLogic.alterGolds(goldTaken, userForReward);
        } catch (PoorUserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String typeOfReward() {
        return "coins";
    }

    @Override
    public int numOfReward() {
        return goldTaken;
    }

    @Override
    public String print() {
        return "Monete: " + goldTaken;
    }

    @Override
    public String toString() {
        return "CoinsReward{" +
                "goldTaken=" + goldTaken +
                '}';
    }

}
