package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Randomizer;

import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */
//Sono i singoli premi (delle BonusCard, delle CityBonusCard, delle RegionBonusCard e dei Token legati alle città)
public abstract class Reward implements Serializable {

    public abstract void takeReward(User userForReward, GameLogic gameLogic);

    public abstract String typeOfReward();
    public abstract int numOfReward();
    public abstract String print();

    public static Reward returnRandomTypeOfReward(){
        Reward reward;
        switch (Randomizer.getRandomTypeOfReward()){
            case 0: {
                reward= new CoinsReward();
                return reward; //ritorno il reward specifico al chiamante.
            }
            case 1: {
                reward = new HelpersReward();
                return reward;
            }
            case 2: {
                reward = new NobilityReward();
                return reward;
            }
            case 3: {
                reward = new VictoryReward();
                return reward;
            }
            case 4: {
                reward = new PoliticReward();
                return reward;
            }
            case 5:{
                reward = new MainActionReward();
                return reward;
            }
        }
        return null; }

}
//TODO: Ci sono reward che chiamano il server?