package Game.Recompenses.Rewards;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.PoorUserException;
import Miscellaneous.Randomizer;

/**
 * Created by edoar on 24/05/2016.
 */
public class VictoryReward extends Reward{

    private int victoryNumber;
    public VictoryReward(){
        this.victoryNumber = Randomizer.getRandomSingleReward(Constant.MIN_VICTORY_BONUS, Constant.MAX_VICTORY_BONUS);
    }
    public VictoryReward(int victoryNumber){
        this.victoryNumber=victoryNumber;
    }

    @Override
    public void takeReward(User userForReward, GameLogic gameLogic) {
        try {
            gameLogic.alterVictory(victoryNumber, userForReward);
        } catch (PoorUserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String typeOfReward() {
        return "victory";
    }

    @Override
    public int numOfReward() {
        return victoryNumber;
    }

    @Override
    public String print() {
        return victoryNumber + " Passi Vittoria";
    }

    @Override
    public String toString() {
        return "VictoryReward{" +
                "victoryNumber=" + victoryNumber +
                '}';
    }
}
