package Game.MartketPack;

/**
 * Created by edobe on 26/06/2016.
 */
public interface Sellable {

    MarketObject sell();
    MarketObject buy();
}
