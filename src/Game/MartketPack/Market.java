package Game.MartketPack;

import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import Game.User;

/**
 * Created by edoar on 24/06/2016.
 */
public class Market implements Serializable{

    private HashMap<BuildingCard, Integer> buildingCostMap = new HashMap<>(); //CARD - COST
    private HashMap<User, HashMap<BuildingCard, Integer>> buildingsOnSell = new HashMap<>(); //USER -CARD-COST

    private HashMap<PoliticCard, Integer> politicCostMap = new HashMap<>();
    private HashMap<User,HashMap <PoliticCard, Integer>> politicsOnSell = new HashMap<>();

    private HashMap<Integer, Integer> helpersCostMap = new HashMap<>();
    private HashMap<User, HashMap<Integer, Integer>> helpersOnSell = new HashMap<>();
    //private HashMap<User, ArrayList<BuildingCard>> buildingsOnSell = new HashMap<>();

    public void sell(User user, Integer helpers, int price){
        helpersCostMap.put(helpers, price);

    }

}
