package Game;

import Miscellaneous.Constant;
import Server.ServerMatch;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by edoar on 13/05/2016.
 */
/** Il gestore delle partite*/
public class Game {
    //L'arraylist di ServerMatch tiene traccia delle diverse partite, che vengono fatte girare su un esecutore della JVM.
    private ArrayList<ServerMatch> serverMatches = new ArrayList<>();
    private ExecutorService executor = Executors.newCachedThreadPool();


    /** Nuova partita: viene istanziato un nuovo oggetto ServerMatch che si occuperà di gestire la singola partita.
     * Se c'è un match aperto, ne ritorna il riferimento*/
    public ServerMatch checkAvaibleMatch(){
        for (ServerMatch serverMatch:serverMatches) {
            if (serverMatchIsAvaible(serverMatch))
                return serverMatch;
        }
        return createMatch();
    }

    /** Logica per determinare se il serverMatch è disponibile */
    private boolean serverMatchIsAvaible(ServerMatch serverMatch){
        return serverMatch.isOpened() && serverMatch.numPlayersOnline()< Constant.numPlayersMatch;
    }

    /** Creo il match passando il primo utente
     * @return il match appena creato*/
    private ServerMatch createMatch(){
        ServerMatch serverMatch = new ServerMatch(); //Creo il match e gli passo il primo User (con dentro solo il nome)
        serverMatches.add(serverMatch);
        return serverMatch;
    }

}
