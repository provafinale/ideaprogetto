package Game;

import java.io.Serializable;

/**
 * Created by Samuele on 20/05/2016.
 */
public enum UserColor implements Serializable {
    BLUE,
    GREEN,
    RED,
    YELLOW;
}
