package Game.Cards;

import Game.Places.Region;
import Miscellaneous.Exceptions.BuildingDeckIsEmptyException;

import java.io.Serializable;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by edoar on 14/05/2016.
 */
public class BuildingDeck implements Serializable { //sono 3. Le carte permesso di costruzione ancora da pescare

    private Queue<BuildingCard> buildingCards = new ArrayBlockingQueue<BuildingCard>(15);
    private BuildingCard buildingCardOnTable1;
    private BuildingCard buildingCardOnTable2;//i 2 permessi di costruzione che si possono prendere (scoperti)
    private Region region;

    public BuildingDeck(Queue<BuildingCard> buildingCards, Region region) {
        this.buildingCards = buildingCards;
        this.region = region;
    }



    public void setBuildingCardOnTable1(BuildingCard buildingCardOnTable1) {
        this.buildingCardOnTable1 = buildingCardOnTable1;
    }

    public void setBuildingCardOnTable2(BuildingCard buildingCardOnTable2) {
        this.buildingCardOnTable2 = buildingCardOnTable2;
    }

    public void replaceCardOnTable(BuildingCard cardToReplace){

        if (cardToReplace.equals(buildingCardOnTable1)) { //se la carta da sostiture è davvero scoperta e in pos 1:
            buildingCards.add(buildingCardOnTable1); //inserisce in fondo
            buildingCardOnTable1 = takeOneCardFromDeck();
        }
        else if (cardToReplace.equals(buildingCardOnTable2)) {//se la carta da sostiture è davvero scoperta e in pos 2:
            buildingCards.add(buildingCardOnTable2);
            buildingCardOnTable2 = takeOneCardFromDeck();
        }
    }

    public void checkIfDeckIsEmpty() throws BuildingDeckIsEmptyException {
        if (buildingCards.size()<=0)
            throw new BuildingDeckIsEmptyException("BuildingDeck Terminato");
    }

    public void refreshCardOnTable(){
        try {
            System.out.println("REFRESH CARD ON TABLE IN BUILDINGDECK.CLASS");
            checkIfDeckIsEmpty();
            if (buildingCardOnTable1 == null)
                if (buildingCards.size()>0)
                    buildingCardOnTable1 = buildingCards.remove();
            if (buildingCardOnTable2 == null)
                if (buildingCards.size()>0)
                    buildingCardOnTable2 = buildingCards.remove();
        } catch (BuildingDeckIsEmptyException e) {
            e.printStackTrace();
        }

    }


    public BuildingCard takeOneCardFromDeck(){
        return (buildingCards.remove());
    }



    public Queue<BuildingCard> getBuildingCards() {
        return buildingCards;
    }

    public BuildingCard getBuildingCardOnTable1() {
        return buildingCardOnTable1;
    }

    public BuildingCard getBuildingCardOnTable2() {
        return buildingCardOnTable2;
    }
}
