package Game.Cards;

import java.io.Serializable;

/**
 * Created by Samuele on 20/05/2016.
 */
public enum ColorPoliticCard implements Serializable {
    VIOLET,
    WHITE,
    BLACK,
    ORANGE,
    PINK,
    BLUE,
    RAINBOW
}
