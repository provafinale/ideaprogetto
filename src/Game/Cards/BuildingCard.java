package Game.Cards;

/**
 * Created by edoar on 13/05/2016.
 */

import Game.Places.City;
import Game.Recompenses.Rewards.Reward;
import Miscellaneous.Randomizer;
import Server.Market.Buyable;

import java.io.Serializable;
import java.util.ArrayList;

/** CARTA PERMESSO DI COSTRUZIONE **/

public class  BuildingCard implements Serializable,Buyable {
    public static final String BUILDING_CARD = "BuildingCard";
    private ArrayList<City> cities = new ArrayList<>(); //le iniziali della città
    private ArrayList<Reward> rewards;

    public BuildingCard(City... addedCities) {
        this.rewards = applyRewards(); //applica random rewards

        for (City city : addedCities){
            cities.add(city);
        }
    }

    /** applica 1 o 2 Rewards alla carta permesso.
     * Utilizza il randomizer per generare randomicamente i bonus*/
    private ArrayList<Reward> applyRewards() {
        ArrayList<Reward> myRewards = new ArrayList<>();
        int howManyRewards = Randomizer.randomNumberOFRewards();
        for (int i = 0; i<howManyRewards; i++ ){
            myRewards.add(Reward.returnRandomTypeOfReward());
        }
    return myRewards;}

    public ArrayList<City> getCities() {
        return cities;
    }
    @Override
    public String toString() {
        return "BuildingCard{" +
                ", cities=" + cities +
                ", rewards=" + rewards +
                '}';
    }

    public String getInitials(){
        String stringToReturn ="";
        for (int i = 0; i < cities.size(); i++){
            stringToReturn = stringToReturn + cities.get(i).getInitial()+" ";
        }
        return stringToReturn;
    }

    public ArrayList<String> getTypesOfRewards(){
        ArrayList<String> typeOfRewards = new ArrayList<>();
        for (Reward reward : rewards){
            typeOfRewards.add(reward.typeOfReward());
        }
        return typeOfRewards;
    }

    public ArrayList<Integer> getNumbersOfRewards(){
        ArrayList<Integer> numOfRewards = new ArrayList<>();
        for (Reward reward : rewards){
            numOfRewards.add(reward.numOfReward());
        }
        return numOfRewards;
    }

    public boolean compareBuilding(BuildingCard buildingCard){
        if (buildingCard.getInitials().equals(this.getInitials()))
            return true;
        else return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuildingCard that = (BuildingCard) o;

        if (cities != null ? !cities.equals(that.cities) : that.cities != null) return false;
        return rewards != null ? rewards.equals(that.rewards) : that.rewards == null;

    }


    @Override
    public int hashCode() {
        int result = cities != null ? cities.hashCode() : 0;
        result = 31 * result + (rewards != null ? rewards.hashCode() : 0);
        return result;
    }

    @Override
    public BuildingCard getTypeOfObject() {
        return this;
    }

    @Override
    public String getNameOfObject() {
        return BUILDING_CARD ;
    }

    public ArrayList<Reward> getRewards() {
        return rewards;
    }

    @Override
    public String getDetails() {
        String initial = "";
        String bonus = "";
        for (City city : cities){
            initial = initial + " " + city.getInitial();}
        for (Reward reward : rewards){
            bonus = bonus + " " + reward.numOfReward()+" "+reward.typeOfReward();
        }
        return "Città:"+ initial+"\n"+"Bonus:"+bonus;
    }

    public String softPrint(){
        String initials =  "Carta permesso: "+getInitials();
        String bonus = null;
        for (Reward reward: rewards) {
            bonus = bonus + " "+ reward.print();
        }
        return bonus;
    }
}
