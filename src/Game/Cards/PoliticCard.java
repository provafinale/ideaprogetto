package Game.Cards;

import Server.Market.Buyable;

import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */

/** La carta politica, contiene un colore, un id univoco per colore*/
public class PoliticCard implements Serializable,Buyable {
    public static final String POLITIC_CARD = "Politic Card";
    private ColorPoliticCard color;
    private boolean visited;
    private int idInColor;
    public PoliticCard(ColorPoliticCard color, int idInColor) {
        this.idInColor = idInColor;
        this.color = color;
    }

    public String getColor(){
        return color.name();
    }
    public ColorPoliticCard getColorPoliticCard(){return color;}


    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isVisited() {
        return visited;
    }

    @Override
    public String toString() {
        return "PoliticCard{" +
                "color=" + color +
                '}';

    }

    public boolean equals(PoliticCard politicCard) {
        return this.getColor().equals(politicCard.getColor());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PoliticCard that = (PoliticCard) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }

    public String print(){
        return "Carta " + color;
    }

    @Override
    public PoliticCard getTypeOfObject() {
        return null;
    }

    @Override
    public String getNameOfObject() {
        return POLITIC_CARD;
    }

    @Override
    public String getDetails() {
        return "Colore: "+color.name();
    }

    public int getIdInColor() {
        return idInColor;
    }
}
