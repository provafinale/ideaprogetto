package Game.Actions;

import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.GameLogic;
import Game.Places.Balcony.Balcony;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.PoorUserException;

import java.util.ArrayList;

/**
 * Created by edoar on 30/05/2016.
 */

//SODDISFA IL BALCONE E COMPRA UNA CARTA PERMESSO DI COSTRUZIONE
public class BuyBuildingCard extends GenericAction{

    String balconyToSatisfy;
    Balcony balcony;
    ArrayList<PoliticCard> clientPoliticCards;
    BuildingCard buildingCardToAcquire;

    public BuyBuildingCard(ArrayList<PoliticCard> clientPoliticCards, String balconyToSatisfy, BuildingCard building){
        this.balconyToSatisfy = balconyToSatisfy;
        this.clientPoliticCards = clientPoliticCards;
        this.buildingCardToAcquire = building;
        genre = Constant.MAIN_ACTION;
    }

    public BuyBuildingCard() {
    }

    @Override
    public void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException {
        user = serverUser;
        int satisfyCost;
        if (hasMoreActions(user)) {
            balcony = gamelogic.getGameMap().getBalconyFromName(balconyToSatisfy);
            if (checkPoliticCards(user, gamelogic, clientPoliticCards)) { //Se il client non ha barato
                int satisfierCards = howManySatisfier(clientPoliticCards, balcony);
                if (satisfierCards == clientPoliticCards.size()) {
                    if (satisfierCards >= 4) { //se ho 4 carte davvero colorate giuste:
                        //Rimuove la buildingCard dalla LOGICA
                        BuildingCard buildingCard = gamelogic.removeBuildingCard(buildingCardToAcquire);
                        gamelogic.takeBuildingCardForClient(user, buildingCard);
                        takeBuildingRewards(user, gamelogic, buildingCard);
                        burnTheCards(clientPoliticCards, user, gamelogic); //rimuovo le carte politiche
                        removeOneAction(serverUser, gamelogic.getServerMatch());
                    } else if (satisfierCards > 0) { //se ho almeno una vera carta, procedo
                        satisfyCost = calculateBuyingCost(satisfierCards, countRainbow(clientPoliticCards));
                        try {
                            //Rimuove la buildingCard dalla LOGICA
                            BuildingCard buildingCard = gamelogic.removeBuildingCard(buildingCardToAcquire);
                            gamelogic.alterGolds(-satisfyCost, user); //provo ad arretrare sulla ricchezza
                            gamelogic.takeBuildingCardForClient(user, buildingCard); //se tutto è andato bene, senda la carta
                            takeBuildingRewards(user, gamelogic, buildingCard);
                            burnTheCards(clientPoliticCards, user, gamelogic);
                            removeOneAction(serverUser, gamelogic.getServerMatch());
                        } catch (PoorUserException e) {
                            throw new ActionNotPossibleException(e);
                        }
                    }
                } else throw new ActionNotPossibleException("Le carte selezionate non soddisfano il consiglio! Riprova!");
            }

        }
    }

}
