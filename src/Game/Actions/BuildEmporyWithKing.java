package Game.Actions;

import Game.Cards.PoliticCard;
import Game.GameLogic;
import Game.Places.Balcony.KingBalcony;
import Game.Places.City;
import Game.Places.GameMap;
import Game.Stuff.Empory;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.*;

import java.util.ArrayList;

/**
 * Created by edoar on 11/06/2016.
 */
public class BuildEmporyWithKing extends GenericAction {

    KingBalcony kingBalcony;
    private ArrayList<PoliticCard> clientPoliticCards;
    private ArrayList<City> kingCities;
    private int kingCost;

    private City cityForBuilding;

    public BuildEmporyWithKing(ArrayList<PoliticCard> politicCards, KingBalcony kingBalcony, ArrayList<City> kingCities) {
        this.genre = Constant.MAIN_ACTION;
        this.clientPoliticCards = politicCards;
        this.kingBalcony = kingBalcony;
        this.kingCities = kingCities; //le città che l'utente ha selezionato per spostare il re
    }

    @Override
    public void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException {
        this.user = serverUser;
        this.kingCities = getTrueCities(gamelogic); // Getto le vere città
        int satisfyCost;
        if (hasMoreActions(user)){
        if (checkPoliticCards(user, gamelogic, clientPoliticCards)) { //se non ho barato
            int satisfierCards = howManySatisfier(clientPoliticCards, kingBalcony);
            if (satisfierCards >= 4) { //se ho 4 carte davvero colorate giuste:
                if(checkKingRoad(gamelogic)){
                    gamelogic.moveKing(cityForBuilding);
                    burnTheCards(clientPoliticCards, user, gamelogic); //rimuovo le carte politiche
                    placeEmpory(gamelogic);
                }
            } else if (satisfierCards > 0) { //se ho almeno una vera carta, procedo
                satisfyCost = calculateBuyingCost(satisfierCards, countRainbow(clientPoliticCards));
                try {
                    if (checkKingRoad(gamelogic) && user.getPosGoldPawn()>=satisfyCost) {
                        gamelogic.moveKing(cityForBuilding);
                        gamelogic.alterGolds(-satisfyCost, user); //provo ad arretrare sulla ricchezza
                        burnTheCards(clientPoliticCards, user, gamelogic);
                        placeEmpory(gamelogic);
                    }
                } catch (PoorUserException e) {
                    throw new ActionNotPossibleException(e);}
            }
        }
        }
    }

    /** Controlla che le città passate dall'utente siano davvero adiacenti
      */
    private boolean checkKingRoad(GameLogic logic) throws ActionNotPossibleException {
        for (int i=0; i<kingCities.size()-1; i++){
            if (!kingCities.get(i).getNearCities().contains(kingCities.get(i+1))){
                return false;
            } else kingCost++;
        }
        try {
            logic.alterGolds(-kingCost, user);
        } catch (PoorUserException e) {
            throw new ActionNotPossibleException(e);
        }

        cityForBuilding = kingCities.get(kingCities.size()-1);
        return true;
    }

    private void placeEmpory(GameLogic gamelogic) throws ActionNotPossibleException {
        Empory empory = null; //Fatti dare un emporio dalla banca
        try {
            empory = gamelogic.getBank().getEmpory(user.getColor());
        } catch (EmptyBankException e) {
            e.printStackTrace();
            throw new ActionNotPossibleException(e);
        }
        empory.setCity(cityForBuilding);
        City city = gamelogic.getGameMap().getCityFromName(cityForBuilding.getName());
        try {
            city.addEmpory(empory, user.getName());
            getAllCityRewards(user, city, gamelogic);
        } catch (EmporyException e) {
            e.printStackTrace();
            throw new ActionNotPossibleException(e);
        }
        removeOneAction(user, gamelogic.getServerMatch());
    }


    private ArrayList<City> getTrueCities(GameLogic logic){
        GameMap gameMap = logic.getGameMap();
        ArrayList<City> trueCities = new ArrayList<>();
        for (City city: kingCities) {
            trueCities.add(gameMap.getCityFromName(city.getName()));
        }
        return trueCities;
    }
}
