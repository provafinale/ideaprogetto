package Game.Actions;

import Game.GameLogic;
import Game.Places.Balcony.Balcony;
import Game.Stuff.Councilor;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.EmptyBankException;
import Miscellaneous.Exceptions.PoorUserException;

/**
 * Created by edoar on 03/06/2016.
 */
public class ElectCouncilorWithHelper extends GenericAction {

    private Councilor councilorToElect;
    private Balcony hotBalcony;

    public ElectCouncilorWithHelper(Councilor councilorToElect, Balcony balcony){
        this.councilorToElect = councilorToElect;
        this.hotBalcony = balcony;
        this.genre = Constant.FAST_ACTION;
    }


    @Override
    public void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException, EmptyBankException {
        this.user = serverUser;
        String councilorColor = councilorToElect.getCouncilorColorName();
        Balcony logicBalcony = gamelogic.getGameMap().getBalconyFromName(hotBalcony.getRegionName());

        if (hasMoreActions(user)) {
            try {
                gamelogic.getBank().hasMoreCouncilors(councilorColor, user.getName()); //se la banca ha ancora almeno un consigliere di quel colore:

                Councilor logicCouncilor = gamelogic.getBank().getCouncilor(councilorToElect.getCouncilorColor());
                gamelogic.alterNumberOfHelpers(-1, true, user);
                gamelogic.getBank().addCouncilor(logicBalcony.removeCouncilor()); //Rimuove consigliere e lo piazza in banca
                logicBalcony.addCouncilor(logicCouncilor);
                removeOneAction(user, gamelogic.getServerMatch());

            } catch (EmptyBankException | PoorUserException e) {
                throw new ActionNotPossibleException(e);
            }
        }
    }

    @Override
    public String toString() {
        return "ElectCouncilorWithHelper{" +
                "councilorToElect=" + councilorToElect +
                ", hotBalcony=" + hotBalcony +
                '}';
    }
}
