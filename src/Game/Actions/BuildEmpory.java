package Game.Actions;

import Game.Cards.BuildingCard;
import Game.GameLogic;
import Game.Places.City;
import Game.Stuff.Empory;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.EmporyException;
import Miscellaneous.Exceptions.EmptyBankException;

/**
 * Created by edoar on 30/05/2016.
 */

//COSTRUISCI EMPORIO USANDO UNA TESSERA PERMESSO
public class BuildEmpory extends GenericAction {

    private BuildingCard clientBuildingCard;
    private City city;


    public BuildEmpory(BuildingCard clientBuildingCard, City city){
        this.clientBuildingCard = clientBuildingCard;
        this.city = city;
        genre = Constant.MAIN_ACTION;
    }

    @Override
    public void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException {
        this.user = serverUser;
        this.city = getTrueServerCity(gamelogic, city);
        if (hasMoreActions(user)) {
            if (checkCityCard(clientBuildingCard, user)){
                if (isValidCityInCard(clientBuildingCard, city)) {
                    try {
                        Empory empory = gamelogic.getBank().getEmpory(user.getColor()); //Fatti dare un emporio dalla banca
                        empory.setCity(city);
                        try {
                            city.addEmpory(empory, user.getName());
                            getAllCityRewards(user, city, gamelogic);
                            if (isWinner(user, gamelogic)) {
                                System.out.println("Abbiamo un vincitore!" +user.getName());
                                gamelogic.setWinner(user);
                                //TODO: SEND CHE HAI VINTO hoo
                            }
                        } catch (EmporyException e) {
                            throw new ActionNotPossibleException(e);
                        }
                        removeOneAction(serverUser, gamelogic.getServerMatch());
                    } catch (EmptyBankException e) {
                        e.printStackTrace();
                        throw new ActionNotPossibleException(e);
                    }
                } else throw new ActionNotPossibleException("La città su cui tenti di costruire non è valida per quella tessera permesso!");
            } else System.out.println("HAI BARATO");
        }
    }

    private City getTrueServerCity(GameLogic logic, City fakeCity){
        return logic.getServerMatch().getCityFromName(fakeCity.getName().name());
    }
}
