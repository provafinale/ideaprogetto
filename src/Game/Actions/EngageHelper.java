package Game.Actions;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.NoMoreActionsException;
import Miscellaneous.Exceptions.PoorUserException;

/**
 * Created by edoar on 30/05/2016.
 */


public class EngageHelper extends GenericAction {

    public EngageHelper(){
        genre = Constant.FAST_ACTION;
    }

    @Override
    public void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException {
        this.user = serverUser;
        if (hasMoreActions(user)) {
            try {
                gamelogic.alterGolds(-3, user); //SPENDO 3 MONETE
                gamelogic.alterNumberOfHelpers(1, true, user); //E GUADAGNO UN AIUTANTE
                removeOneAction(serverUser, gamelogic.getServerMatch());
            } catch (PoorUserException e) {
                e.printStackTrace();
                throw new ActionNotPossibleException(e);
            }
        }else {
            System.out.println("non ho azioni disponibili [DEBUG]");
            NoMoreActionsException sub = new NoMoreActionsException(user.getName());
            System.out.println(sub);
            throw  new ActionNotPossibleException(sub);}
    }

    @Override
    public String toString() {
        return "EngageHelper{}";
    }
}
