package Game.Actions;

import Game.Cards.BuildingCard;
import Game.Cards.ColorPoliticCard;
import Game.Cards.PoliticCard;
import Game.GameLogic;
import Game.Places.Balcony.Balcony;
import Game.Places.City;
import Game.Recompenses.Rewards.Reward;
import Game.Stuff.Councilor;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.EmptyBankException;
import Server.ServerMatch;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by edoar on 20/05/2016.
 */

public abstract class GenericAction implements Serializable {

    protected String genre;
    protected User user; //VIENE SETTATO NELLE EXECUTE PASSANDOGLI IL TORNATO DALLA HASHMAP

    /** Tenta di eseguire l'azione specifica
     * @param gamelogic riferimento al model per l'esecuzione reale delle azioni sulla situazione consolidata
     * @param serverUser l'utente che ha richiesto l'esecuzione dell'azione
     * @throws ActionNotPossibleException in caso un punto del business specifico non sia eseguibile completamente.*/
    public abstract void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException, EmptyBankException;//SERVER-SIDE

    // nella classe astratta implementeremo comunque i metodi che on richiedono la action specifica: checks eccetera.
    //Overrideremo executeAction nelle azioni specifiche. DA ESEGUIRE SERVERSIDE

    /**
     * CONTROLLO AZIONI RIMANENTI
     * @throws ActionNotPossibleException in caso l'utente non abbia più azioni di quel tipo
     */
    public boolean hasMoreActions(User user) throws ActionNotPossibleException {
        switch (genre) { //da usare in sottoclassi
            case (Constant.MAIN_ACTION): {
                if (user.getRemainingMainActions() <= 0) {
                    System.out.println("TERMINATE MAIN ACTIONS");
                    throw new ActionNotPossibleException(user.getName() + " ha terminato le azioni principali a sua disposizione.");
                }
                else return true;
            }
            case (Constant.FAST_ACTION): {
                if (user.getRemainingFastActions() <= 0) {
                    System.out.println("TERMINATE FAST ACTIONS.");
                    throw new ActionNotPossibleException(user.getName() + " ha terminato le azioni veloci a sua disposizione.");
                }
                else return true;
            }
            default:
                throw new ActionNotPossibleException("Non è definita quest'azione."); //per protezione
        }
    }

    /**
     * Rimuove un'azione del tipo desiderato e dispatcha uno snapshot a tutti i clients
     */
    public void removeOneAction(User user, ServerMatch serverMatch) {
        final String ANSI_GREEN = "\u001B[32m";
        final String ANSI_RESET = "\u001B[0m";
        switch (genre) {
            case (Constant.MAIN_ACTION): {
                user.setNumMainAction(user.getRemainingMainActions() - 1); //rimuovo azione principale
                break;
            }
            case (Constant.FAST_ACTION): {
                user.setNumFastAction(user.getRemainingFastActions() - 1);
                break;
            }
        }
        //user.getBuildingCards().forEach(buildingCard1 -> System.out.println("building: "+buildingCard1));
        //user.getPoliticCards().forEach(politicCard -> System.out.println("Politica: "+politicCard));
        serverMatch.createAndSendSnapshots(); //Avendo effettuato un'azione, aggiorno tutti i clients
        System.out.println(ANSI_GREEN+"Azione effettuata con successo da "+user.getName()+
                "\n"+"Snapshot inviato a tutti i giocatori."+ANSI_RESET);
    }

    /** Scorre tutte le città e acquisice i bonus relativi come da regole del gioco
     * @param  user l'utente che si vedrà accreditati gli eventuali bonus
     * @param userCity la città su cui costruisci l'emporio: l'algoritmo di acquisizione bonus dalle città
     * limitrofe parte da qui
     * @param logic la logica su cui operare*/
    public void getAllCityRewards(User user, City userCity, GameLogic logic){
        //prendi il reward della singola città:
        City city = logic.getServerMatch().getCityFromName(userCity.getName().name());
        takeTheRewards(city, logic);
        city.setVisited(true);

        for (City singleCity: city.getNearCities()) {
            if (!singleCity.isVisited()){
                singleCity.setVisited(true);
                //Se ho un emporio mio
                singleCity.getCityEmpories().stream().filter(empory ->
                        empory.getColor().equals(user.getColor())).forEach(empory -> { //Se ho un emporio mio
                    takeTheRewards(singleCity, logic);
                });
            }
            for (City subCity: singleCity.getNearCities()) { //Scorri le sottocittà di ogni sottocittà
                if (!subCity.isVisited()){
                    subCity.setVisited(true);
                    singleCity.getCityEmpories().stream().filter(empory ->
                            empory.getColor().equals(user.getColor())).forEach(empory -> { //Se ho un emporio mio
                        takeTheRewards(singleCity, logic);
                    });
                }
            }
        }
    }

    /** Controlla che le carte siano davvero possedute lato server
     * @param  clientCards le carte passate dal client, ne testo la genuinità*/
    public boolean checkPoliticCards(User user, GameLogic gameLogic, ArrayList<PoliticCard> clientCards) {
        int checker = 0;
        resetCheckIsVisitedPoliticCards(clientCards);
        for (PoliticCard clientCard : clientCards) {
            for (PoliticCard trueUserCard : user.getPoliticCards()) {
                if (trueUserCard.equals(clientCard) && !clientCard.isVisited()) {
                    clientCard.setVisited(true);
                    checker++;
                }
            }
        }
        return (checker == clientCards.size()); //torna vero se le carte client sono davvero possedute
    }

    public void resetCheckIsVisitedPoliticCards(ArrayList<PoliticCard> politicCards){
        for (PoliticCard politicCard : politicCards){
            politicCard.setVisited(false);
        }
    }

    /** invocato in punti sensibili alla vittoria,
     * @return  true se l'utente è vincitore, false altrimenti*/
    public boolean isWinner(User user, GameLogic logic){
        if (logic.getBank().getNumberOfEmpory(user) != 0){ //se ci sono ancora empori in banca
            return false;
        } else return  true;
    }

    /** Controlla che la carta sia davvero di proprietà dell'utente
     * @param clientCard la carta permesso passata dal client*/
    public boolean checkBuildingCard(User user, BuildingCard clientCard) {
        for (BuildingCard trueServerCard: user.getBuildingCards()) {
            if (trueServerCard.getInitials().equalsIgnoreCase(clientCard.getInitials())){
                return true;
            }
        }
        return false;

    }

    /** Calcola secondo le specifiche di gioco il prezzo in monete necessario al soddisfacimento del consiglio
     * @param trueSatisfierCards il numero delle carte politiche (non arcobaleno) che hanno passato i check precedenti
     * @param rainbows il numero delle carte arcobaleno*/
    int calculateBuyingCost(int trueSatisfierCards, int rainbows) {
        switch (trueSatisfierCards + rainbows) {
            case 1: {
                return (10 + rainbows);
            }
            case 2: {
                return (7 + rainbows);
            }
            case 3: {
                return (4 + rainbows);
            }
            case 4: {
                return (rainbows);
            }
            default:
                return 1000;
        }

    }

    /** ritorna quanti consiglieri soddisfano davvero
     * @param clientPoliticCards le carte politiche passate in atto di creazione azione
     * @param balcony il balcone da soddisfare scelto dal client*/
    protected  int howManySatisfier(ArrayList<PoliticCard> clientPoliticCards, Balcony balcony){ //ritorna quante carte politiche scelte dal client soddisfano davvero il consiglio
        int howMany=0;
        System.out.print("HOWMANY SATISFIER:   ");
        clientPoliticCards.forEach(System.out::println);
        resetSatisfied(balcony);
        for (PoliticCard politicCard: clientPoliticCards) {
            for (Councilor councilor: balcony.getBalconyConcilors()) {
                if (politicCard.getColor()== ColorPoliticCard.RAINBOW.name()) {
                    howMany++;
                    break;
                }
                if(councilor.getCouncilorColorName().equalsIgnoreCase(politicCard.getColor()) && !councilor.isSatisfied()){
                    councilor.setSatisfied(true);
                    howMany++;
                    break;
                }
            }
        }
        resetSatisfied(balcony);
        return howMany;
    }

    private void resetSatisfied(Balcony balcony){
        for (Councilor counciolor: balcony.getBalconyConcilors()) {
            counciolor.setSatisfied(false);
        }
    }


    protected static int countRainbow(ArrayList<PoliticCard>clientPoliticCards){
        int rainbow = 0;
        for (PoliticCard politicCard: clientPoliticCards) {
            if (politicCard.getColor().equalsIgnoreCase("RAINBOW")){
                rainbow++;
            }
        }
        return rainbow;
    }

    /** rimuove le carte dalla mano dell'utente e le ripiazza nella banca
     * @param politicCards le carte da rimuovere
     * @param user l'utente a cui rimuoverle*/
    protected void burnTheCards(ArrayList<PoliticCard> politicCards, User user, GameLogic logic){
        user.getPoliticCards().removeAll(politicCards);
        logic.addPoliticCards(politicCards); //ripiazza le carte in banca
    }

    /** acquisisce tutti i possibile rewards delle città*/
    private void takeTheRewards(City city, GameLogic logic){
           /* Acquisisci i rewards */
        ArrayList<Reward> rewardsToTake = city.getRewards();
        for (Reward reward: rewardsToTake) {
            reward.takeReward(user, logic);
        }
    }

    /** acquisisce tutti i possibili rewards della carta permesso
     * @param user l'utente da arricchire dei bonus
     * @param card la carta di cui recuperare i bonus*/
    void takeBuildingRewards(User user, GameLogic logic, BuildingCard card){
        ArrayList<Reward> rewards = card.getRewards();
        for (Reward reward: rewards) {
            System.out.println("In takeBuildingReward: " +reward);
            reward.takeReward(user, logic);
        }
    }
    boolean checkCityCard(BuildingCard clientCard, User user){
        for (BuildingCard userCard: user.getBuildingCards()) {
            if (clientCard.getInitials().equalsIgnoreCase(userCard.getInitials())){
                return true;
            }
        }
        System.out.println("CHECKCITYCARD FALLITO...");
        return false;

    }
    boolean isValidCityInCard(BuildingCard card, City city){
       if (card.getInitials().indexOf(city.getInitial()) >=0){
        return true;
       } else return false;
    }
}
