package Game.Actions;

import Game.GameLogic;
import Game.Places.Balcony.Balcony;
import Game.Stuff.Councilor;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.EmptyBankException;
import Miscellaneous.Exceptions.MoveNotPossibleException;

/**
 * Created by edoar on 20/05/2016.
 */
public class  ElectCouncilor extends GenericAction {

    private Councilor councilorToElect;
    private Balcony hotBalcony;

    public ElectCouncilor(Councilor councilorToElect, Balcony balcony) {
        this.councilorToElect = councilorToElect;
        this.hotBalcony = balcony;
        genre = Constant.MAIN_ACTION;
    }


    @Override
    public void executeAction(GameLogic gameLogic, User serverUser) throws ActionNotPossibleException {
        this.user = serverUser;
        if (hasMoreActions(user)) {

            //Tenta di aggiungere l'ulteriore pezzo di Azione (4 monete)
            try {
                gameLogic.getBank().hasMoreCouncilors(councilorToElect.getCouncilorColorName(), user.getName());

                Balcony logicBalcony = gameLogic.getGameMap().getBalconyFromName(hotBalcony.getRegionName());
                Councilor logicCouncilor = gameLogic.getBank().getCouncilor(councilorToElect.getCouncilorColor());

                gameLogic.getGameMap().getGoldTrack().go(user, Constant.GOLD_EARNED_FOR_MAIN_ELECTION);

                gameLogic.getBank().addCouncilor(logicBalcony.removeCouncilor());//rimuove consigliere e mette in banca
                logicBalcony.addCouncilor(logicCouncilor);
                removeOneAction(user, gameLogic.getServerMatch());
            } catch (MoveNotPossibleException e) {
                e.printStackTrace();
            } catch (EmptyBankException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "ElectCouncilor{" +
                "councilorToElect=" + councilorToElect +
                ", hotBalcony=" + hotBalcony +
                '}';
    }
}