package Game.Actions;

import Game.Cards.BuildingDeck;
import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.PoorUserException;

/**
 * Created by edoar on 28/05/2016.
 */
public class ChangeBuildingCards extends GenericAction{

    private BuildingDeck hotDeck;
    private String chosenDeck;

    public ChangeBuildingCards(String regionDeck){
        this.chosenDeck = regionDeck;
        genre = Constant.FAST_ACTION;
    }



    @Override
    public void executeAction(GameLogic gameLogic, User serverUser) throws ActionNotPossibleException {
        this.user = serverUser;
        hotDeck = gameLogic.getBuildingDeckFromRegion(chosenDeck);
        boolean bankInvolved = true; //comporta il reinserimento di un aiutante in banca
        if (hasMoreActions(user)) {
            try {
                gameLogic.alterNumberOfHelpers(-1, bankInvolved, user);
                hotDeck.replaceCardOnTable(hotDeck.getBuildingCardOnTable1());
                hotDeck.replaceCardOnTable(hotDeck.getBuildingCardOnTable2());
                removeOneAction(serverUser, gameLogic.getServerMatch());
            } catch (PoorUserException e) {
                e.printStackTrace();
            }
        }
    }
}
