package Game.Actions;

import Game.GameLogic;
import Game.User;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.PoorUserException;

/**
 * Created by edoar on 30/05/2016.
 */
public class AnotherMainAction extends GenericAction {

    public AnotherMainAction(){
        this.genre = Constant.FAST_ACTION;
    }

    @Override
    public void executeAction(GameLogic gamelogic, User serverUser) throws ActionNotPossibleException {
        this.user = serverUser;
        if (hasMoreActions(user)) {
            try {
                gamelogic.alterNumberOfHelpers(-3, true, user);
                gamelogic.alterNumberMainAction(user, 1);
                removeOneAction(user, gamelogic.getServerMatch());
            } catch (PoorUserException e) {
                e.printStackTrace();
            }
        }
    }
}
