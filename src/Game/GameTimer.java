package Game;

import Server.ServerMatch;

import java.util.TimerTask;

/**
 * Created by Samuele on 28/05/2016.
 */
public class GameTimer extends TimerTask {
    private ServerMatch serverMatch;

    public GameTimer(ServerMatch serverMatch) {
        this.serverMatch = serverMatch;
    }

    @Override
    public void run() {
        serverMatch.startMatch();
    }
}
