package Game;

import Game.Cards.BuildingCard;
import Game.Places.City;
import Game.Places.GameMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Samuele on 14/05/2016.
 */

/**Un flash della situazione. Deve contentere tutti e soli gli elementi variabili sul turno.
 * */

public class Snapshot implements Serializable { //Il server torna uno Snapshot se l'azione è andata a buon fine.
    private Bank bank; //Situazione attuale della Banca
    private User me; //l'utente che ha effettuato la Action sul Server.
    private ArrayList<Enemy> enemies; //Contiene tutti gli altri giocatori con un set di dati limitato.
    private GameMap gameMap; //la nuova situazione della mappa
    private HashMap<String,List<BuildingCard>> buildingCardsOnTable;
    private City cityOfKing;


    public Snapshot(User userForPersonalSnapshot, ArrayList<Enemy> enemies, Bank refreshedBank, GameMap refreshedGameMap,HashMap<String,List<BuildingCard>> buildingCardsOnTable,City cityOfKing) {
        this.me = userForPersonalSnapshot;
        this.enemies=enemies;
        this.bank= refreshedBank;
        this.gameMap = refreshedGameMap;
        this.buildingCardsOnTable = buildingCardsOnTable;
        this.cityOfKing = cityOfKing;
    }

    public City getCityOfKing() {
        return cityOfKing;
    }

    public Bank getBank() {
        return bank;
    }

    public User getMe() {
        return me;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public GameMap getGameMap() {
        return gameMap;
    }

    public List<BuildingCard> getBuildingCardFromRegionName(String regionName){
        return buildingCardsOnTable.get(regionName);
    }

    @Override
    public String toString() {
        return "Snapshot{" +
                "bank=" + bank +
                ", me=" + me +
                ", enemies=" + enemies +
                ", gameMap=" + gameMap +
                '}';
    }
}
