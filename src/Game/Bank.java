package Game;

import Game.Stuff.Councilor;
import Game.Stuff.CouncilorColor;
import Game.Stuff.Empory;
import Miscellaneous.Exceptions.EmptyBankException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by edoar on 14/05/2016.
 */
/** Simula la scatola.
 * Contiene tutti gli oggetti di gioco, vengono prelevati dal gameCreator e vi vengono riposti
 * qualora necessario*/
public class Bank implements Serializable{
    //Nella banca conserviamo i consiglieri e gli aiutanti non utilizzati.
    private HashMap<String,List<Councilor>> councilors;
    private int helpers;
    private HashMap<String,List<Empory>> empories;

    public Bank(HashMap<String, List<Councilor>> councilors, int helpers, HashMap<String, List<Empory>> empories) {
        this.helpers = helpers;
        this.empories = empories;
        this.councilors = councilors;
    }


    /** Controlla che l'utente abbia ancora consiglieri disponibili in banca
     * @param color il colore del consigliere (= colore utente)
     * @param tryingUserNick nickname utente*/
    public boolean hasMoreCouncilors(String color, String tryingUserNick) throws EmptyBankException {
        List<Councilor> someCouncilours = councilors.get(color);
        if (someCouncilours.size()>0) return true;
        else throw new EmptyBankException("Sono terminati i consiglieri di "+tryingUserNick + " in banca", tryingUserNick);
    }
    /**Ritorna un consigliere dalla banca, spilandolo
     * @param color il colore del consigliere da togliere dalla banca
     * @throws EmptyBankException qualora fossero terminati*/
    public Councilor getCouncilor(CouncilorColor color) throws EmptyBankException{
        List<Councilor> selectedCouncilor  = councilors.get(color.name());

        if (!selectedCouncilor.isEmpty()) {
            int dimList = selectedCouncilor.size();
            Councilor councilor = selectedCouncilor.get(dimList - 1);
            selectedCouncilor.remove(dimList - 1);
            return councilor;
        }
        else
            throw new EmptyBankException("Sono terminati i consiglieri in banca.");
    }

    /** rimuove dalla banca
     * @param num numero di aiutanti da rimuovere
     * @throws EmptyBankException qualora fossero insufficienti*/
    public boolean getSomeHelpers(int num) throws EmptyBankException {
        if (helpers-num>=0){
            helpers = helpers - num;
            return true;
        }

        else
            throw new EmptyBankException("No More Helpers in Bank.");
    }


    public HashMap<String, List<Empory>> getEmpories() {
        return empories;
    }
    public int getNumberOfEmpory(User user){
        return empories.get(user.getColor().name()).size();
    }

    /** Ritorna un emporio dato il colore utente
     * @param color il colore utente (= il colore emporio)*/
    public Empory getEmpory(UserColor color) throws EmptyBankException{
        List<Empory> selectedEmpories = empories.get(color.name());

        if (!selectedEmpories.isEmpty()){
            int dimList = selectedEmpories.size();
            Empory empory = selectedEmpories.get(dimList-1);
            selectedEmpories.remove(dimList-1);
            return empory;
        }
        else
            throw new EmptyBankException("No More Empories in Bank.");
    }


    /**aggiunge un consigliere alla banca
     * @param councilor il consigliere da aggiungere */
    public void addCouncilor(Councilor councilor){
        String color = councilor.getCouncilorColorName();
        List<Councilor> selectedCouncilor = councilors.get(color);
        selectedCouncilor.add(councilor);
    }

    public void addHelper(int num){
        helpers = helpers + num;
    }

    public int getNumberOfHelper(){
        return helpers;
    }


    public HashMap<String, List<Councilor>> getCouncilors(){
        return councilors;
    }
    @Override
    public String toString() {
        return "Bank{" +
                "councilors=" + councilors + "\n"+
                ", helpers=" + helpers + "\n"+
                ", empories=" + empories +
                '}';
    }
}
