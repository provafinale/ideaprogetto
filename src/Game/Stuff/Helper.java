package Game.Stuff;

import Game.User;
import Server.Market.Buyable;
import java.io.Serializable;

/**
 * Created by edoar on 13/05/2016.
 */
public class Helper implements Buyable,Serializable {
    public static final String HELPER = "Helper";

    @Override
    public Helper getTypeOfObject() {
        return this;
    }

    @Override
    public String getNameOfObject() {
        return HELPER;
    }

    @Override
    public String getDetails() {
        return "1 Aiutante";
    }
}
