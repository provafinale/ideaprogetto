package Game.Stuff;

import Game.Places.Balcony.Balcony;

import java.io.Serializable;

/**
 * Created by edoar on 14/05/2016.
 */
/** Il consigliere */
public class Councilor implements Serializable { // i consiglieri

    CouncilorColor councilorColor;
    Balcony balcony; //NULL se in banca
    boolean satisfied = false;

    public String getCouncilorColorName() {
        return councilorColor.name();
    }

    public CouncilorColor getCouncilorColor(){
        return councilorColor;
    }

    public Councilor(CouncilorColor color){
        this.councilorColor=color;
    }

    @Override
    public String toString() {
        return "Councilor{" +
                "councilorColor=" + councilorColor +
                ", balcony=" + balcony +
                '}';
    }

    public String print(){
        return "Consigliere: " +councilorColor;
    }

    public boolean isSatisfied() {
        return satisfied;
    }

    public void setSatisfied(boolean satisfied) {
        this.satisfied = satisfied;
    }
}
