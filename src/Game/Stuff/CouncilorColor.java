package Game.Stuff;

import Game.Game;

/**
 * Created by Samuele on 20/05/2016.
 */
public enum CouncilorColor {
    VIOLET,
    WHITE,
    BLACK,
    ORANGE,
    PINK,
    BLUE;

    public static CouncilorColor getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
