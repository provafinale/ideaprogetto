package Game.Stuff;

import Game.Places.City;
import Game.UserColor;

import java.io.Serializable;

/**
 * Created by edoar on 14/05/2016.
 */
/** L'emporio. risiede in banca finchè non è assegnato*/
public class Empory  implements Serializable {

    private UserColor color; //Sono associati ad un utente.
    private transient City city; //null se in banca

    public Empory(UserColor color, City city) {
        this.color = color;
        this.city = city;
    }

    public UserColor getColor() {
        return color;
    }


    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Empory{" +
                "color=" + color +
                ", city=" + city +
                '}';
    }
}
