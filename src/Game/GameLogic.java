package Game;

import Game.Cards.BuildingCard;
import Game.Cards.BuildingDeck;
import Game.Cards.ColorPoliticCard;
import Game.Cards.PoliticCard;
import Game.Pawn.King;
import Game.Pawn.NobilityPawn;
import Game.Pawn.Pawn;
import Game.Places.*;
import Game.Places.Tracks.*;
import Game.Stuff.Councilor;
import Game.Stuff.CouncilorColor;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.EmptyBankException;
import Miscellaneous.Exceptions.MoveNotPossibleException;
import Miscellaneous.Exceptions.PoorUserException;
import Server.ServerMatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import static java.lang.Math.abs;

/**
 * Created by Samuele on 24/05/2016.
 */
/** Contiene tutta la logica completa. Tutto lo stato della partita è posizionato in questa classe.*/
public class GameLogic {
    private GameCreator gameCreator = new GameCreator();
    private Bank bank;
    private Queue<PoliticCard> politicDeck = new ArrayBlockingQueue<PoliticCard>(Constant.numPoliticCards);
    private HashMap<String,BuildingDeck> buildingDecks;
    private GameMap gameMap;
    private ArrayList<User> users = new ArrayList<>(); // i giocatori attualmente in partita
    private User turnUser;
    private HashMap<String,List<NobilityPawn>> pawns = new HashMap<>();
    private ServerMatch serverMatch;
    private King king = King.createKing();
    private User winner = null;

    /** Costruisce la logica invocando i metodi che consentono lo startup della partita
     * @param serverMatch il riferimento al Thread del match*/
    public GameLogic(ServerMatch serverMatch){
        generateBank();
        generateMap();
        generatePaws();
        generatePoliticDeck();
        serverMatch.setBank(bank);
        serverMatch.setGameMap(gameMap);
        populateAllBalcony();
        buildingDecks = createBuildingDecks();
        System.out.println("Generati Oggetti di Gioco della Partita.");
        setKingToJuvelar();
        this.serverMatch=serverMatch;
    }

    /**Routine che controlla, ad ogni inizio turno, se le building card scoperte sono a null (son state prese).
     * Se sono null, ne pesca una nuova dal buildingdeck*/
    public void setBuildingCardsToTable(){
        for (RegionName region : RegionName.values()) {
            BuildingDeck buildingDeck = buildingDecks.get(region.name());
            BuildingCard card1 = buildingDeck.getBuildingCardOnTable1();
            BuildingCard card2 = buildingDeck.getBuildingCardOnTable2();
            if (card1 == null){
                card1 = buildingDeck.takeOneCardFromDeck();
                buildingDeck.setBuildingCardOnTable1(card1);
            }
            if (card2 == null) {
                card2 = buildingDeck.takeOneCardFromDeck();
                buildingDeck.setBuildingCardOnTable2(card2);
            }

        }
    }

    /**Inizializza tutte le pedine
     * @param user l'utente di cui inizializzare le pedine*/
    public void setUserPawns(User user){
        //Getto dalle Pawns generate, quali colori sono disponibili.
        ArrayList<String> aviableColor = getAviablePawnColor();
        int size = aviableColor.size();

        //Tra i disponibili, Scelgo l'ultimo.
        String chosenColor = aviableColor.get(size-1);
        aviableColor.remove(size-1);

        //Ottengo la lista delle 3 Pedine dello stesso colore
        List<NobilityPawn> chosenPawn = pawns.get(chosenColor);
        pawns.remove(chosenColor);

        //Setto le 3 pedine
        user.setGoldPawn(chosenPawn.get(0));
        user.setVictoryPawn(chosenPawn.get(1));
        user.setNobilityPawn(chosenPawn.get(2));

        //Assegno di conseguenza un colore all'utente
        UserColor color = user.getGoldPawn().getColor();
        user.setColor(color);
    }

    /** Sposta il re in juvelar come da specifica*/
    public void setKingToJuvelar(){
        City juvelar = getGameMap().getCityFromName(CitiesName.JUVELAR);
        king.setCity(juvelar);
    }

    /** aggiunge il riferimento alla banca per lo user*/
    public void setUserBank(User user){
        user.setBank(bank);
    }

    private ArrayList<String> getAviablePawnColor(){
        ArrayList<String> aviableColor = new ArrayList<>();
        aviableColor.addAll(pawns.keySet());
        return aviableColor;
    }


    /**GETTER METHODS*/

    /**ROUTINE INIT GAME METHODS*/

    /**Imposta Nobility e Victory Pawn di tutti i giocatori, allo slot 0. Imposta Gold Pawn e helpers iniziali.*/
    public void equipUsers(){
        Slot initialVictorySlot = gameMap.getVictoryTrack().getSlotFromNumber(0);
        NobilitySlot initialNobilitySlot = gameMap.getNobilityTrack().getNobilitySlotFromNum(0);

        for (int i = 0; i < users.size(); i++) {
            //VictoryPawn
            Pawn victoryPawn = users.get(i).getVictoryPawn();
            victoryPawn.setSlot(initialVictorySlot);

            //NobilityPawn
            NobilityPawn nobilityPawn = users.get(i).getNobilityPawn();
            nobilityPawn.setSlot(initialNobilitySlot);

            //GoldPawn
            Pawn goldPawn = users.get(i).getGoldPawn();
            Slot initialGoldSlot = gameMap.getGoldTrack().getSlotFromNumber(Constant.START_GOLD_TRACK_POS + i);
            goldPawn.setSlot(initialGoldSlot);

            //Carte Politiche
            ArrayList<PoliticCard> userPoliticCards = new ArrayList<>();
            for (int j=0; j < Constant.START_NUM_POLITIC_CARDS; j++) {
                userPoliticCards.add(politicDeck.poll());
            }
            users.get(i).setPoliticCards(userPoliticCards);
            //Helpers
            try {
                alterNumberOfHelpers(i+1,false,users.get(i));
            } catch (PoorUserException e) {
                e.printStackTrace();
            }
        }
    }

    /** ordina al gamecreator di generare i mazzi di carte permesso per ogni regione*/
    private HashMap<String,BuildingDeck> createBuildingDecks (){
        ArrayList<Region> regions = gameMap.getRegions();
        return gameCreator.generateAllBuildingDeck(regions);
    }


    /**Data una regione, restituisce le building card scoperte
     * @param regionName il nome della regione di cui tornare le carte*/
    public ArrayList<BuildingCard> getBuildingCardsFromRegion(String regionName){
        BuildingDeck buildingDeck = getBuildingDeckFromRegion(regionName);
        ArrayList<BuildingCard> buildingCardsToReturn = new ArrayList<>();
        buildingCardsToReturn.add(buildingDeck.getBuildingCardOnTable1());
        buildingCardsToReturn.add(buildingDeck.getBuildingCardOnTable2());
        return buildingCardsToReturn;
    }

    /**Data una regione, restituisce il buildingdeck (le due carte scoperte, più il mazzo di carte coperte)*/
    public BuildingDeck getBuildingDeckFromRegion(String regionName){
        return buildingDecks.get(regionName);
    }

    private void populateAllBalcony(){
        populateKingBalcony();
        populateRegionBalcony();
    }

    private void populateRegionBalcony(){
        Queue<Councilor> councilorsQueueToAdd;
        ArrayList<Region> regions = gameMap.getRegions();

        for (Region region : regions) {
            councilorsQueueToAdd = takeInitialCouncilorsFromBank();
            region.setBalconyCouncilors(councilorsQueueToAdd);
        }
    }

    private void populateKingBalcony(){
        KingGangplank kingGangplank = gameMap.getKingGangplank();
        Queue<Councilor> councilorQueueToAdd = takeInitialCouncilorsFromBank();

        kingGangplank.setBalconyCouncilors(councilorQueueToAdd);
    }

    //Coda di 4 consiglieri, pescati dalla banca a caso.
    private Queue<Councilor> takeInitialCouncilorsFromBank(){
        Queue<Councilor> councilorsQueueToAdd = new ArrayBlockingQueue<Councilor>(Constant.NUM_COUNCILORS);

        while (councilorsQueueToAdd.size() < Constant.NUM_COUNCILORS) {
            try {
                CouncilorColor randomColor = CouncilorColor.getRandom();
                Councilor selectedCouncilor = bank.getCouncilor(randomColor);
                councilorsQueueToAdd.add(selectedCouncilor);
            } catch (EmptyBankException e){
                System.out.println();
            }
        }
        return councilorsQueueToAdd;
    }

    /** Prende una carta permesso per l'utente
     * @param user l'utente a cui assegnare la carte
     * @param buildingCard la carta permesso da aggiungere all'utente*/
    public void takeBuildingCardForClient(User user, BuildingCard buildingCard){
        ArrayList<BuildingCard> userBuilding = user.getBuildingCards();
        userBuilding.add(buildingCard); //Aggiunge la carta
        user.setBuildingCards(userBuilding);
    }

    // *** ALTERS  ***
    /** Rimuove le carte politiche di quel colore*/
    public PoliticCard removePoliticCards(ColorPoliticCard color, User user){
        for (PoliticCard selectedCard : user.getPoliticCards()) {
            String selectedColor = selectedCard.getColor();
            if (selectedColor.equals(color.name())){
                user.getPoliticCards().remove(selectedCard);
                return selectedCard;
            }
        }
        return null;
    }

    public void moveKing(City city){
        king.setCity(city);
    }
    public City getPosKing(){
        return king.getCity();
    }

    /** Rimuove la carta permesso dal detentore*/
    public BuildingCard removeBuildingCard(BuildingCard buildingCard){
        String cardClient = buildingCard.getInitials();
        for (RegionName region : RegionName.values()) {
            BuildingDeck buildingDeck = buildingDecks.get(region.name());

            //Se le iniziali delle carte del mazzo selezionato sono uguali alla carta del client:
            if (buildingDeck.getBuildingCardOnTable1().getInitials().equals(cardClient)){
                BuildingCard buildToReturn = buildingDeck.getBuildingCardOnTable1();
                buildingDeck.setBuildingCardOnTable1(null);
                buildingDeck.refreshCardOnTable();
                return buildToReturn;
            }
            if (buildingDeck.getBuildingCardOnTable2().getInitials().equals(cardClient)){
                BuildingCard buildToReturn = buildingDeck.getBuildingCardOnTable2();
                buildingDeck.setBuildingCardOnTable2(null);
                buildingDeck.refreshCardOnTable();
                return buildToReturn;
            }
        }
        return null;
    }
    /** modifica il numero di azioni principali disponibili
     * @param user l'utente da alterare
     * @param  increment la differenza*/
    public void alterNumberMainAction(User user, int increment){
        user.setNumMainAction(user.getRemainingMainActions()+increment);

    }

    /** Modifica il numero di aiutanti per l'utente
     * @param num di quanto alterare
     * @param toBank se vero, riposiziona gli aiutanti in banca
     * @param user l'utente da alterare*/
    public void alterNumberOfHelpers(int num, boolean toBank, User user) throws PoorUserException {
        int userHelpers = user.getHelpers();
        //se sto guadagnando aiutanti
        if (num >0){
            try {
                if (bank.getSomeHelpers(num)){
                    user.alterHelpers(num);}
            } catch (EmptyBankException e) {
                e.printStackTrace();}
        }

        //se sto spendendo aiutanti ma non ne ho abbastanza
        if (num < 0 && userHelpers < -num) {
            System.out.println("caso2");
            throw new PoorUserException(user.getName(), "Non ha abbastanza aiutanti");
        }

        //se sto spendendo aiutanti ma ne ho abbastanza
        if (num < 0 && userHelpers >= -num) {
            if (toBank) {  //se comporta il reinserimento in banca:
                user.alterHelpers(num);
                bank.addHelper(abs(num)); //aggiungili anche in banca
            } else {
                user.alterHelpers(num);
            }
        }
    }
    /**Modifica il numero di monete per l'utente
     * @param goldsDiff la differenza di monete
     * @param  user l'utente da modificare*/
    public void alterGolds(int goldsDiff, User user) throws PoorUserException{
        GoldTrack goldTrack = gameMap.getGoldTrack();
        try {
            goldTrack.go(user, goldsDiff); //try to move the goldPawn
        } catch (MoveNotPossibleException moveExc) {
            moveExc.printStackTrace();
            throw  new PoorUserException(user.getName(), moveExc);
        }
    }
    /**Modifica il percorso della nobiltà per l'utente
     * @param  nobilityDiff la differenza
     * @param  user l'utente da modificare*/
    public void alterNobility(int nobilityDiff, User user) throws PoorUserException {
        NobilityTrack nobilityTrack = gameMap.getNobilityTrack();
        try {
            nobilityTrack.go(user,nobilityDiff);
        } catch (MoveNotPossibleException e) {
            e.printStackTrace();
            throw new PoorUserException(user.getName(), e);
        }
    }

    /** Modifica il percorso della vittoria per l'utente
     * @param  victoryDiff la differenza
     * @param user l'utente da modificare*/
    public void alterVictory(int victoryDiff, User user) throws PoorUserException {
        VictoryTrack victoryTrack = gameMap.getVictoryTrack();
        try {
            victoryTrack.go(user, victoryDiff);
        } catch (MoveNotPossibleException e) {
            e.printStackTrace();
            throw new PoorUserException(user.getName(), e);
        }
    }
    /** Modifica le carte politiche dell'utente spilandone dal mazzo quante definite dalla diff
     * @param politicDiff il numero di carte da aggiungere
     * @param user l'utente a cui assegnare le carte*/
    public void alterPolitic(int politicDiff, User user){
        for (int i =0; i<politicDiff; i++){
            user.addPoliticCard(getPoliticCard());
        }
    }

    /** Aggiunge la carta politica al mazzo logico
     * @param  politicCardsToAdd la carta polita da aggiungere al mazzo*/
    public void addPoliticCards(ArrayList<PoliticCard> politicCardsToAdd){
        politicDeck.addAll(politicCardsToAdd);
    }

    public PoliticCard getPoliticCard(){
        return politicDeck.remove();
    }



    private void generatePoliticDeck(){
        politicDeck.addAll(gameCreator.createPoliticDeck());
    }

    private void generateMap(){
        gameMap = gameCreator.generateMap();
    }

    private void generateBank(){
        bank = gameCreator.generateBank();
    }

    private void generatePaws(){
        pawns =gameCreator.createPaws();
    }


    public void addUser(User user){
        users.add(user);
    }


    public GameMap getGameMap() {
        return gameMap;
    }

    public Bank getBank() {
        return bank;
    }
    public ServerMatch getServerMatch(){
        return serverMatch;
    }

    public ArrayList<User> getUsers(){
        return users;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }
}

