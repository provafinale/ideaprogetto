package Game;

import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Pawn.NobilityPawn;
import Game.Pawn.Pawn;

import java.io.Serializable;
import java.util.ArrayList;

/** L'utente. Contiene la situazione aggiornata del giocatore*/
public class User implements Serializable{


    // USER-SPECIFIC CONFIGURATION
    private UserColor color;
    private String name;
    private int numMainAction;
    private int numFastAction;
    private Bank bank;
    private boolean isMyTurn;
    private boolean isConnected = true;


    //USER-SPECIFIC GAME-OBJS
    private int helpers; //Gli aiutanti di ogni giocatore
    private ArrayList<PoliticCard> politicCards; //le carte politiche di ogni giocatore
    private ArrayList<BuildingCard> buildingCards = new ArrayList<>(); //le carte permesso di costruzione di ogni giocatore

    //THE PAWNS
        private Pawn victoryPawn;
        private Pawn goldPawn;
        private NobilityPawn nobilityPawn;

    //CONSTRUCTOR
    public User(String readedName){
        this.name=readedName;
        isMyTurn = false;
    }


    //SETTER
    public void setUserName(String name){
        this.name=name;
    }
    public void setIsMyTurn(boolean setTurn){
        this.isMyTurn = setTurn;
    }
    public void addPoliticCard(PoliticCard card){
        politicCards.add(card);
    }

    public void setPoliticCards(ArrayList<PoliticCard> politicCards){
        this.politicCards = politicCards;
    }

    public void setVictoryPawn(NobilityPawn victoryPawn) {
        this.victoryPawn = victoryPawn;
    }

    public void setGoldPawn(NobilityPawn goldPawn) {
        this.goldPawn = goldPawn;
    }

    public void setNobilityPawn(NobilityPawn nobilityPawn) {
        this.nobilityPawn = nobilityPawn;
    }

    public void setNumMainAction(int numMainAction) {
        this.numMainAction = numMainAction;
    }
    public void setNumFastAction(int numFastAction) {
        this.numFastAction = numFastAction;
    }

    public void setColor(UserColor color) {
        this.color = color;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public void setHelpers(int helpers) {
        this.helpers = helpers;
    }

    public void alterHelpers(int num){
        helpers = helpers + num;
    }

    //GETTER

    /**@return true se è il turno di questo giocatore */
    public boolean getIsMyTurn(){
        return isMyTurn;
    }

    public BuildingCard getAndRemoveBuildingCard(BuildingCard buildingCard){
        for (int i = 0; i < buildingCards.size(); i++) {
            if (buildingCards.get(i).compareBuilding(buildingCard)) {
                BuildingCard buildingToReturn = buildingCards.get(i);
                buildingCards.remove(i);
                return buildingToReturn;
            }
        }
        return null;
    }


    public int getPosVictoryPawn(){
        return victoryPawn.getSlot().getNumber();
    }

    public int getPosGoldPawn(){
        return goldPawn.getSlot().getNumber();
    }

    public int getPosNobilityPawn(){
        return nobilityPawn.getSlot().getNumber();
    }

    public Pawn getGoldPawn() {
        return goldPawn;
    }

    public NobilityPawn getNobilityPawn() {
        return nobilityPawn;
    }

    public Pawn getVictoryPawn() {
        return this.victoryPawn;
    }

    public ArrayList<BuildingCard> getBuildingCards(){
        return buildingCards;
    }

    public ArrayList<PoliticCard> getPoliticCards() {
        return politicCards;
    }

    public int getHelpers() {
        return helpers;
    }

    public String getName() {
        return name;
    }



    public int getNumberOfPoliticCards(){
        return politicCards.size();}

    public UserColor getColor() {
        return color;
    }


    public int getRemainingMainActions() {
        return numMainAction;
    }
    public int getRemainingFastActions() {
        return numFastAction;
    }


    public void setBuildingCards(ArrayList<BuildingCard> buildingCards) {
        this.buildingCards = buildingCards;
    }


    @Override
    public String toString() {
        return "User{" +
                "color=" + color +
                ", name='" + name + '\'' +
                ", numMainAction=" + numMainAction +
                ", numFastAction=" + numFastAction +
                ", bank=" + bank +
                ", isMyTurn=" + isMyTurn +
                ", helpers=" + helpers +
                ", politicCards=" + politicCards +
                ", buildingCards=" + buildingCards +
                ", victoryPawn=" + victoryPawn +
                ", goldPawn=" + goldPawn +
                ", nobilityPawn=" + nobilityPawn +
                '}';
    }

    public String print(){
        return
        "Colore: " +color +
                " Nome: " + name +
                " Numero di aiutanti: " +helpers +
                " Carte politiche: " +
                "\n"+
                " Carte permesso: " +printBuildings();
    }
    private String printBuildings(){
        String buildingsStrings = null;
        for (BuildingCard card: buildingCards) {
            buildingsStrings = buildingsStrings + card.softPrint();
        }
        return buildingsStrings;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
        if (!connected)
            System.out.println(this.getName()+" si è disconnesso");
    }
    public boolean getisConnected(){
        return isConnected;
    }

}
