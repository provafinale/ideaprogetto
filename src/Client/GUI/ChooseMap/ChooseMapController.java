package Client.GUI.ChooseMap;

import Client.ClientManager;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Samuele on 31/05/2016.
 */
public class ChooseMapController implements Initializable{
    @FXML private JFXButton goButton;
    @FXML private JFXButton leftButton;
    @FXML private JFXButton rightButton;
    @FXML private ImageView mapImage;
    @FXML private ImageView mapCanva;

    private ClientManager clientManager;
    private String selectedMap;
    private String pathSelectedMap;

    //Chiave NOMEMAPPA ;; Valore PATHMAPPA
    private ArrayList<String> maps = new ArrayList<>();
    private int current=0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        maps.add("map1");
        maps.add("map2");


        goButton.getStyleClass().add("button-raised");
        leftButton.getStyleClass().addAll("button-raised", "nexter");
        rightButton.getStyleClass().addAll("button-raised", "nexter");

        pathSelectedMap = "maps/images/"+maps.get(0)+".jpg";
        File canvaImg = new File("maps/images/Misc/chooseMap.png");
        Image image = new Image(canvaImg.toURI().toString());
        mapCanva.setImage(image);

        setImage();
    }

    public void onClickLeft(){
        selectedMap = getNewMap();
        pathSelectedMap = "maps/images/"+selectedMap+".jpg";
        setImage();
    }

    public void onClickRight(){
        selectedMap = getNewMap();
        pathSelectedMap = "maps/images/"+selectedMap+".jpg";
        setImage();

    }

    public void onClickLetsGo(){
        String selectedMap = maps.get(current);
        clientManager.sendChosenMap(selectedMap);

    }

    private void setImage(){
        File file = new File(pathSelectedMap);
        Image image = new Image(file.toURI().toString());
        mapImage.setImage(image);
    }


    private String getNewMap(){
        if (current == maps.size()-1){
            current=0;
        }
        else
            current++;
        return maps.get(current);
    }

    public void setClientManager(ClientManager client){
        this.clientManager = client;}


}
