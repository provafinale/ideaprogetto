package Client.GUI.Game.Situation;

import Client.ClientMatch;
import Game.Enemy;
import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Samuele on 06/07/2016.
 */
public class SituationController implements Initializable {
    @FXML private ChoiceBox<String> choiceBox;

    @FXML private ImageView politicImage;
    @FXML private ImageView helperImage;
    @FXML private ImageView victoryImage;
    @FXML private ImageView goldImage;
    @FXML private ImageView nobilityImage;
    @FXML private Label politicText;
    @FXML private Label helperText;
    @FXML private Label victoryText;
    @FXML private Label goldText;
    @FXML private Label nobilityText;
    private ClientMatch clientMatch;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("maps/images/helper.png");
        helperImage.setImage(new Image(file.toURI().toString(),250,150,true,true,true));
        file = new File("maps/gameUI/shield.png");
        victoryImage.setImage(new Image(file.toURI().toString(),250,150,true,true,true));
        file = new File("maps/gameUI/nobility.png");
        nobilityImage.setImage(new Image(file.toURI().toString(),250,150,true,true,true));
        file = new File("maps/gameUI/gold.png");
        goldImage.setImage(new Image(file.toURI().toString(),250,150,true,true,true));
        file = new File("maps/images/politicsCard/WHITE.png");
        politicImage.setImage(new Image(file.toURI().toString(),250,150,true,true,true));
        politicText.getStyleClass().add("detailsText");
        helperText.getStyleClass().add("detailsText");
        victoryText.getStyleClass().add("detailsText");
        goldText.getStyleClass().add("detailsText");
        nobilityText.getStyleClass().add("detailsText");
        choiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            setEnemyInformation(newValue);});
    }
    public void setChoiceBox(String string){
        choiceBox.getItems().add(string);
    }

    public void setClientMatch(ClientMatch clientMatch){
        this.clientMatch = clientMatch;
    }

    private void setEnemyInformation(String userName){
        Enemy enemy = clientMatch.getEnemyFromName(userName);
        if (enemy != null){
            Platform.runLater(() -> {
                politicText.setText(String.valueOf(enemy.getNumPoliticCards()));
                helperText.setText(String.valueOf(enemy.getNumHelpers()));
                victoryText.setText(String.valueOf(enemy.getPosVictoryPawn()));
                nobilityText.setText(String.valueOf(enemy.getPosNobilityPawn()));
                goldText.setText(String.valueOf(enemy.getPosGoldPawn()));
            });

        }
    }



}
