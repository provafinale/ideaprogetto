package Client.GUI.Game;

import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Samuele on 23/06/2016.
 */
public class GaugeTimer implements Runnable {
    Listener listener;
    boolean runningTimer = true;

    public GaugeTimer(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void run() {
        listener.updateGauge();
    }

}
