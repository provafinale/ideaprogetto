package Client.GUI.Game.Market;

import Client.ClientMarket;
import Client.ClientMatch;
import Client.GUI.Game.BankView.BankView;
import Client.GUI.Game.GameController;
import Client.GUI.Game.Handlers.MarketHandler.ForSaleObjectHandler;
import Client.Inteface.MarketInterface;
import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Stuff.Helper;
import Server.Market.Buyable;
import Server.Market.BuyableObject;
import com.jfoenix.controls.JFXButton;
import eu.hansolo.medusa.Gauge;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import org.controlsfx.control.PopOver;

import java.io.File;
import java.net.URL;
import java.util.*;

/**
 * Created by Samuele on 28/06/2016.
 */
public class MarketController implements Initializable {
    private ClientMatch clientMatch;
    private ClientMarket clientMarket;
    private GameController gameController;
    private TableColumn<BuyableObject,String> userColumn;
    private TableColumn<BuyableObject,String> objectColumn;
    private TableColumn<BuyableObject,String> priceColumn;
    private BuyableObject objectSelected = null;
    PopOver detailsPopOver;

    @FXML private AnchorPane gaugeTimer;
    @FXML private ImageView marketImage;
    @FXML private TableView<BuyableObject> table;
    @FXML private JFXButton buyButton;
    @FXML private StackPane buildingPane;
    @FXML private HBox politicHbox;
    @FXML private ImageView helperImage;
    @FXML private Text helperText;
    @FXML private AnchorPane timerPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Css Provvisorio
        buyButton.setStyle("" +
                "-fx-font-size: 12px;\n" +
                "    -fx-button-type: RAISED;\n" +
                "    -fx-background-color: rgb(142, 204, 111);\n" +
                "    -fx-pref-width: 80;\n" +
                "    -fx-text-fill: WHITE;");

        File file = new File("maps/images/market.png");
        Image image = new Image(file.toURI().toString(),300,300,true,true,true);
        marketImage.setImage(image);
        file = new File("maps/images/helper.png");
        helperImage.setImage(new Image(file.toURI().toString(),40,40,true,true,true));
        helperImage.setOnMouseClicked(new ForSaleObjectHandler(new Helper(),this));
        table.setMouseTransparent(false);
        table.setEditable(false);
        table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        userColumn = new TableColumn<>("Utente");
        userColumn.setCellValueFactory(new PropertyValueFactory<>("userName"));
        objectColumn = new TableColumn<>("Oggetto");
        objectColumn.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getObject().getNameOfObject()));
        priceColumn = new TableColumn<>("Prezzo");
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        table.getColumns().addAll(userColumn,objectColumn,priceColumn);
        table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            objectSelected=newValue;
            if (objectSelected != null) {
                Pane popPane = new Pane();
                detailsPopOver = new PopOver();
                detailsPopOver.setContentNode(popPane);
                detailsPopOver.setTitle("Dettagli");
                Label label = new Label(objectSelected.getDetails());
                popPane.getChildren().add(label);
                detailsPopOver.show(table);
            }
            else if (objectSelected == null)
                detailsPopOver.hide();
        });

    }

    public void updateMarketGUI(){
        Platform.runLater(()-> {
            this.clientMarket = clientMatch.getClientMarket();
            table.setItems(getBuyableObjects());});
    }

    public ObservableList<BuyableObject> getBuyableObjects(){
        ObservableList<BuyableObject> products = FXCollections.observableArrayList();
        HashMap<String, ArrayList<BuyableObject>> market = clientMarket.getClientMarket();
        for (Map.Entry<String, ArrayList<BuyableObject>> entry : market.entrySet()) {
            ArrayList<BuyableObject> userMarket = clientMarket.getElementFromMarket(entry.getKey());
            products.addAll(userMarket);
        }
        return products;
    }

    public void disposeItemsToSell(){
        Platform.runLater(()-> {
            buildingPane.getChildren().clear();
            politicHbox.getChildren().clear();
            ArrayList<BuildingCard> buildingCards = clientMatch.getUser().getBuildingCards();
            ArrayList<PoliticCard> politicCards = clientMatch.getUser().getPoliticCards();

            for (BuildingCard buildingCard : buildingCards) {
                buildingPane.getChildren().add(BankView.getBuldingCards(clientMatch.getUser(), buildingCard, this,gameController));
            }
            for (PoliticCard politicCard : politicCards) {
                File file = new File("maps/images/politicsCard/" + politicCard.getColor() + ".png");
                Image image = new Image(file.toURI().toString(), 100, 100, true, true, true);
                ImageView imageView = new ImageView(image);
                imageView.setOnMouseClicked(new ForSaleObjectHandler(politicCard, this));
                imageView.setOnMouseEntered(event -> gameController.scaleTransitionPos(event,1.10));
                imageView.setOnMouseExited(event -> gameController.scaleTransitionPos(event,1));
                politicHbox.getChildren().add(imageView);
            }
            int helpers = clientMatch.getUser().getHelpers();
            helperText.setText(String.valueOf(helpers));
        });
    }


    public void onClickBuyButton(){
        if (clientMarket.canBuy() && objectSelected!=null) {
            clientMatch.buyItem(objectSelected.getId());
            table.getSelectionModel().clearSelection();
            objectSelected = null;
        }
        else {
            gameController.showException("Non puoi acquistare in questo momento.");
        }
    }

    public void addItemToMarket(Buyable buyable, int price){
        clientMatch.sellItem(buyable,price);
        System.out.println("Vendo "+buyable);
    }

    public void buyItemFromMarket(int id){
        clientMatch.buyItem(id);
    }

    public void setClientMatch(ClientMatch clientMatch){
        this.clientMatch = clientMatch;
        this.clientMarket = clientMatch.getClientMarket();
        clientMatch.addMarketListener(new MarketInterface() {
            @Override
            public void updateMarket() {
                updateMarketGUI();
                disposeItemsToSell();
            }
        });
    }


    //Popover per il prezzo
    public int setItemPrice(){
        Dialog<Integer> dialog = new Dialog<>();
        dialog.setTitle("Scegli il prezzo");
        dialog.setHeaderText("Prezzo di vendita");
        File file = new File("maps/gameUI/gold.png");
        Image image = new Image(file.toURI().toString(),50,50,true,true);
        dialog.setGraphic(new ImageView(image));
        ButtonType confirmButtonType = new ButtonType("Conferma", ButtonBar.ButtonData.OK_DONE);

        dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);
        GridPane pane = new GridPane();
        pane.setHgap(10);
        pane.setVgap(10);

        Spinner<Integer> spinner = new Spinner<>();
        spinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,10,5));
        spinner.setEditable(true);
        pane.add(spinner,0,0);
        pane.setAlignment(Pos.CENTER);
        dialog.getDialogPane().setContent(pane);
        dialog.setResultConverter(dialogButton->{
            if (dialogButton == confirmButtonType){
                return spinner.getValue();
            }
            return null;
        });
        Optional<Integer> price = dialog.showAndWait();
        return price.orElse(0);
    }

    public void setGameController(GameController gameController){
        this.gameController = gameController;
    }

    public void setGauge(Gauge gauge) {
        gaugeTimer.getChildren().add(gauge);
    }
}
