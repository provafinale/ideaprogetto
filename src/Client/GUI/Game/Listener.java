package Client.GUI.Game;

/**
 * Created by Samuele on 02/06/2016.
 */
public interface Listener {
    void onReceiveSnapshot();
    void updateTurn(String userInTurn);
    void setUsernameText(String userName);
    void setCitiesOnUI();
    void updateGauge();
    void onTimerExceed();
    void startMarket();
    void startBuyMarket();
    void endMarket();

}
