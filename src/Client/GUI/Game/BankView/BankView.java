package Client.GUI.Game.BankView;

import Client.GUI.Game.GameController;
import Client.GUI.Game.Handlers.GameHandler.BuildingCardHandler;
import Client.GUI.Game.Handlers.GameHandler.CouncilorHandler;
import Client.GUI.Game.Handlers.GameHandler.EmporyHandler;
import Client.GUI.Game.Handlers.GameHandler.BuildingDeckHandler;
import Client.GUI.Game.Handlers.MarketHandler.ForSaleObjectHandler;
import Client.GUI.Game.Market.MarketController;
import Game.Bank;
import Game.Cards.BuildingCard;
import Game.Stuff.Councilor;
import Game.Stuff.CouncilorColor;
import Game.Stuff.Empory;
import Game.User;
import Game.UserColor;
import Server.Market.Buyable;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Samuele on 19/06/2016.
 */
public class BankView {
    private Bank bank;
    private GameController gameController;

    private HashMap<String,List<Councilor>> councilors;
    private int helpers;
    private HashMap<String,List<Empory>> empories;
    private User user;
    HBox councilorHbox = new HBox();
    HBox emporyHbox = new HBox();
    HBox buildingCardsHbox = new HBox();
    Stage bankStage;
    ExecutorService executorService = Executors.newCachedThreadPool();

    public BankView (Bank bank, GameController gameController, User user,Stage bankStage) {
        this.bank = bank;
        this.gameController = gameController;
        this.user = user;
        this.bankStage = bankStage;

        councilors = bank.getCouncilors();
        helpers = bank.getNumberOfHelper();
        empories = bank.getEmpories();
    }

    public BankView(User user) {
        this.user = user;
    }

    public static HBox getBuldingCards(User user, Buyable buyable, MarketController marketController,GameController gameController){
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        ArrayList<BuildingCard> buildingCards = user.getBuildingCards();
        for (BuildingCard buildingCard : buildingCards) {
            StackPane cardPane = new StackPane();
            HBox bonusHbox = new HBox();
            bonusHbox.setAlignment(Pos.CENTER);
            File file = new File("maps/images/buildingCard/BACK.png");
            Image image = new Image(file.toURI().toString(),160,100,true,true,true);
            ImageView backBuildingImage = new ImageView(image);

            Text citiesName = new Text(buildingCard.getInitials());
            cardPane.getChildren().addAll(backBuildingImage, citiesName);
            ArrayList<Integer> numRewards = buildingCard.getNumbersOfRewards();
            ArrayList<String> typeOfRewards = buildingCard.getTypesOfRewards();
            //AnchorPane anchorPane = new AnchorPane();
            for (int i = 0; i < numRewards.size(); i++) {
                StackPane stackPane = new StackPane();
                Text bonusText = new Text(String.valueOf(numRewards.get(i)));
                file = new File("maps/images/rewards/" + typeOfRewards.get(i) + ".png");
                image = new Image(file.toURI().toString(),30,25,true,true,true);
                ImageView bonus = new ImageView(image);
                stackPane.getChildren().addAll(bonus, bonusText);
                //anchorPane.getChildren().add(stackPane);
                bonusHbox.getChildren().add(stackPane);
            }
            cardPane.setOnMouseEntered(event -> gameController.scaleTransitionPos(event,1.1));
            cardPane.setOnMouseExited(event -> gameController.scaleTransitionPos(event,1));

            cardPane.getChildren().add(bonusHbox);
            cardPane.setAlignment(citiesName,Pos.TOP_CENTER);
            cardPane.setMargin(citiesName,new Insets(65,0,0,0));
            cardPane.setAlignment(bonusHbox, Pos.BOTTOM_CENTER);
            cardPane.setMargin(bonusHbox,new Insets(10,0,0,0));
            cardPane.setOnMouseClicked(new ForSaleObjectHandler(buyable, marketController));
            hBox.getChildren().add(cardPane);
        }
        return hBox;
    }

    public void showBank(boolean isForAction) {
        bankStage = new Stage();
        gameController.setBankStage(bankStage);
        bankStage.setTitle("Stato della Banca");
        StackPane bankPane = new StackPane();
        VBox vBox = new VBox();
        StackPane helperPane = new StackPane();

        Scene scene = new Scene(bankPane);
        bankStage.setScene(scene);
        //Building card
        ArrayList<BuildingCard> buildingCards = user.getBuildingCards();
        for (BuildingCard buildingCard : buildingCards) {
            StackPane cardPane = new StackPane();
            HBox bonusHbox = new HBox();
            ImageView backBuildingImage = getImageView("maps/images/buildingCard/BACK.png", 100, 60);

            Text citiesName = new Text(buildingCard.getInitials());
            cardPane.getChildren().addAll(backBuildingImage, citiesName);
            ArrayList<Integer> numRewards = buildingCard.getNumbersOfRewards();
            ArrayList<String> typeOfRewards = buildingCard.getTypesOfRewards();
            //AnchorPane anchorPane = new AnchorPane();
            for (int i = 0; i < numRewards.size(); i++) {
                StackPane stackPane = new StackPane();
                Text bonusText = new Text(String.valueOf(numRewards.get(i)));
                ImageView bonus = getImageView("maps/images/rewards/" + typeOfRewards.get(i) + ".png", 25, 20);
                stackPane.getChildren().addAll(bonus, bonusText);
                //anchorPane.getChildren().add(stackPane);
                bonusHbox.getChildren().add(stackPane);
            }

            cardPane.getChildren().add(bonusHbox);
            cardPane.setAlignment(citiesName, Pos.TOP_CENTER);
            cardPane.setAlignment(bonusHbox, Pos.BOTTOM_CENTER);
            cardPane.setOnMouseClicked(new BuildingCardHandler(buildingCard, gameController));
            cardPane.setOnMouseEntered(event -> gameController.scaleTransitionPos(event, 1.05));
            cardPane.setOnMouseExited(event -> gameController.scaleTransitionPos(event, 1));
            buildingCardsHbox.getChildren().add(cardPane);
        }

        //Helpers
        File helperFile = new File("maps/images/helper.png");
        Image helperImage = new Image(helperFile.toURI().toString(), 150, 100, true, true);
        ImageView helperImageView = new ImageView(helperImage);
        Label helperLabel = new Label(String.valueOf(helpers));
        helperLabel.getStyleClass().add("detailsText");
        helperPane.getChildren().addAll(helperImageView, helperLabel);

        //Councilor
        for (CouncilorColor color : CouncilorColor.values()) {
            Label councilorLabel = new Label(getNumberOfCouncilorColor(color.name()));
            councilorLabel.getStyleClass().add("detailsText");
            File councilorFile = new File("maps/images/councilor/" + color.name() + ".png");
            Image image = new Image(councilorFile.toURI().toString(), 150, 100, true, true);
            ImageView imageView = new ImageView(image);
            StackPane pane = new StackPane();
            pane.getChildren().addAll(imageView, councilorLabel);
            if (isForAction)
                pane.setOnMouseClicked(new CouncilorHandler(gameController, new Councilor(color)));
            councilorHbox.getChildren().add(pane);
        }

        //Empories
        for (UserColor color : UserColor.values()) {
            Label emporyLabel = new Label(getNumberOfEmpory(color.name()));
            emporyLabel.getStyleClass().add("detailsText");
            File emporyFile = new File("maps/images/empories/" + color.name() + ".png");
            Image image = new Image(emporyFile.toURI().toString(), 100, 50, true, true);
            ImageView imageView = new ImageView(image);
            StackPane pane = new StackPane();
            pane.getChildren().addAll(imageView, emporyLabel);
            if (isForAction)
                pane.setOnMouseClicked(new EmporyHandler(new Empory(color, null), gameController));
            emporyHbox.getChildren().add(pane);
        }


        vBox.getChildren().addAll(helperPane, emporyHbox, buildingCardsHbox, councilorHbox);
        vBox.setAlignment(Pos.CENTER);
        emporyHbox.setAlignment(Pos.CENTER);
        buildingCardsHbox.setAlignment(Pos.CENTER);
        vBox.setSpacing(3);
        bankPane.getChildren().add(vBox);
        bankPane.setAlignment(vBox, Pos.CENTER);
        bankStage.show();
    }

    private ImageView getImageView(String url,int a, int l){
        File file = new File(url);
        Image image = new Image(file.toURI().toString(),a,l,true,true);
        ImageView imageView = new ImageView(image);
        return imageView;
    }


    private String getNumberOfCouncilorColor(String key){
        List<Councilor> councilorsList = councilors.get(key);
        return String.valueOf(councilorsList.size());
    }

    private String getNumberOfEmpory(String key){
        List<Empory> emporiesList = empories.get(key);
        return String.valueOf(emporiesList.size());
    }


    public HBox getCouncilorHbox() {
        return councilorHbox;
    }

    public HBox getEmporyHbox() {
        return emporyHbox;
    }

    public HBox getBuildingCardsHbox() {
        return buildingCardsHbox;
    }


}
