package Client.GUI.Game.Handlers.MarketHandler;

import Client.GUI.Game.Market.MarketController;
import Server.Market.Buyable;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Created by Samuele on 29/06/2016.
 */
/**Handler che viene associato agli oggetti che possono essere messi in vendita*/
public class ForSaleObjectHandler implements EventHandler<MouseEvent> {
    private Buyable buyable;
    private int price;
    private MarketController marketController;

    public ForSaleObjectHandler(Buyable buyable, MarketController marketController) {
        this.buyable = buyable;
        this.marketController = marketController;
    }

    @Override
    public void handle(MouseEvent event) {
        price = marketController.setItemPrice();
        if (price>0)
            marketController.addItemToMarket(buyable,price);



    }

    public void setPrice(int price){
        if (price>0)
            this.price = price;
    }


}
