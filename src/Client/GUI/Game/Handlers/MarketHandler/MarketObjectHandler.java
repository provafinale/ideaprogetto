package Client.GUI.Game.Handlers.MarketHandler;

import Client.GUI.Game.Market.MarketController;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Created by Samuele on 29/06/2016.
 */
/**Handler associato agli oggetti messi in vendita. Ora l'oggetto lo considero solo come ID.*/
public class MarketObjectHandler implements EventHandler<MouseEvent> {
    private int idItem;
    private MarketController marketController;

    public MarketObjectHandler(int idItem, MarketController marketController) {
        this.idItem = idItem;
        this.marketController = marketController;
    }

    @Override
    public void handle(MouseEvent event) {
        marketController.buyItemFromMarket(idItem);
    }
}
