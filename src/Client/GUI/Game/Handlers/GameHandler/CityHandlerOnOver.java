package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Places.City;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * Created by Samuele on 06/07/2016.
 */
public class CityHandlerOnOver  implements EventHandler<MouseEvent> {
    City city;
    GameController gameController;
    AnchorPane cityPane;
    public CityHandlerOnOver(City city, GameController gameController,AnchorPane cityPane) {
        this.city = city;
        this.gameController = gameController;
        this.cityPane = cityPane;
    }

    @Override
    public void handle(MouseEvent event) {

        if (gameController.isFocus())
            gameController.createCityDialog(city.getName(),cityPane);
    }
}
