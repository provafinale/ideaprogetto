package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Stuff.Councilor;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;


/**
 * Created by Samuele on 20/06/2016.
 */
public class CouncilorHandler implements EventHandler<MouseEvent> {
    GameController gameController;
    Councilor councilor;


    public CouncilorHandler(GameController gameController, Councilor councilor) {
        this.gameController = gameController;
        this.councilor = councilor;
    }

    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().setCouncilorForAction(councilor);
    }
}
