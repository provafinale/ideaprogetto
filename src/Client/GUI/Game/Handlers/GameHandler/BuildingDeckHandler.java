package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Cards.BuildingDeck;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.PopOver;

/**
 * Created by Samuele on 26/06/2016.
 */
public class BuildingDeckHandler implements EventHandler<MouseEvent> {
    String buildingDeck;
    GameController gameController;
    PopOver popDeck;

    public BuildingDeckHandler(String buildingDeck, GameController gameController, PopOver popDeck) {
        this.buildingDeck = buildingDeck;
        this.gameController = gameController;
        this.popDeck = popDeck;
    }

    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().setBuildingDeck(buildingDeck);
        popDeck.hide();
    }
}
