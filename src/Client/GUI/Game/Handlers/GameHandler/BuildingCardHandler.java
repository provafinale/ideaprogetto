package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Cards.BuildingCard;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Created by Samuele on 17/06/2016.
 */
public class BuildingCardHandler implements EventHandler<MouseEvent> {
    private BuildingCard buildingCard;
    private GameController gameController;
    private Pane pane;

    public BuildingCardHandler(BuildingCard buildingCard, GameController gameController) {
        this.buildingCard = buildingCard;
        this.gameController = gameController;
    }
    public BuildingCardHandler(BuildingCard card, GameController controller, Pane pane){
        this.buildingCard = card;
        this.gameController = controller;
        this.pane = pane;
    }

    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().setBuildingCardForAction(buildingCard, pane);
    }
}
