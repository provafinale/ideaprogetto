package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Cards.PoliticCard;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Created by Samuele on 17/06/2016.
 */
public class PoliticCardHandler implements EventHandler<MouseEvent> {
    private PoliticCard politicCard;
    private GameController gameController;
    private ImageView imageView;

    public PoliticCardHandler(PoliticCard politicCard, GameController gameController) {
        this.politicCard = politicCard;
        this.gameController = gameController;
    }
    public PoliticCardHandler(PoliticCard card, GameController gameController, ImageView imageView){
        this.politicCard = card;
        this.gameController = gameController;
        this.imageView = imageView;
    }

    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().setPoliticCardsForAction(politicCard, imageView);

    }
}
