package Client.GUI.Game.Handlers.GameHandler;

import Client.ClientMatch;
import Client.GUI.Game.GameController;
import Game.Places.City;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * Created by Samuele on 18/06/2016.
 */
public class CityHandler implements EventHandler<MouseEvent> {
    City city;
    GameController gameController;
    AnchorPane cityPane;
    public CityHandler(City city, GameController gameController,AnchorPane cityPane) {
        this.city = city;
        this.gameController = gameController;
        this.cityPane = cityPane;
    }

    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().addCity(city);
    }
}
