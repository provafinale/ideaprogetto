package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Places.Balcony.Balcony;
import Game.Places.Balcony.KingBalcony;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Created by Samuele on 21/06/2016.
 */
public class KingBalconyHandler implements EventHandler<MouseEvent> {
    KingBalcony kingBalcony;
    GameController gameController;

    public KingBalconyHandler(KingBalcony kingBalcony, GameController gameController) {
        this.kingBalcony = kingBalcony;
        this.gameController = gameController;
    }

    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().setKingBalcony(kingBalcony);
    }
}
