package Client.GUI.Game.Handlers.GameHandler;

import Client.GUI.Game.GameController;
import Game.Places.Balcony.Balcony;
import Game.Places.Balcony.RegionBalcony;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Created by Samuele on 17/06/2016.
 */
public class RegionBalconyHandler implements EventHandler<MouseEvent> {
    private RegionBalcony balcony;
    private GameController gameController;

    public RegionBalconyHandler(RegionBalcony balcony, GameController gameController) {
        this.balcony = balcony;
        this.gameController = gameController;
    }


    @Override
    public void handle(MouseEvent event) {
        gameController.getActionContainer().setBalconyForAction(balcony);
    }
}
