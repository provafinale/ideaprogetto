package Client.GUI.Game;
import Client.ActionContainer;
import Client.ClientMatch;
import Client.GUI.Game.BankView.BankView;
import Client.GUI.Game.Handlers.GameHandler.*;
import Client.GUI.Game.Market.MarketController;
import Client.GUI.Game.Situation.SituationController;
import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Enemy;
import Game.Places.Balcony.KingBalcony;
import Game.Places.Balcony.RegionBalcony;
import Game.Places.*;
import Game.Places.Region;
import Game.Recompenses.Rewards.Reward;
import Game.Stuff.Councilor;
import Game.Stuff.Empory;
import Game.User;
import Miscellaneous.Constant;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXNodesList;
import com.jfoenix.controls.JFXTabPane;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import javafx.animation.Interpolator;
import javafx.animation.KeyValue;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.PopOver;

import java.io.File;
import java.net.URL;
import java.util.*;

/**
 * Created by Samuele on 30/05/2016.
 */
/** JAVAFX controller per il main stage di gioco*/
public class GameController implements Initializable{
    private ClientMatch clientMatch; //Riferimento al Model

    //Actions
    @FXML private Button electCouncilorButton;
    @FXML private Button buyBuildingButton;
    @FXML private Button engageHelperButton;
    @FXML private Button buildEmporyButton;
    @FXML private Button endTurnButton;
    private JFXNodesList buttons = new JFXNodesList();
    @FXML AnchorPane buttonPane;
    @FXML VBox bottoniera;

    @FXML private AnchorPane baseAnchor;
    @FXML private AnchorPane gaugeTimeAnchor;
    private Gauge gauge;

    //Game Objects
    @FXML private GridPane bigGrid;
    @FXML private JFXTabPane tabPaneBase;
    @FXML private Tab gameTab;
    @FXML private Tab marketTab;
    @FXML private ImageView mapImage;
    @FXML private Text userNameText;
    @FXML private Text userInTurnText;
    @FXML private Text myVictoryText;
    @FXML private Text myNobilityText;
    @FXML private Text myGoldText;
    @FXML private Text myHelperLabel;
    @FXML private Text mainActionText;
    @FXML private Text fastActionText;
    @FXML private ImageView goldLogo;
    @FXML private ImageView victoryLogo;
    @FXML private ImageView nobilityLogo;
    @FXML private ImageView myHelperImage;
    @FXML private ImageView mainActionImage;
    @FXML private ImageView fastActionImage;
    @FXML private AnchorPane root;
    @FXML private AnchorPane rootMap;
    @FXML private Pane balconyBGPane;
    @FXML private AnchorPane mapAnchorPane;
    //AnchorPane Cities
    @FXML private AnchorPane arkonPane;
    @FXML private AnchorPane castrumPane;
    @FXML private AnchorPane burgenPane;
    @FXML private AnchorPane dorfulPane;
    @FXML private AnchorPane estiPane;
    @FXML private AnchorPane framekPane;
    @FXML private AnchorPane gradenPane;
    @FXML private AnchorPane indurPane;
    @FXML private AnchorPane juvelarPane;
    @FXML private AnchorPane hellarPane;
    @FXML private AnchorPane kultosPane;
    @FXML private AnchorPane narisPane;
    @FXML private AnchorPane lyramPane;
    @FXML private AnchorPane merkatimPane;
    @FXML private AnchorPane osiumPane;
    @FXML private AnchorPane anchorCities;
    @FXML private Group group;
    private JFXDialog exceptionDialog = new JFXDialog();

    private HashMap<String,AnchorPane> kingOncityAnchorPaneHash = new HashMap<>();
    private HashMap<String,AnchorPane> citiesAnchorPane = new HashMap<>();
    private HashMap<City, ImageView> citiesImageView = new HashMap<>();
    //private ArrayList<ImageView> citiesImageView = new ArrayList<>();
    private PopOver popOverCities = new PopOver();
    private PopOver popOver = new PopOver();
    private ImageView kingImage = new ImageView(new Image(new File("maps/images/king.png").toURI().toString(),30,30,true,true));
    private boolean firstSnapshot = true;

    //AnchorPane Balcony
    @FXML private HBox seaBalconyPane;
    @FXML private HBox hillBalconyPane;
    @FXML private HBox mountainBalconyPane;
    @FXML private HBox kingBalconyPane;

    //Politic Cards
    @FXML private HBox politicsCardHBox;
    private ArrayList<ImageView> politicsCardsImage = new ArrayList<>();

    //Building Card
    @FXML private HBox mountainBuildingHBox;
    @FXML private HBox hillBuildingHBox;
    @FXML private HBox seaBuildingHBox;

    //Councilors image view
    private ArrayList<ImageView> seaBalconyImage = new ArrayList<>();
    private ArrayList<ImageView> hillBalconyImage = new ArrayList<>();
    private ArrayList<ImageView> mountainBalconyImage = new ArrayList<>();
    private ArrayList<ImageView> kingBalconyImage = new ArrayList<>();
    private Stage bankStage;
    private ActionContainer actionContainer;
    @FXML private Button showBankButton;

    private BooleanProperty scaleCouncilorsTransition = new SimpleBooleanProperty();
    private BooleanProperty scalePoliticsTransition = new SimpleBooleanProperty();
    private BooleanProperty scaleCitiesTransition = new SimpleBooleanProperty();
    private BooleanProperty scaleBuildingTransition = new SimpleBooleanProperty();
    @FXML private MarketController marketController;
    @FXML private SituationController situationController;
    private boolean focus = true;

    public boolean isFocus() {
        return focus;
    }

    public void setFocus(boolean focus) {
        this.focus = focus;
    }


    public void showException(String exc){
        Platform.runLater(()-> {
            Alert exceptionAlert = new Alert(Alert.AlertType.ERROR);
            File file = new File("maps/images/exception.png");
            Image image = new Image(file.toURI().toString(), 50, 50, true, true);
            exceptionAlert.setGraphic(new ImageView(image));
            exceptionAlert.setTitle("Consiglio dei Quattro");
            exceptionAlert.setHeaderText("La Mossa Non è Valida.");
            exceptionAlert.setContentText(exc);
            exceptionAlert.show();
        });

    }
    public void showBank(boolean toAction) throws Exception {
        BankView bankView = new BankView(clientMatch.getBank(),this,clientMatch.getUser(),bankStage);
        bankView.showBank(toAction);
    }
    public void closeBank(){
        Platform.runLater(()->{
            bankStage.close();});
    }
    public void showBuildingDeck(){
        Platform.runLater(()-> {
            PopOver popDeck = new PopOver();
            HBox hBox = new HBox();
            hBox.setSpacing(4);
            for (RegionName region : RegionName.values()){
                StackPane stackPane = new StackPane();
                File file = new File("maps/images/buildingCard/BACK.png");
                ImageView imageView = new ImageView(new Image(file.toURI().toString(),250,180,true,true,true));
                Label label = new Label(region.name());
                stackPane.getChildren().addAll(imageView,label);
                stackPane.setOnMouseClicked(new BuildingDeckHandler(region.name(),this,popDeck));
                hBox.getChildren().add(stackPane);
            }
            popDeck.setTitle("Scegli Mazzo Permessi");
            popDeck.setDetached(true);
            popDeck.setContentNode(hBox);
            double x = hillBuildingHBox.getLayoutX();
            double y = hillBuildingHBox.getLayoutY();
            popDeck.show(hillBuildingHBox,x,y);});
    }

    public ActionContainer getActionContainer(){
        return actionContainer;
    }
    public void setBankStage(Stage stage){
        this.bankStage = stage;
    }
    public void onClickShowBank() throws Exception {
        showBank(false);
    }
    public void onClickEngageHelper(){
        actionContainer.engageHelper();
    }
    public void onClickElectCouncilorWithHelper() throws Exception {
        actionContainer.electCouncilorWithHelper();
    }
    public void onClickElectCouncilor() throws Exception {
        actionContainer.electCouncilor();
    }
    public void onClickChangeBuildingCard(){
        actionContainer.changeBuildingCard();
    }
    public void onClickBuyBuildingCard() throws InterruptedException {
        actionContainer.buyBuildingCard();
    }
    public void onClickBuildEmporyWithKing(){
        actionContainer.buildEmporyWithKing();
    }
    public void onClickBuildEmpory() throws Exception {
        actionContainer.buildEmpory();
    }
    public void onClickAnotherMainAction(){
        actionContainer.anotherMainAction();
    }
    //Indica che ho selezionato tutte le città che voglio
    public void onClickNextStep(){
        actionContainer.setNext(true);
    }
    public void onClickEndTurn(){
        actionContainer.setActionCancel(true);
        clientMatch.skipTurn();
    }
    public void onClickActionCancel(){
        actionContainer.setActionCancel(true);
    }

    public void createGaugeTime(){
        gauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.DASHBOARD)
                .title("Turno")
                .minValue(0)
                .maxHeight(150)
                .maxWidth(150)
                .animated(true)
                .prefWidth(150)
                .prefHeight(150)
                .barEffectEnabled(true)
                .shadowsEnabled(true)
                .build();
        gauge.setMaxValue(Constant.DURATION_TURN_MIN);
        gauge.setMinValue(0);
        gaugeTimeAnchor.getChildren().add(gauge);
    }

    public void setGauge(double val) {
        Platform.runLater(()-> {
            gauge.setValue(val);
        });
    }

    public void createGameButtons(){
        buttons.setSpacing(6);
        JFXButton actions = new JFXButton();
        Label actionsLabel = new Label("Azioni");
        actions.setGraphic(actionsLabel);
        actionsLabel.setStyle("-fx-text-fill:WHITE");
        actions.setButtonType(JFXButton.ButtonType.RAISED);
        actions.getStyleClass().addAll("animated-option-button","rotoButton");
        actions.setId("firstRoto");

        JFXButton buyBuildingCard = new JFXButton("Compra Permesso" );
        buyBuildingCard.setButtonType(JFXButton.ButtonType.RAISED);
        buyBuildingCard.getStyleClass().addAll("animated-option-button", "rotoButton" , "mainAction");
        buyBuildingCard.setOnMouseClicked(event -> {
            try {
                onClickBuyBuildingCard();
            } catch (InterruptedException e) {
                e.printStackTrace();}});

        JFXButton buildEmpory = new JFXButton("Nuovo Emporio");
        buildEmpory.setButtonType(JFXButton.ButtonType.RAISED);
        buildEmpory.getStyleClass().addAll("animated-option-button", "rotoButton", "mainAction");
        buildEmpory.setOnMouseClicked(event -> {
            try {
                onClickBuildEmpory();} catch (Exception e) {
                e.printStackTrace();}});

        JFXButton buildEmporyWithKing = new JFXButton("Emporio con il Re" );
        buildEmporyWithKing.setButtonType(JFXButton.ButtonType.RAISED);
        buildEmporyWithKing.getStyleClass().addAll("animated-option-button", "rotoButton" , "mainAction");
        buildEmporyWithKing.setOnMouseClicked(event -> onClickBuildEmporyWithKing());

        JFXButton electCouncilor = new JFXButton("Eleggi");
        electCouncilor.setButtonType(JFXButton.ButtonType.RAISED);
        electCouncilor.getStyleClass().addAll("animated-option-button", "rotoButton", "mainAction");
        electCouncilor.setOnMouseClicked(event -> {
            try {
                onClickElectCouncilor();
            } catch (Exception e) {e.printStackTrace();}});

        JFXButton changeBuildingCards = new JFXButton("Cambia Permesso");
        changeBuildingCards.setButtonType(JFXButton.ButtonType.RAISED);
        changeBuildingCards.getStyleClass().addAll("animated-option-button", "rotoButton", "fastAction");
        changeBuildingCards.setOnMouseClicked(event -> {
            try {
                onClickChangeBuildingCard();} catch (Exception e) {
                e.printStackTrace();}});

        JFXButton electCouncilorWithHelper = new JFXButton("Eleggi con Aiutante");
        electCouncilorWithHelper.setButtonType(JFXButton.ButtonType.RAISED);
        electCouncilorWithHelper.getStyleClass().addAll("animated-option-button", "rotoButton", "fastAction");
        electCouncilorWithHelper.setOnMouseClicked(event -> {
            try {
                onClickElectCouncilorWithHelper();} catch (Exception e) {
                e.printStackTrace();}});

        JFXButton anotherMainAction = new JFXButton("Aggiungi Principale");
        anotherMainAction.setButtonType(JFXButton.ButtonType.RAISED);
        anotherMainAction.getStyleClass().addAll("animated-option-button", "rotoButton", "fastAction");
        anotherMainAction.setOnMouseClicked(event -> onClickAnotherMainAction());

        JFXButton engageHelper = new JFXButton("Richiedi Aiutante");
        engageHelper.setButtonType(JFXButton.ButtonType.RAISED);
        engageHelper.getStyleClass().addAll("animated-option-button", "rotoButton" ,"fastAction");
        engageHelper.setOnMouseClicked(event -> onClickEngageHelper());

        buttons.addAnimatedNode(actions, (expanded)->{ return new ArrayList<KeyValue>(){{ add(new KeyValue(actionsLabel.rotateProperty(), expanded? 360:0 , Interpolator.EASE_BOTH));}};});
        buttons.addAnimatedNode(buyBuildingCard);
        buttons.addAnimatedNode(anotherMainAction);
        buttons.addAnimatedNode(buildEmpory);
        buttons.addAnimatedNode(buildEmporyWithKing);
        buttons.addAnimatedNode(electCouncilor);
        buttons.addAnimatedNode(electCouncilorWithHelper);
        buttons.addAnimatedNode(engageHelper);
        buttons.addAnimatedNode(changeBuildingCards);

        buttons.setRotate(180);
        AnchorPane.setBottomAnchor(buttons, 10.0);
        buttonPane.getChildren().add(buttons);
    }

    public void addOptionButtons(){
        JFXButton revertAction = new JFXButton("Annulla");
        revertAction.setPrefWidth(100);
        revertAction.getStyleClass().addAll("bottonato", "danger");
        JFXButton sendAction = new JFXButton("Invia Azione");
        sendAction.getStyleClass().add("bottonato");
        JFXButton endTurn = new JFXButton("Passa Turno");
        endTurn.getStyleClass().add("bottonato");
        JFXButton bankButton = new JFXButton("Banca");
        bankButton.getStyleClass().add("bottonato");

        revertAction.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                onClickActionCancel();}});
        sendAction.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                onClickNextStep();}});
        endTurn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                onClickEndTurn();}});
        bankButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    onClickShowBank();} catch (Exception e) {
                    e.printStackTrace();}}});
        bottoniera.getChildren().addAll(revertAction,sendAction,endTurn,bankButton);
        bottoniera.setSpacing(10);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
       /* baseAnchor = new AnchorPane();
        baseAnchor.getChildren().add(tabPaneBase); */


        group.setScaleX(1);
        group.setScaleY(1);

        createGaugeTime();
        createGameButtons();
        addOptionButtons();
        scaleCitiesTransition.set(false);
        scaleCouncilorsTransition.set(false);
        scalePoliticsTransition.set(false);
        scaleBuildingTransition.set(false);

        File file = new File("maps/gameUI/gold.png");
        Image image = new Image(file.toURI().toString(),30,30,true,true,true);
        goldLogo.setImage(image);
        file = new File("maps/gameUI/shield.png");
        image = new Image(file.toURI().toString(),30,30,true,true,true);
        victoryLogo.setImage(image);

        file = new File("maps/gameUI/nobility.png");
        image = new Image(file.toURI().toString(),30,30,true,true,true);
        nobilityLogo.setImage(image);

        file = new File("maps/images/helper.png");
        image = new Image(file.toURI().toString(),30,30,true,true,true);
        myHelperImage.setImage(image);

        file = new File("maps/images/fastAction.png");
        image = new Image(file.toURI().toString(),30,30,true,true,true);
        fastActionImage.setImage(image);

        file = new File("maps/images/mainAction.png");
        image = new Image(file.toURI().toString(),30,30,true,true,true);
        mainActionImage.setImage(image);

        //Building Cards Background
        insertBackgroundBuildingCard(seaBuildingHBox);
        insertBackgroundBuildingCard(hillBuildingHBox);
        insertBackgroundBuildingCard(mountainBuildingHBox);

        gameTab.setId("gameTab");
        marketTab.setId("marketTab");
        myVictoryText.getStyleClass().add("detailsText");
        myGoldText.getStyleClass().add("detailsText");
        myHelperLabel.getStyleClass().add("detailsText");
        myNobilityText.getStyleClass().add("detailsText");
        userInTurnText.getStyleClass().add("detailsText");
        userNameText.getStyleClass().add("detailsText");
    }

    /**Routine di avvio che prepara le building cards, per ogni Hbox su cui viene chiamata*/
    private void insertBackgroundBuildingCard(HBox hbox){
        StackPane card1 = new StackPane();
        StackPane card2 = new StackPane();
        card1.setId("carta1");
        card2.setId("carta2");

        File file = new File ("maps/images/buildingCard/BACK.png");
        Image image = new Image(file.toURI().toString(),120,90,true,true,true);

        ImageView backBuilding_1 = new ImageView(image);
        ImageView backBuilding_2 = new ImageView(image);

        Text citiesName1 = new Text();
        citiesName1.setId("citiesCarta");
        citiesName1.getStyleClass().add("initialOfCityBulding");

        Text citiesName2 = new Text();
        citiesName2.setId("citiesCarta");
        citiesName2.getStyleClass().add("initialOfCityBulding");

        card1.getChildren().addAll(backBuilding_1,citiesName1);
        card2.getChildren().addAll(backBuilding_2,citiesName2);
        //Aggiunge, per ogni building card 2 image view (per i bonus) e due text (per i bonus)

        HBox pane1 = new HBox();
        pane1.setAlignment(Pos.CENTER);
        HBox pane2 = new HBox();
        pane2.setAlignment(Pos.CENTER);
        for (int i = 1; i < 3; i++) {
            StackPane stackPane1 = new StackPane();
            StackPane stackPane2 = new StackPane();

            Text textBonus1 = new Text();
            textBonus1.setId("textBonus"+i);
            textBonus1.getStyleClass().add("textBonusBuildingCard"+i);
            ImageView imageBonus1 = new ImageView();
            imageBonus1.setId("imageBonus"+i);
            imageBonus1.getStyleClass().add("bonusBuildingCard"+i);
            stackPane1.getChildren().addAll(imageBonus1, textBonus1);
            stackPane1.setPadding(new Insets(-10,0,0,0));
            pane1.getChildren().add(stackPane1);

            Text textBonus2 = new Text();
            textBonus2.setId("textBonus"+i);
            textBonus2.getStyleClass().add("textBonusBuildingCard"+i);
            ImageView imageBonus2 = new ImageView();
            imageBonus2.setId("imageBonus"+i);
            imageBonus2.getStyleClass().add("bonusBuildingCard"+i);
            stackPane2.getChildren().addAll(imageBonus2, textBonus2);
            stackPane2.setPadding(new Insets(-10,0,0,0));
            pane2.getChildren().add(stackPane2);
        }


        card1.getChildren().add(pane1);
        card2.getChildren().add(pane2);

        card1.setAlignment(pane1, Pos.BOTTOM_CENTER);
        card1.setAlignment(citiesName1,Pos.TOP_CENTER);
        card1.setMargin(citiesName1,new Insets(15,0,0,0));
        card1.setMargin(pane1, new Insets(10,0,0,0));


        card2.setAlignment(pane2, Pos.BOTTOM_CENTER);
        card2.setAlignment(citiesName2,Pos.TOP_CENTER);
        card2.setMargin(citiesName2,new Insets(15,0,0,0));
        card2.setMargin(pane2, new Insets(10,0,0,0));
        hbox.getChildren().addAll(card1,card2);
    }

    /**Dato un anchor pane e una building card, inserisce nell'anchor pane tutta la parte grafica della building*/
    private void insertDataIntoAnchorCard(StackPane anchorPane,BuildingCard buildingCard){
        anchorPane.setOnMouseClicked(new BuildingCardHandler(buildingCard,this, anchorPane));
        Text citiesName = (Text) anchorPane.lookup("#citiesCarta");
        ImageView bonusCity1 = (ImageView) anchorPane.lookup("#imageBonus1");
        Text textBonus1 = (Text) anchorPane.lookup("#textBonus1");
        ImageView bonusCity2 = (ImageView) anchorPane.lookup("#imageBonus2");
        Text textBonus2 = (Text) anchorPane.lookup("#textBonus2");
        //Get la nuova buildingCard dalla Logica
        ArrayList<String> typeOfRewards = buildingCard.getTypesOfRewards();
        ArrayList<Integer> numOfRewards = buildingCard.getNumbersOfRewards();

        //Aggiorno componenti grafiche della buildingCard
        //iniziali città
        citiesName.setText(buildingCard.getInitials());
        //Tipi di rewards
        if (typeOfRewards.size()>0)
            setImageOfReward(bonusCity1,typeOfRewards.get(0),textBonus1,numOfRewards.get(0));
        if (typeOfRewards.size()>1)
            setImageOfReward(bonusCity2,typeOfRewards.get(1),textBonus2,numOfRewards.get(1));

        //Numeri di rewards


    }

    /**Aggiorna a gruppi di due, le building cards di una regione (data dall'hbox che gli si passa)*/
    private void insertDataToBuildingCard(HBox hBox, List<BuildingCard> buildingCards){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //Componenti delle 2 BuildingCards.
                StackPane anchorCard1 = (StackPane) hBox.lookup("#carta1");
                StackPane anchorCard2 = (StackPane) hBox.lookup("#carta2");
                //Get le 2 nuove buildingCard dalla Logica
                BuildingCard buildingCard1 = buildingCards.get(0);
                BuildingCard buildingCard2 = buildingCards.get(1);

                insertDataIntoAnchorCard(anchorCard1,buildingCard1);
                insertDataIntoAnchorCard(anchorCard2,buildingCard2);
            }
        });

    }

    private void setImageOfReward(ImageView imageView,String typeOfReward,Text textNumOfReward,int numOfReward){
        File file = new File("maps/images/rewards/"+typeOfReward+".png");
        Image image = new Image(file.toURI().toString(),30,25,true,true,true);
        imageView.setImage(image);
        textNumOfReward.setText(String.valueOf(numOfReward));
    }


    public void setUserInTurn(String user){
        userInTurnText.setText("Turno di: "+user);
    }

    public void setMap(String map){
        String pathSelectedMap = "maps/images/"+map+".jpg";
        File file = new File(pathSelectedMap);
        Image image = new Image(file.toURI().toString(),true);
        mapImage.setImage(image);

        actionContainer = clientMatch.getActionContainer();
        actionContainer.setGameController(this);
    }

    public void enableAllElements(){
        turnOffBalcony(false);
        turnOffBuildingCards(false);
        turnOffPoliticCards(false);
        anchorCities.setMouseTransparent(false);
        stopAllAnimations();
    }

    public void disableAllElements(){
        turnOffBalcony(true);
        turnOffBuildingCards(true);
        turnOffPoliticCards(true);
        anchorCities.setMouseTransparent(true);
        stopAllAnimations();
    }

    public void enableOnlyCities(){
        stopAllAnimations();
        anchorCities.setMouseTransparent(false);
        turnOffBuildingCards(true);
        turnOffBuildingCards(true);
        turnOffBalcony(true);
        for (Map.Entry<City,ImageView> select : citiesImageView.entrySet()){
            ImageView selectedImage = select.getValue();
            borderGlowEffect(selectedImage,1.1);
        }
    }

    public void enableOnlyPoliticCards(){
        stopAllAnimations();
        turnOffPoliticCards(false);
        turnOffBalcony(true);
        turnOffBuildingCards(true);
        anchorCities.setMouseTransparent(true);

        borderGlowEffect(politicsCardHBox,80);
    }

    public void enableOnlyBuildingCards(){
        stopAllAnimations();
        turnOffBuildingCards(false);
        anchorCities.setMouseTransparent(true);
        turnOffBalcony(true);
        borderGlowEffect(seaBuildingHBox,80);
        borderGlowEffect(hillBuildingHBox,80);
        borderGlowEffect(mountainBuildingHBox,80);
    }

    public void enableOnlyBalconies(){
        stopAllAnimations();
        seaBalconyPane.setMouseTransparent(false);
        hillBalconyPane.setMouseTransparent(false);
        mountainBalconyPane.setMouseTransparent(false);

        anchorCities.setMouseTransparent(true);
        kingBalconyPane.setMouseTransparent(true);
        turnOffBuildingCards(true);
        turnOffPoliticCards(true);
        borderGlowEffect(seaBalconyPane,80);
        borderGlowEffect(hillBalconyPane,80);
        borderGlowEffect(mountainBalconyPane,80);
    }
    public void stopAllAnimations(){
        borderGlowEffect(seaBalconyPane,0);
        borderGlowEffect(hillBalconyPane,0);
        borderGlowEffect(mountainBalconyPane,0);
        borderGlowEffect(kingBalconyPane,0);
        borderGlowEffect(seaBuildingHBox,0);
        borderGlowEffect(hillBuildingHBox,0);
        borderGlowEffect(mountainBuildingHBox,0);
        borderGlowEffect(politicsCardHBox,0);

        removeEffect(politicsCardHBox.getChildren());
        removeEffect(seaBuildingHBox.getChildren());
        removeEffect(mountainBuildingHBox.getChildren());
        removeEffect(hillBuildingHBox.getChildren());
        for (Map.Entry<City,ImageView> select : citiesImageView.entrySet()){
            ImageView selectedImage = select.getValue();
            borderGlowEffect(selectedImage,1.0);
        }
    }

    private void turnOffBalcony(boolean bool){
        borderGlowEffect(seaBalconyPane,0);
        borderGlowEffect(hillBalconyPane,0);
        borderGlowEffect(mountainBalconyPane,0);
        borderGlowEffect(kingBalconyPane,0);

        seaBalconyPane.setMouseTransparent(bool);
        hillBalconyPane.setMouseTransparent(bool);
        mountainBalconyPane.setMouseTransparent(bool);
        kingBalconyPane.setMouseTransparent(bool);
    }

    private void turnOffBuildingCards(boolean bool){
        borderGlowEffect(seaBuildingHBox,0);
        borderGlowEffect(hillBuildingHBox,0);
        borderGlowEffect(mountainBuildingHBox,0);

        seaBuildingHBox.setMouseTransparent(bool);
        hillBuildingHBox.setMouseTransparent(bool);
        mountainBuildingHBox.setMouseTransparent(bool);
    }

    private void turnOffPoliticCards(boolean bool){
        borderGlowEffect(politicsCardHBox,0);
        politicsCardHBox.setMouseTransparent(bool);
    }





    /**Metodo chiamato dal ClientMatch quando arriva un nuovo snapshot: aggiorna la view*/
    public void updateMyData(){
        User user = clientMatch.getUser();
        //Aggiorno le lettere delle città delle buildingCards
        insertDataToBuildingCard(seaBuildingHBox,clientMatch.getSeaBuildingCard());
        insertDataToBuildingCard(hillBuildingHBox,clientMatch.getHillBuildingCards());
        insertDataToBuildingCard(mountainBuildingHBox,clientMatch.getMountainBuildingCards());

        //Label di gioco
        myHelperLabel.setText(String.valueOf(user.getHelpers()));
        myVictoryText.setText(String.valueOf(user.getPosVictoryPawn()));
        myGoldText.setText(String.valueOf(user.getPosGoldPawn()));
        myNobilityText.setText(String.valueOf(user.getPosNobilityPawn()));
        mainActionText.setText(String.valueOf(user.getRemainingMainActions()));
        fastActionText.setText(String.valueOf(user.getRemainingFastActions()));
        resetHBoxToRefresh();
        updateCouncilors();
        updatePoliticsCard();
        if (!firstSnapshot) {
            updateKing(clientMatch.getCityOfKing());
        }
    }

    public void updateKing(City city){
        Platform.runLater(()-> {
            if (kingImage.getParent() != null) {
                AnchorPane oldKingPane = (AnchorPane) kingImage.getParent();
                oldKingPane.getChildren().remove(kingImage);
            }
            AnchorPane kingAnchorPane = kingOncityAnchorPaneHash.get(city.getName().name());
            kingAnchorPane.getChildren().add(kingImage);
        });

    }


    /**Metodo di servizio che pulisce tutti i box della view, prima dell'aggiornamento.*/
    private void resetHBoxToRefresh(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                politicsCardHBox.getChildren().clear();
                kingBalconyPane.getChildren().clear();
                seaBalconyPane.getChildren().clear();
                hillBalconyPane.getChildren().clear();
                mountainBalconyPane.getChildren().clear();}});
    }


    private void updatePoliticsCard(){
        Platform.runLater(()->{
            ArrayList<PoliticCard> politicCards = clientMatch.getUser().getPoliticCards();
            politicsCardsImage = new ArrayList<ImageView>();
            for (PoliticCard card : politicCards) {
                updateGameObjects(card.getColor(),politicsCardsImage,politicsCardHBox,"politicsCard");
            }
            for (int i=0; i <politicsCardsImage.size(); i++){
                politicsCardsImage.get(i).setOnMouseClicked(new PoliticCardHandler(politicCards.get(i),this, politicsCardsImage.get(i)));
            }
        });
    }



    private void updateCouncilors(){
        Platform.runLater(()-> {

            ArrayList<Region> regions = clientMatch.getGameMap().getRegions();

            RegionBalcony seaBalcony = regions.get(2).getBalcony();
            RegionBalcony hillBalcony = regions.get(1).getBalcony();
            RegionBalcony mountainBalcony = regions.get(0).getBalcony();
            KingBalcony kingBalcony = clientMatch.getGameMap().getKingGangplank().getKingBalcony();
            seaBalconyPane.setOnMouseClicked(new RegionBalconyHandler(seaBalcony,this));
            hillBalconyPane.setOnMouseClicked(new RegionBalconyHandler(hillBalcony,this));
            mountainBalconyPane.setOnMouseClicked(new RegionBalconyHandler(mountainBalcony,this));
            kingBalconyPane.setOnMouseClicked(new KingBalconyHandler(kingBalcony,this));

            ArrayList<Councilor> councilorsSea = new ArrayList<>();
            councilorsSea.addAll(seaBalcony.getBalconyConcilors());
            ArrayList<Councilor> councilorsHill = new ArrayList<Councilor>();
            councilorsHill.addAll(hillBalcony.getBalconyConcilors());
            ArrayList<Councilor> councilorsMnt = new ArrayList<Councilor>();
            councilorsMnt.addAll(mountainBalcony.getBalconyConcilors());
            ArrayList<Councilor> councilorsKing = new ArrayList<Councilor>();
            councilorsKing.addAll(kingBalcony.getBalconyConcilors());
            seaBalconyImage = new ArrayList<ImageView>();
            hillBalconyImage = new ArrayList<ImageView>();
            mountainBalconyImage = new ArrayList<ImageView>();
            kingBalconyImage = new ArrayList<ImageView>();

            for (int i = 0; i < councilorsSea.size(); i++) {
                updateGameObjects(councilorsSea.get(i).getCouncilorColorName(),seaBalconyImage,seaBalconyPane,"councilor");
                updateGameObjects(councilorsHill.get(i).getCouncilorColorName(),hillBalconyImage,hillBalconyPane,"councilor");
                updateGameObjects(councilorsMnt.get(i).getCouncilorColorName(),mountainBalconyImage,mountainBalconyPane,"councilor");
                updateGameObjects(councilorsKing.get(i).getCouncilorColorName(),kingBalconyImage,kingBalconyPane,"councilor");
            }
        });
    }


    public Group getGroup(){
        return this.group;
    }

    /**Metodo chiamato quando aggiorno le immagini dei consiglieri e delle carte politiche*/
    private void updateGameObjects(String color, ArrayList<ImageView> balconyImages, HBox hBoxPanel,String type){
        File counsFile = new File ("maps/images/"+type+"/"+ color +".png");
        Image counsImage = new Image(counsFile.toURI().toString(),100,60,true,true,true);
        ImageView counsImageView = new ImageView(counsImage);
        balconyImages.add(counsImageView);
        hBoxPanel.getChildren().add(counsImageView);
    }

    public void setModel(ClientMatch clientMatch){
        this.clientMatch = clientMatch;
        marketController.setClientMatch(clientMatch);
        marketController.setGameController(this);
        situationController.setClientMatch(clientMatch);

        /**Aggiungo il Listener dell'interfaccia con i relativi metodi*/
        clientMatch.addListener(new Listener() {
            @Override
            public void onReceiveSnapshot() {
                System.out.println("Aggiorno Interfaccia.....");
                updateMyData();
            }
            public void updateTurn(String userInTurn){
                stopAllAnimations();
                setGauge(Constant.DURATION_TURN_MIN);
                userInTurnText.setText(userInTurn);}

            public void setUsernameText(String username){
                userNameText.setText(username);}
            public void setCitiesOnUI(){
                addImagesOfCities();
                if (firstSnapshot){
                    ArrayList<Enemy> enemies = clientMatch.getEnemies();
                    for (Enemy enemy : enemies){
                        situationController.setChoiceBox(enemy.getUserName());
                    }
                    updateKing(clientMatch.getCityOfKing());
                    firstSnapshot=false;}
            }
            @Override
            public void updateGauge() {
                Platform.runLater(()->{
                    if (gauge.getValue()>0)
                        gauge.setValue(gauge.getValue()-1);});
            }
            @Override
            public void onTimerExceed(){
                Platform.runLater(()->{
                    notifyEndTurn();});}

            @Override
            public void startMarket() {
                if (!tabPaneBase.getSelectionModel().getSelectedItem().getId().equals("marketTab"))
                    tabPaneBase.getSelectionModel().select(marketTab);
                setGauge(Constant.MARKET_TIME_MIN);
            }

            @Override
            public void startBuyMarket() {
                if (!tabPaneBase.getSelectionModel().getSelectedItem().getId().equals("marketTab"))
                    tabPaneBase.getSelectionModel().select(marketTab);
                setGauge(Constant.MARKET_TIME_MIN);
            }

            @Override
            public void endMarket() {
                if (!tabPaneBase.getSelectionModel().getSelectedItem().getId().equals("gameTab"))
                    tabPaneBase.getSelectionModel().select(gameTab);
                setGauge(Constant.DURATION_TURN_MIN);
            }
        });
    }

    public void addImagesOfCities(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                GameMap gameMap = clientMatch.getGameMap();
                ArrayList<Region> regions = gameMap.getRegions();
                Region sea = regions.get(2);
                Region hill = regions.get(1);
                Region mountain = regions.get(0);

                addImageOfCity(arkonPane,sea.getCityFromName(CitiesName.ARKON.name()));
                addImageOfCity(castrumPane,sea.getCityFromName(CitiesName.CASTRUM.name()));
                addImageOfCity(burgenPane,sea.getCityFromName(CitiesName.BURGEN.name()));
                addImageOfCity(dorfulPane,sea.getCityFromName(CitiesName.DORFUL.name()));
                addImageOfCity(estiPane,sea.getCityFromName(CitiesName.ESTI.name()));


                addImageOfCity(framekPane,hill.getCityFromName(CitiesName.FRAMEK.name()));
                addImageOfCity(gradenPane,hill.getCityFromName(CitiesName.GRADEN.name()));
                addImageOfCity(indurPane,hill.getCityFromName(CitiesName.INDUR.name()));
                addImageOfCity(juvelarPane,hill.getCityFromName(CitiesName.JUVELAR.name()));
                addImageOfCity(hellarPane,hill.getCityFromName(CitiesName.HELLAR.name()));

                addImageOfCity(kultosPane,mountain.getCityFromName(CitiesName.KULTOS.name()));
                addImageOfCity(narisPane,mountain.getCityFromName(CitiesName.NARIS.name()));
                addImageOfCity(lyramPane,mountain.getCityFromName(CitiesName.LYRAM.name()));
                addImageOfCity(merkatimPane,mountain.getCityFromName(CitiesName.MERKATIM.name()));
                addImageOfCity(osiumPane,mountain.getCityFromName(CitiesName.OSIUM.name()));
            }});
    }

    /**Dato un AnchorPane, e una città, associa a quell'anchorpane l'immagine col colore selezionato*/
    private void addImageOfCity(AnchorPane pane, City city){
        String color = city.getColorName();
        File colorFile = new File("maps/images/cities/"+color+".png");
        Image colorImage = new Image(colorFile.toURI().toString(),250,250,true,true,true);

        ImageView imageView = new ImageView(colorImage);
        imageView.setMouseTransparent(false);
        //imageView.setPreserveRatio(false);
        imageView.fitWidthProperty().bind(pane.widthProperty());
        imageView.fitHeightProperty().bind(pane.heightProperty());

        //Bonus City
        ArrayList<Reward> rewards = city.getRewards();
        ArrayList<String> typeOfRewards = new ArrayList<>();
        ArrayList<Integer> numOfRewards = new ArrayList<>();

        for (Reward reward : rewards){
            typeOfRewards.add(reward.typeOfReward());
            numOfRewards.add(reward.numOfReward());
        }

        FlowPane imageFlow = new FlowPane(); // PANE PER AFFIANCARE IMMAGINI REWARDS
        imageFlow.setPrefWrapLength(60);

        ImageView rewardImage1 = new ImageView();
        StackPane stackPane1 = new StackPane();
        Label rewardText1 = new Label();
        stackPane1.getChildren().addAll(rewardImage1, rewardText1);
        imageFlow.getChildren().add(stackPane1);

        StackPane stackPane2 = new StackPane();
        ImageView rewardImage2 = new ImageView();
        Label rewardText2 = new Label();
        stackPane2.getChildren().addAll(rewardImage2, rewardText2);
        imageFlow.getChildren().add(stackPane2);

        if (rewards.size()>0){
            File file = new File("maps/images/rewards/"+typeOfRewards.get(0)+".png");
            rewardImage1.setImage(new Image(file.toURI().toString(),30,30,true,true));
            rewardText1.setText(String.valueOf(numOfRewards.get(0)));
        }
        if (rewards.size()>1){
            File file = new File("maps/images/rewards/"+typeOfRewards.get(1)+".png");
            rewardImage2.setImage(new Image(file.toURI().toString(),30,30,true,true));
            rewardText2.setText(String.valueOf(numOfRewards.get(1)));
        }

        AnchorPane kingAnchorPane = new AnchorPane();
        kingAnchorPane.setPadding(new Insets(10,10,0,0));
        kingOncityAnchorPaneHash.put(city.getName().name(),kingAnchorPane);
        AnchorPane.setBottomAnchor(kingAnchorPane,7.0);
        citiesImageView.put(city,imageView);
        pane.getChildren().addAll(new ImageView(colorImage),imageFlow,kingAnchorPane);
        imageFlow.setPadding(new Insets(20,0,0,0));
        pane.setOnMouseClicked(new CityHandler(city,this,pane));
        pane.setOnMouseEntered(new CityHandlerOnOver(city,this,pane));
        pane.setId(city.getName().name());

    }

    public void createCityDialog(CitiesName cityName,AnchorPane cityPane){
        HBox emporiesBox = new HBox();
        City city = clientMatch.getGameMap().getCityFromName(cityName);
        emporiesBox.setAlignment(Pos.CENTER);
        ArrayList<Empory> empories = city.getCityEmpories();
        if (empories.size()==0)
            emporiesBox.getChildren().add(new Label("Non c'è nessun emporio su "+city.getName()));

        for (Empory empory : empories) {
            String emporyImage = empory.getColor().name();
            File file = new File("maps/images/empories/"+emporyImage+".png");
            ImageView imageView = new ImageView(new Image(file.toURI().toString(),50,50,true,true));
            emporiesBox.getChildren().add(imageView);
        }
        Pane popPane = new Pane();
        popPane.getChildren().add(emporiesBox);
        popOverCities = new PopOver();
        popOverCities.setContentNode(popPane);
        popOverCities.setTitle("Empori su "+city.getName());
        popOverCities.setArrowLocation(PopOver.ArrowLocation.TOP_CENTER);
        popOverCities.show(cityPane);
        cityPane.setOnMouseExited(event -> popOverCities.hide());
    }

    public void notifyEndTurn(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Notifications.create()
                        .title("FINE TURNO").darkStyle().text("E' finito il tuo turno! E' il turno di "+userInTurnText.getText()).showInformation();
            }
        });
    }

    /**Animazioni **/
    @FXML
    public void onMouseEnteredCity(MouseEvent event){
        scaleTransitionPos(event,1.15);
    }

    public void onMouseExitCity(MouseEvent event){
        scaleTransitionPos(event,1);
    }


    public void scaleTransitionPos(MouseEvent event,double scale) {
        ScaleTransition pulseTransition;
        pulseTransition = new ScaleTransition(Duration.millis(300), (Node)event.getTarget());
        pulseTransition.setCycleCount(1);
        pulseTransition.setInterpolator(Interpolator.EASE_BOTH);

        pulseTransition.setFromX(((Node)event.getTarget()).getScaleX());
        pulseTransition.setFromY(((Node)event.getTarget()).getScaleY());
        pulseTransition.setToX(scale);
        pulseTransition.setToY(scale);
        pulseTransition.playFromStart();
    }

    public void borderGlowEffect(Node node,double var){
        DropShadow borderGlow = new DropShadow();
        borderGlow.setColor(Color.WHITE);
        borderGlow.setOffsetX(0f);
        borderGlow.setOffsetY(0f);
        borderGlow.setHeight(var);
        borderGlow.setWidth(var);
        node.setEffect(borderGlow);
    }
    public void selectedEffect(Node node, double lenght){
        DropShadow borderGlow = new DropShadow();
        borderGlow.setColor(Color.RED);
        borderGlow.setOffsetX(0f);
        borderGlow.setOffsetY(0f);
        borderGlow.setHeight(lenght);
        borderGlow.setWidth(lenght);
        node.setEffect(borderGlow);
    }

    public void removeEffect(ObservableList<Node> nodes){
        for (Node node : nodes) {
            borderGlowEffect(node, 0);
        }
    }
}

