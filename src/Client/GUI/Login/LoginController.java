package Client.GUI.Login;

import Client.ClientManager;
import com.jfoenix.controls.JFXSpinner;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

import java.io.File;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

/**
 * Created by Samuele on 30/05/2016.
 */
/** Gestisce la parte grafica della sezione di login */
public class LoginController implements Initializable{
    @FXML private TextField ipText;
    @FXML private TextField nickname;
    @FXML private Button loginButton;
    @FXML private ImageView loginImage;
    @FXML private JFXSpinner spinner;
    @FXML private BorderPane borderPane;

    private ClientManager clientManager;



    public void onClickLoginButton() throws RemoteException {
        //Getto le info che l'utente ha inserito nella UI
        String ip = ipText.getText();
        String nick = nickname.getText();

        try {
            clientManager.createClient(ip,nick);

        } catch (Exception e) {
            System.out.println(e);
        }

        //spinner.setPrefSize(100,100);
        spinner.setScaleX(2);
        spinner.setScaleY(2);
        spinner.setScaleZ(2);
        spinner.setVisible(true);
        borderPane.setOpacity(0.6);
        spinner.setOpacity(1);
        System.out.println(spinner.getOpacity());
        //connectionText.setVisible(true);
        loginButton.setMouseTransparent(true);

    }

    public void setClientManager(ClientManager client){
        this.clientManager = client;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("maps/images/Misc/guiLogin.png");
        Image image = new Image(file.toURI().toString());
        loginImage.setImage(image);
    }
}
