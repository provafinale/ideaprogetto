package Client.CLI;

import Client.ClientManager;
import Client.ClientMarket;
import Client.ClientMatch;
import Client.GUI.Game.Listener;
import Client.Inteface.GraphicInterface;
import Client.Inteface.MarketInterface;
import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Places.Balcony.Balcony;
import Game.Places.City;
import Game.Places.RegionName;
import Game.Stuff.CouncilorColor;
import Game.Stuff.Helper;
import Game.User;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.BadInputException;
import Miscellaneous.Randomizer;
import Server.Market.BuyableObject;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Samuele on 11/06/2016.
 */
/** La Command Line*/
public class CommandLineInterface implements GraphicInterface {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private HashMap<Character, City> characterCityHashMap;

    ClientManager clientManager;
    ClientMatch clientMatch;
    ClientMarket clientMarket;
    Scanner input = new Scanner(System.in);
    private String userPlaying = "";
    // boolean canRead;
    String myUserName;
    ExecutorService service = Executors.newCachedThreadPool();

    public CommandLineInterface(ClientManager clientManager, ClientMatch clientMatch) {
        this.clientManager = clientManager;
        this.clientMatch = clientMatch;
        clientMatch.addMarketListener(new MarketInterface() {
            @Override
            public void updateMarket() {
                updateMarketCLI();
                if (clientMarket.canSell())
                    printMarkets();
                if (clientMarket.canBuy())
                    showBuyMarket();
            }
        });
        clientMatch.addListener(new Listener() {
                                    @Override
                                    public void onReceiveSnapshot() {
                                        System.out.println("Ricevuto Snapshot nella CLI");
                                        if (canPlay()) displayAndTryActions();
                                        else
                                            displaySituation();
                                    }

                                    @Override
                                    public void updateTurn(String userInTurn) {
                                        userPlaying = userInTurn;

                                        if (canPlay()) { //se è il mio turno
                                            System.out.println("E' il tuo turno, giocatelo bene ;) ");
                                            displayAndTryActions();
                                        } else {
                                            System.out.println("E' il turno di" + userInTurn + " , vediamo" +
                                                    "cosa combina!");
                                            displaySituation();
                                        }

                                    }

                                    @Override
                                    public void setUsernameText(String userName) {
                                        asciiArt();
                                        System.out.println("Benvenuto " + userName);
                                        myUserName = userName;
                                    }

                                    @Override
                                    public void setCitiesOnUI() {
                                        System.out.println("Set cities nella CLI");

                                    }

                                    @Override
                                    public void updateGauge() {
                                        //SOLO GUI
                                    }

                                    @Override
                                    public void onTimerExceed() {

                                    }


                                    //MARKET
                                    @Override
                                    //Metodo chiamato alla partenza del Market
                                    public void startMarket() {
                                        System.out.println(" ---- MARKET: ONLINE ----");
                                    }

                                    @Override
                                    public void startBuyMarket() {
                                        System.out.println("STARTBUYMARKET()");
                                       /* ArrayList<BuyableObject> buyableObjects = getBuyableObjects();
                                        System.out.println(buyableObjects);
                                        */

                                    }

                                    @Override
                                    //Metodo chiamato alla fine del market
                                    public void endMarket() {
                                        System.out.println("END_MARKET");
                                        Robot robot = null;
                                        try {
                                            if (!clientMatch.getClientMarket().canBuy() && !clientMatch.getClientMarket().canSell()) {

                                                robot = new Robot();
                                                robot.keyPress(KeyEvent.VK_0);
                                                robot.keyRelease(KeyEvent.VK_0);
                                                robot.keyPress(KeyEvent.VK_ENTER);
                                                robot.keyRelease(KeyEvent.VK_ENTER);
                                            }
                                        } catch (AWTException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
        );
    }

    @Override
    public void callChooseMapUI() {
        System.out.println("Scegli Mappa: ");
        String map = input.nextLine();
        clientManager.sendChosenMap(map);


    }

    @Override
    public void changeToGameUI(String map) {

    }

    /** Consente la scelta di ip e nome utente*/
    @Override
    public void startInterface() {
        System.out.println(ANSI_BLUE + "Inserisci IP Server: ");
        String ip = input.nextLine();


        System.out.println("Inserisci Nome Utente: " + ANSI_RESET);
        String nickname = input.nextLine();


        try {
            clientManager.createClient(ip, nickname);
            System.out.println("<<Connessione Stabilita con il Server " + ip + ">>");

        } catch (Exception e) {
            System.out.println("Errore Connessione.... Server non disponibile....");
            System.exit(-1);
        }

    }

    @Override
    public void sendChosenMap(String map) {

    }

    @Override
    public void printException(String msg) {
        System.out.println("Errore dal server! "+msg);
        //TODO: SOUT
    }

    /** Visualizza e permette all'utente di selezionare l'azione da effettuare*/
    private void displayAndTryActions() {


        service.execute(() -> {
            boolean badInput = false;
            int main = clientMatch.getUser().getRemainingMainActions();
            int fast = clientMatch.getUser().getRemainingFastActions();
            System.out.println(ANSI_PURPLE + "Situazione attuale:" + clientMatch.print() + "\n" + ANSI_RESET);
            if (canPlay()) { //Se è davvero il tuo turno
                System.out.println("Hai a disposizione " + main +
                        " azioni principali e " + fast + " azioni veloci.");
                if (main > 0) {
                    System.out.println(ANSI_GREEN + "AZIONI PRINCIPALI \n");
                    System.out.println("1: Eleggi un Consigliere");
                    System.out.println("2: Acquista una tessera permesso di costruzione");
                    System.out.println("3: Costruisci un emporio utilizzando una tessera permesso");
                    System.out.println("4: Costruisci un emporio con l'aiuto del re: " + ANSI_RESET);
                }
                if (fast > 0) {
                    System.out.println(ANSI_YELLOW + "AZIONI VELOCI \n");
                    System.out.println("5: Ingaggia un aiutante");
                    System.out.println("6: Cambia le tessere permesso di costruzione");
                    System.out.println("7: Manda un aiutante a eleggere un consigliere");
                    System.out.println("8: Compi un'azione principale aggiuntiva");
                    System.out.println(ANSI_RESET);
                }

                int azione = 0; //LEGGI

                if (canPlay() && !(clientMatch.getClientMarket().canBuy() || clientMatch.getClientMarket().canSell())) {
                        //  if (input.hasNextInt()) {
                        try {
                            azione = input.nextInt();
                            try {
                                switch (azione) {
                                    case 1: {
                                        try {
                                            CouncilorColor color = councilourInit();
                                            if (color != null) {
                                                String balconyName = balconyInit();
                                                if (balconyName != null) {
                                                    System.out.println("Stai eleggendo un consigliere...");
                                                    clientMatch.tryElectCouncilor(color, balconyName);
                                                }
                                            }
                                            break;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    case 2: {
                                        System.out.println("Stai comprando una carta permesso... Soddisfa il Consiglio!");
                                        ArrayList<PoliticCard> selectedCards = new ArrayList<PoliticCard>();

                                        while (true) {
                                            PoliticCard card = selectPoliticCard(selectedCards);
                                            if (card == null) break;
                                            selectedCards.add(card);
                                        }

                                        try {
                                            String balconyName = balconyInit();
                                            BuildingCard cardToAcquire = selectBuildingCard(balconyName);
                                            clientMatch.tryBuyBuildingCard(selectedCards, balconyName, cardToAcquire);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    }
                                    case 3: {
                                        BuildingCard card;
                                        City city;
                                        System.out.println("Stai costruendo un emporio...");
                                        System.out.println("I tuoi permessi di costruzione;");
                                        card = chooseBuilding();
                                        if (card != null) {
                                            city = selectCity();
                                            clientMatch.tryBuildEmpory(card, city);
                                        } else System.out.println("Azione non valida! Riprova senza barare!");
                                        break;
                                    }
                                    case 4: {
                                        System.out.println("Stai costruendo con l'aiuto del re... Ecco il balcone del re:");
                                        System.out.println(clientMatch.getGameMap().getKingGangplank().getKingBalcony().print());

                                        break;
                                    }
                                    case 5: {
                                        clientMatch.tryEngageHelper();
                                        System.out.println("Stai ingaggiando un aiutante...");
                                        break;
                                    }
                                    case 6: {
                                        System.out.println("Scegli la regione di cui cambiare le carte permesso: ");
                                        try {
                                            clientMatch.tryChangeBuildingCards(balconyInit());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    case 7: {
                                        try {
                                            CouncilorColor color = councilourInit();
                                            if (color != null) {
                                                String balconyName = balconyInit();
                                                if (balconyName != null) {
                                                    System.out.println("Stai eleggendo un consigliere con l'aiuto di un aiutante... [Sfruttamento del lavoro nero?]");
                                                    clientMatch.tryElectCouncilorWithHelper(color, balconyName);
                                                }
                                            }
                                        } catch (Exception e) {
                                            System.out.println("Eccezione strana nella CLI");
                                            e.printStackTrace();
                                        }
                                        break;
                                    }
                                    case 8: {
                                        clientMatch.tryAnotherMainAction();
                                        System.out.println("Stai aggiungendo una azione principale...");
                                        break;
                                    }
                                    case 0: {
                                        System.out.println("Passa il turno!");
                                        clientMatch.skipTurn();
                                        break;
                                    }
                                    default: {
                                        System.out.println(" Riprova. ");
                                        badInput = true;
                                        break;
                                    }
                                }
                                if (badInput) throw new BadInputException();

                        } catch (BadInputException badExcept){
                                System.out.println("BADINPUT catchata!");
                                return;
                                //displayAndTryActions();
                        }
                        } catch (IndexOutOfBoundsException indexExc) {
                        }
                }
               // }
            }
        });
    }

    private CouncilorColor councilourInit() throws Exception {
        // service.execute(()->{}); //TODO OOOOOOOOOOOOOO DIOOOOO
        System.out.println("Consiglieri da eleggere disponibili:");
        System.out.println(clientMatch.getBank().getCouncilors());
        System.out.println("Scegli un consigliere in base al colore:");
        System.out.println("1: VIOLET,\n" +
                "  2:  WHITE,\n" +
                "  3:  BLACK,\n" +
                "  4:  ORANGE,\n" +
                "  5:  PINK,\n" +
                "  6:  BLUE;");

        if (canPlay()) {
            switch (input.nextInt()) {
                case 1:
                    return CouncilorColor.VIOLET;
                case 2:
                    return CouncilorColor.WHITE;
                case 3:
                    return CouncilorColor.BLACK;
                case 4:
                    return CouncilorColor.ORANGE;
                case 5:
                    return CouncilorColor.PINK;
                case 6:
                    return CouncilorColor.BLUE;
            }
        } else {
            timerExceed();
        }
        return null;
    }

    private PoliticCard selectPoliticCard(ArrayList<PoliticCard> selectedBefore) {
        int scelta;
        ArrayList<Integer> indexes = new ArrayList<>();
        ArrayList<Integer> selectedIds = new ArrayList<>();
        ArrayList<PoliticCard> userCards = clientMatch.getUser().getPoliticCards(); //le vere carte
        System.out.println("Ecco le tue carte politiche: ");
        System.out.println(userCards);
        for (PoliticCard card: selectedBefore) {
            selectedIds.add(card.getIdInColor());
        }


        System.out.println("SelectedBefore: " +selectedBefore);
        for (int i = 1; i <= userCards.size(); i++) {

            if (!(selectedBefore.contains(userCards.get(i-1)) && selectedIds.contains(userCards.get(i-1).getIdInColor()))  ){
                System.out.println(i +" : "+userCards.get(i-1));
                indexes.add(i);
            }

        }

        scelta = input.nextInt();
        System.out.println("Scelto: " + scelta);
        if (scelta > userCards.size() || !indexes.contains(scelta)) {
            System.out.println("E' nullll");
            return null;
        } else return userCards.get(scelta - 1);
    }

    private BuildingCard selectBuildingCard(String regionName) {
        int scelta;
        switch (regionName) {
            case "BEACH": {
                System.out.println("Le carte politiche della Spiaggia che puoi comprare: ");
                for (int i = 0; i < clientMatch.getSeaBuildingCard().size(); i++) {
                    int j = i + 1;
                    System.out.println(j + " : " + clientMatch.getSeaBuildingCard().get(i));
                }
                scelta = input.nextInt();
                if (scelta > 0 && scelta < 3) {
                    return clientMatch.getSeaBuildingCard().get(scelta - 1);
                }
            }
            case "MOUNTAIN": {
                System.out.println("Le carte politiche della Montagna che puoi comprare: ");
                for (int i = 0; i < clientMatch.getMountainBuildingCards().size(); i++) {
                    int j = i + 1;
                    System.out.println(j + " : " + clientMatch.getMountainBuildingCards().get(i));
                }
                scelta = input.nextInt();
                if (scelta > 0 && scelta < 3) {
                    return clientMatch.getMountainBuildingCards().get(scelta - 1);
                }
            }
            case "HILL": {
                System.out.println("Le carte politiche della Collina che puoi comprare: ");
                for (int i = 0; i < clientMatch.getHillBuildingCards().size(); i++) {
                    int j = i + 1;
                    System.out.println(j + " : " + clientMatch.getHillBuildingCards().get(i));
                }
                scelta = input.nextInt();
                if (scelta > 0 && scelta < 3) {
                    return clientMatch.getHillBuildingCards().get(scelta - 1);
                }
            }
        }
        return null;
    }


    private City selectCity(){
        ArrayList<City> allcities;
        allcities = clientMatch.getGameMap().getAllCities();
        System.out.println("Seleziona la città! ");
        int i =1;
        for (i=1; i<allcities.size(); i++){
            System.out.println(i + " : " +allcities.get(i-1));
        }
        int scelta = input.nextInt();
        System.out.println("Selezionata: "+allcities.get(i-1).getName());
        return allcities.get(scelta-1);
    }

    private String balconyInit() throws Exception {
        System.out.println(ANSI_YELLOW + "La situazione dei Balconi: ");

        clientMatch.getAllBalconies().forEach(Balcony::print);
        System.out.println("Scegli un balcone:");
        System.out.println("1: Mare, \n 2: Montagna, \n 3: Collina, \n 4: Balcone del Re" + ANSI_RESET);
        if (canPlay()) {
            switch (input.nextInt()) {
                case 1:
                    return RegionName.BEACH.name();
                case 2:
                    return RegionName.MOUNTAIN.name();
                case 3:
                    return RegionName.HILL.name();
                case 4:
                    return "KING";
            }
        } else {
            timerExceed();
        }
        return null;

    }

    private void displaySituation() {
        System.out.println(ANSI_PURPLE + "Situazione attuale:" + clientMatch.print() + "\n" + ANSI_RESET);

    }

    private void timerExceed() {
        System.out.println("Tempo scaduto, mi dispiace!");
        clientManager.actionIsNotPossibile(new ActionNotPossibleException("Tempo " + userPlaying + " Scaduto"));
    }

    private boolean canPlay() {
        String myName = clientMatch.getUser().getName();
        if (myName.equalsIgnoreCase(userPlaying)) {
            return true;
        } else return false;
    }

    private BuildingCard chooseBuilding() {
        ArrayList<String> initials = new ArrayList<>();
        ArrayList<BuildingCard> cards = clientMatch.getUser().getBuildingCards();
        if (cards.size()==0) {
            System.out.println("Dovresti prima acquistare una tessera permesso di costruzione!");
            return  null;
        }
        for (int i = 0; i < cards.size(); i++) {
            System.out.println(i + 1 + " :" + cards.get(i)); //TODO: .print()
            //TODO: NON SO COME FARE A GETTARE LA CITTà DATO L'INPUT UTENTE DELL'INIZIALE
        }
        try {

            int scelta = input.nextInt();
            return cards.get(scelta - 1);
        } catch (IndexOutOfBoundsException e){
        }
        return null;
    }

    private void printMarkets(){
        User user = clientMatch.getUser();
        System.out.println("Le carte politiche che puoi vendere: ");
        for (PoliticCard politicCard: user.getPoliticCards()) {
            System.out.println(politicCard.print());
        }
        System.out.println("Le carte permesso di costruzione che puoi vendere: ");
        for (BuildingCard buildingCard: user.getBuildingCards() ) {
            System.out.println(buildingCard.getDetails());
        }
        System.out.println("Puoi vendere i tuoi "+user.getHelpers() + " aiutanti");

        System.out.println("1: Vendi carte politiche -  2: Vendi Carte permesso -  3: Vendi aiutanti");
        switch (input.nextInt()){
            case 1: {
                int i=0;
                for (i=0; i<user.getPoliticCards().size(); i++){
                    System.out.println(i+1+ " : "+user.getPoliticCards().get(i));
                }
                int scelta = input.nextInt()-1; //leggo e tolgo uno per allinearmi all'array
                int prezzo = selectMarketPrice();
                if (scelta >0 && scelta<= user.getPoliticCards().size() && prezzo>0) {
                    PoliticCard politicCard = user.getPoliticCards().get(scelta);
                    clientMatch.sellItem(politicCard,prezzo);
                }
                break;
            }
            case 2: {
                int i =0;
                for (i=0; i<user.getBuildingCards().size() ; i++){
                    System.out.println(i+1 + " : "+user.getBuildingCards().get(i));
                }
                int scelta = input.nextInt()-1;
                int prezzo = selectMarketPrice();
                if (scelta > 0 && scelta <= user.getBuildingCards().size() && prezzo>0){
                    BuildingCard buildingCard = user.getBuildingCards().get(scelta);
                    clientMatch.sellItem(buildingCard,prezzo);
                }
                break;
            }
            case 3: {
                System.out.println("Quanti dei tuoi " +user.getHelpers() + " aiutanti vuoi vendere?");
                //int scelta = input.nextInt();
                System.out.println("Scegli rezzo unitario di vendita: ");
                int costo = input.nextInt();
                if (user.getHelpers()>0 && costo>0 ){
                    clientMatch.sellItem(new Helper(),costo);
                }

                break;
            }
            case 0:{
                break;
            }
            default: printMarkets();
        }
    }

    private void updateMarketCLI(){
        this.clientMarket = clientMatch.getClientMarket();
    }

    private ArrayList<BuyableObject> getBuyableObjects(){
        ArrayList<BuyableObject> products = new ArrayList<>();
        HashMap<String, ArrayList<BuyableObject>> market = clientMarket.getClientMarket();
        for (Map.Entry<String, ArrayList<BuyableObject>> entry : market.entrySet()) {
            ArrayList<BuyableObject> userMarket = clientMarket.getElementFromMarket(entry.getKey());
            products.addAll(userMarket);
        }
        return products;
    }
    private String getInitial() {
        return null;
    }

    private int selectMarketPrice(){
        System.out.println("Scrivi un prezzo per la messa in vendita dell'oggetto:");
        return input.nextInt();

    }
    private void showBuyMarket(){
        ClientMarket clientMarket = clientMatch.getClientMarket();
        System.out.println(ANSI_YELLOW + "***** Puoi comprare *****" + ANSI_RESET);
        System.out.println(ANSI_BLUE + "Id" + "   " + "Tipo" + "       " + "Costo" + ANSI_RESET);
        HashMap<String, ArrayList<BuyableObject>> marketMap = clientMarket.getClientMarket();
        for (Map.Entry<String, ArrayList<BuyableObject>> market : marketMap.entrySet()) {
            ArrayList<BuyableObject> marketOfClient = market.getValue();
            for (BuyableObject object : marketOfClient) {
                System.out.println(object.getId() + "   " + object.getObject().getDetails() + "       " + object.getPrice());
            }
        }
        System.out.print(ANSI_RED+"Inserisci l'id dell'oggetto che vuoi comprare: "+ANSI_RESET);
        try {
            int idObject = input.nextInt();
            if (clientMarket.marketContainsId(idObject))
                clientMatch.buyItem(idObject);
            else
                System.out.println("Oggetto non presente!");

        }catch (IndexOutOfBoundsException indexExc) {
            System.out.println("Ehilà");}

    }
    private void asciiArt() {
        switch (Randomizer.getRandom(1, 2)) {
            case 1: {
                System.out.println("  ______                                           __  __                   ______          ______                               \n" +
                        " /      \\                                         |  \\|  \\                 /      \\        /      \\                              \n" +
                        "|  $$$$$$\\  ______   __    __  _______    _______  \\$$| $$        ______  |  $$$$$$\\      |  $$$$$$\\ ______   __    __   ______  \n" +
                        "| $$   \\$$ /      \\ |  \\  |  \\|       \\  /       \\|  \\| $$       /      \\ | $$_  \\$$      | $$_  \\$$/      \\ |  \\  |  \\ /      \\ \n" +
                        "| $$      |  $$$$$$\\| $$  | $$| $$$$$$$\\|  $$$$$$$| $$| $$      |  $$$$$$\\| $$ \\          | $$ \\   |  $$$$$$\\| $$  | $$|  $$$$$$\\\n" +
                        "| $$   __ | $$  | $$| $$  | $$| $$  | $$| $$      | $$| $$      | $$  | $$| $$$$          | $$$$   | $$  | $$| $$  | $$| $$   \\$$\n" +
                        "| $$__/  \\| $$__/ $$| $$__/ $$| $$  | $$| $$_____ | $$| $$      | $$__/ $$| $$            | $$     | $$__/ $$| $$__/ $$| $$      \n" +
                        " \\$$    $$ \\$$    $$ \\$$    $$| $$  | $$ \\$$     \\| $$| $$       \\$$    $$| $$            | $$      \\$$    $$ \\$$    $$| $$      \n" +
                        "  \\$$$$$$   \\$$$$$$   \\$$$$$$  \\$$   \\$$  \\$$$$$$$ \\$$ \\$$        \\$$$$$$  \\$$             \\$$       \\$$$$$$   \\$$$$$$  \\$$      \n" +
                        "                                                                                                                                 \n" +
                        "                                                                                                                                 \n" +
                        "                                                                                                                                 \n");
                break;
            }
            case 2: {
                System.out.println("      ...                                                                .          ..                                                                             \n" +
                        "   xH88\"`~ .x8X                                                         @88>  x .d88\"                   oec :       oec :                                          \n" +
                        " :8888   .f\"8888Hf        u.      x.    .        u.    u.               %8P    5888R             u.    @88888      @88888         u.      x.    .        .u    .   \n" +
                        ":8888>  X8L  ^\"\"`   ...ue888b   .@88k  z88u    x@88k u@88c.       .      .     '888R       ...ue888b   8\"*88%      8\"*88%   ...ue888b   .@88k  z88u    .d88B :@8c  \n" +
                        "X8888  X888h        888R Y888r ~\"8888 ^8888   ^\"8888\"\"8888\"  .udR88N   .@88u    888R       888R Y888r  8b.         8b.      888R Y888r ~\"8888 ^8888   =\"8888f8888r \n" +
                        "88888  !88888.      888R I888>   8888  888R     8888  888R  <888'888k ''888E`   888R       888R I888> u888888>    u888888>  888R I888>   8888  888R     4888>'88\"  \n" +
                        "88888   %88888      888R I888>   8888  888R     8888  888R  9888 'Y\"    888E    888R       888R I888>  8888R       8888R    888R I888>   8888  888R     4888> '    \n" +
                        "88888 '> `8888>     888R I888>   8888  888R     8888  888R  9888        888E    888R       888R I888>  8888P       8888P    888R I888>   8888  888R     4888>      \n" +
                        "`8888L %  ?888   ! u8888cJ888    8888 ,888B .   8888  888R  9888        888E    888R      u8888cJ888   *888>       *888>   u8888cJ888    8888 ,888B .  .d888L .+   \n" +
                        " `8888  `-*\"\"   /   \"*888*P\"    \"8888Y 8888\"   \"*88*\" 8888\" ?8888u../   888&   .888B .     \"*888*P\"    4888        4888     \"*888*P\"    \"8888Y 8888\"   ^\"8888*\"    \n" +
                        "   \"888.      :\"      'Y\"        `Y\"   'YP       \"\"   'Y\"    \"8888P'    R888\"  ^*888%        'Y\"       '888        '888       'Y\"        `Y\"   'YP        \"Y\"      \n" +
                        "     `\"\"***~\"`                                                 \"P'       \"\"      \"%                     88R         88R                                            \n" +
                        "                                                                                                        88>         88>                                            \n" +
                        "                                                                                                        48          48                                             \n" +
                        "                                                                                                        '8          '8                                             \n");
            }

        }
    }
}
