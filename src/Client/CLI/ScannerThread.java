package Client.CLI;

import javafx.concurrent.Task;

import java.util.Scanner;

/**
 * Created by edoar on 20/06/2016.
 */
public class ScannerThread extends Task<Integer> {
    Scanner input = new Scanner(System.in);
    @Override
    protected Integer call() throws Exception {

        return input.nextInt();
    }
}
