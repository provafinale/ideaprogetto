package Client.Inteface;

/**
 * Created by edoar on 11/06/2016.
 */
public interface GraphicInterface {

    void callChooseMapUI();
    void changeToGameUI(String map);
    void startInterface();
    void sendChosenMap(String map);
    void printException(String msg);
}
