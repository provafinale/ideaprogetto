package Client.Inteface;

import Client.ClientManager;
import Client.ClientMarket;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Miscellaneous.Exceptions.OurException;
import Server.Market.Buyable;
import Server.Market.BuyableObject;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Samuele on 11/05/2016.
 */
public interface NetworkInterface extends Remote {

    void connetti (String ip,String nickname) throws IOException, NotBoundException, AlreadyBoundException,RemoteException;
    void setSnapshot(Snapshot snapshot)throws RemoteException;
    void serverException(OurException ourException)throws RemoteException;
    void chooseMap()throws RemoteException;
    void sendChosenMap(String map)throws RemoteException;
    void setManager(ClientManager manager)throws RemoteException;
    void sendAction(GenericAction action) throws RemoteException;
    void setChosenMap(String map)throws RemoteException;
    void skipTurn() throws RemoteException;
    void setWhoIsInTurn(String user) throws RemoteException;
    void sendPurchase(int idItem) throws RemoteException;
    void sendItemToSell(Buyable buyable, int price) throws RemoteException;
    void updateMarket(ClientMarket clientMarket) throws RemoteException;
    void disconnect() throws RemoteException;


}
