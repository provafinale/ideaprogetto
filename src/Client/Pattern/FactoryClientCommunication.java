package Client.Pattern;

import Client.Class.NetworkRMI;
import Client.Class.NetworkSocket;
import Client.Inteface.NetworkInterface;

import java.rmi.RemoteException;

/**
 * Created by Samuele on 12/05/2016.
 */
/** Ritorna l'istanza di un nuovo oggetto di comunicazione in base all'input dato dall'utente.
 * Consente di utilizzare i metodi dell'interfaccia di rete definita.*/
public class FactoryClientCommunication {

    public static NetworkInterface getClient(String tipo) throws RemoteException {
        if (tipo.equals("RMI"))
            return new NetworkRMI();
        if (tipo.equals("Socket"))
            return new NetworkSocket();
        return null;
    }
}
