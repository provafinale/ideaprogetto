package Client.Pattern;

import Client.ClientManager;
import Client.ClientMatch;
import Client.CLI.CommandLineInterface;
import Client.GraphicalUserInterface;
import Client.Inteface.GraphicInterface;

/**
 * Created by edoar on 11/06/2016.
 */
/** Factory pattern per il recupero runtime della grafica*/
public class FactoryClientGraphic {

    public static GraphicInterface getClient(String tipo, ClientManager clientManager, ClientMatch clientMatch) {
        if (tipo.equals("CLI"))
            return new CommandLineInterface(clientManager,clientMatch);
        if (tipo.equals("GUI"))
            return new GraphicalUserInterface(clientManager,clientMatch);
        return null;
    }

}
