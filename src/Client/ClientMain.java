package Client;

import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.util.ResourceBundle;

/**
 * Created by Samuele on 11/05/2016.
 */
/** Main class*/
public class ClientMain implements Initializable{

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**Alloca il manager e lo inizializza*/
    public static void main(String[] args) throws IOException, NotBoundException, AlreadyBoundException {
        ClientManager clientManager = new ClientManager();
        clientManager.setMySelf(clientManager);
        clientManager.initClient();
    }






}