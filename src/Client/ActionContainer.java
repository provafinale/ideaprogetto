package Client;

import Client.GUI.Game.GameController;
import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Places.Balcony.KingBalcony;
import Game.Places.Balcony.RegionBalcony;
import Game.Places.City;
import Game.Stuff.Councilor;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by edoar on 21/06/2016.
 */

/** Classe di storage-appoggio per il mantenimento degli stati delle azioni lato GUI che prevedono più steps*/
public class ActionContainer {
    private RegionBalcony balconyForAction;
    private KingBalcony kingBalcony;
    private ArrayList<PoliticCard> politicCardsForAction = new ArrayList<>();
    private ArrayList<City> citiesForAction = new ArrayList<>();
    private BuildingCard buildingCardForAction;
    private String buildingDeck;
    private Councilor councilorForAction;
    private GameController gameController;
    private ClientMatch clientMatch;
    boolean actionCancel;
    boolean actionIsStarted;
    boolean next;
    private ExecutorService actionService = Executors.newCachedThreadPool();


    public ActionContainer(ClientMatch clientMatch) {
        this.clientMatch = clientMatch;
        resetActionContainer();
    }


    public void changeBuildingCard() {
        if (!actionIsStarted) {
            resetActionContainer();
            actionIsStarted = true;

            actionService.execute(() -> {
                gameController.showBuildingDeck();
                try {
                    while (buildingDeck == null && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    if (!actionCancel)
                        clientMatch.tryChangeBuildingCards(buildingDeck);
                    resetActionContainer();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void buildEmpory() throws Exception {
        if (!actionIsStarted) {
            resetActionContainer();
            actionIsStarted = true;
            gameController.showBank(true);
            gameController.disableAllElements();
            actionService.execute(() -> {
                try {
                    while (buildingCardForAction == null && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    gameController.closeBank();
                    gameController.enableOnlyCities();
                    while (citiesForAction.size() < 1 && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    if (!actionCancel)
                        clientMatch.tryBuildEmpory(buildingCardForAction, citiesForAction.get(0));
                    resetActionContainer();

                } catch (InterruptedException inter) {
                    inter.printStackTrace();
                }
            });
        }
    }

    public void buildEmporyWithKing(){
        if (!actionIsStarted){
            resetActionContainer();
            actionIsStarted = true;
            actionService.execute(()->{
                gameController.enableOnlyPoliticCards();
                try {
                    while (!next && !actionCancel) {
                        synchronized (this) {this.wait();}
                    }
                    gameController.enableOnlyCities();
                    next=false;
                    while (!next && !actionCancel){
                        synchronized (this){this.wait();}
                    }
                    if (!actionCancel) {
                        clientMatch.tryBuildWithKing(politicCardsForAction, citiesForAction);
                        System.out.println("Azione eseguita!! Buildato emporio!");
                    }
                    resetActionContainer();
                }catch (Exception e){
                    System.out.println(e);}});
        }
    }

    public synchronized void buyBuildingCard() {
        if (!actionIsStarted) {
            resetActionContainer();
            actionIsStarted = true;
            actionService.execute(() -> {
                gameController.enableOnlyPoliticCards();
                try {
                    while (!next && !actionCancel) {
                        synchronized (this) {
                            this.wait();
                        }
                    }
                    gameController.enableOnlyBalconies();
                    while (balconyForAction == null && !actionCancel) {
                        synchronized (this) {
                            this.wait();
                        }
                    }
                    gameController.enableOnlyBuildingCards();
                    while (buildingCardForAction == null && !actionCancel) {
                        synchronized (this) {
                            this.wait();
                        }
                    }

                    System.out.println("building selezionata");
                    if (!actionCancel)
                        clientMatch.tryBuyBuildingCard(politicCardsForAction, balconyForAction.getRegionName(), buildingCardForAction);
                    resetActionContainer();
                    gameController.stopAllAnimations();
                    gameController.disableAllElements();
                } catch (InterruptedException inter) {
                    inter.printStackTrace();
                }
            });
        }
    }
    public void engageHelper(){
        clientMatch.tryEngageHelper();
        resetActionContainer();
    }

    public void anotherMainAction(){
        clientMatch.tryAnotherMainAction();
        resetActionContainer();
    }

    public void electCouncilor() throws Exception {
        if (!actionIsStarted) {
            resetActionContainer();
            actionIsStarted = true;
            gameController.showBank(true);
            actionService.execute(() -> {
                try {
                    gameController.disableAllElements();
                    while (councilorForAction == null && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    gameController.closeBank();
                    gameController.enableOnlyBalconies();
                    while (balconyForAction == null && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    gameController.enableAllElements();
                    if (!actionCancel)
                        clientMatch.tryElectCouncilor(councilorForAction.getCouncilorColor(), balconyForAction.getRegionName());
                    resetActionContainer();

                } catch (InterruptedException inter) {
                    inter.printStackTrace();
                }
            });
        }
    }

    public void electCouncilorWithHelper() throws Exception {
        if (!actionIsStarted) {
            resetActionContainer();
            actionIsStarted = true;
            gameController.showBank(true);
            actionService.execute(() -> {
                try {
                    gameController.disableAllElements();
                    while (councilorForAction == null && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    gameController.closeBank();
                    gameController.enableOnlyBalconies();
                    while (balconyForAction == null && !actionCancel) {
                        synchronized (this) {
                            wait();
                        }
                    }
                    gameController.enableAllElements();
                    if (!actionCancel)
                        clientMatch.tryElectCouncilorWithHelper(councilorForAction.getCouncilorColor(), balconyForAction.getRegionName());
                    resetActionContainer();

                } catch (InterruptedException inter) {
                    inter.printStackTrace();
                }
            });
        }
    }



    /** Ripristina la situazione del Container in modo da poterlo usare per l'azione successiva*/
    public void resetActionContainer(){
        balconyForAction = null;
        buildingDeck = null;
        politicCardsForAction = new ArrayList<>();
        citiesForAction = new ArrayList<>();
        buildingCardForAction = null;
        councilorForAction = null;
        actionIsStarted = false;
        actionCancel = false;
        next = false;
    }

    // I setters risvegliano gli steps

    public void setBalconyForAction(RegionBalcony balconyForAction) {
        this.balconyForAction = balconyForAction;
        synchronized (this){this.notify();}
    }

    public void setBuildingDeck(String buildingDeck) {
        this.buildingDeck = buildingDeck;
        synchronized (this) {this.notify();}
    }


    public void setPoliticCardsForAction(PoliticCard politicCard, ImageView imageView) {
        politicCardsForAction.add(politicCard);
        gameController.selectedEffect(imageView, 80);
        synchronized (this){this.notify();}
    }


    public void addCity(City city){
        if (actionIsStarted) {
            this.citiesForAction.add(city);
            synchronized (this) {
                this.notify();
            }
        }
    }
    public void setBuildingCardForAction(BuildingCard buildingCardForAction, Pane pane) {
        if (actionIsStarted) {
            this.buildingCardForAction = buildingCardForAction;
            //gameController.selectedEffect(pane, 80);
            synchronized (this) {
                this.notify();
            }
        }
    }

    public void setCouncilorForAction(Councilor councilorForAction) {
        if (actionIsStarted) {
            this.councilorForAction = councilorForAction;
            synchronized (this) {
                this.notify();
            }
        }
    }

    public void setGameController(GameController gameController){
        this.gameController = gameController;
    }

    public void setKingBalcony(KingBalcony kingBalcony) {
        if (actionIsStarted) {
            this.kingBalcony = kingBalcony;
            synchronized (this) {
                this.notify();
            }
        }
    }

    public void setNext(boolean bool){
        if (actionIsStarted) {
            next = bool;
            synchronized (this) {
                this.notify();
            }
        }
    }

    public void setActionCancel(boolean bool){
        actionCancel = bool;
        synchronized (this){this.notify();}
    }

    public void clearSituation(){
        if (actionIsStarted)
            setActionCancel(true);
    }
}
