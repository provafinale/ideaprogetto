package Client;

import Server.Market.BuyableObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Samuele on 28/06/2016.
 */

/** Detiene la situazione del market per ogni utente*/
public class ClientMarket implements Serializable {
    private HashMap<String, ArrayList<BuyableObject>> clientMarket = new HashMap<>();
    private boolean canBuy;
    private boolean canSell;

    public ClientMarket() {}

    public ClientMarket(HashMap<String, ArrayList<BuyableObject>> clientMarket,boolean canBuy,boolean canSell) {
        this.clientMarket = clientMarket;
        this.canBuy = canBuy;
        this.canSell = canSell;
    }

    public boolean marketContainsId(int id){
        for (Map.Entry<String,ArrayList<BuyableObject>> objects : clientMarket.entrySet()){
            for (BuyableObject buyableObject : objects.getValue()){
                if (buyableObject.getId() == id)
                    return true;
            }
        }
        return false;
    }
    /** @return true se l'utente può comprare*/
    public boolean canBuy() {
        return canBuy;
    }

    /** @return true se l'utente può vendere*/
    public boolean canSell() {
        return canSell;
    }

    public HashMap<String, ArrayList<BuyableObject>> getClientMarket() {
        return clientMarket;
    }

    public void setClientMarket(HashMap<String,ArrayList<BuyableObject>> market){
        this.clientMarket = market;
    }
    public ArrayList<BuyableObject> getElementFromMarket(String username){
        return clientMarket.get(username);
    }

    @Override
    public String toString() {
        return "ClientMarket{" +
                "clientMarket=" + clientMarket +
                ", canBuy=" + canBuy +
                ", canSell=" + canSell +
                '}';
    }
}

