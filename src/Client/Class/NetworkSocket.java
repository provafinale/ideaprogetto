package Client.Class;

import Client.ClientManager;
import Client.ClientMarket;
import Client.Inteface.NetworkInterface;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.OurException;
import Miscellaneous.Jsonifier;
import Server.Comunicazione.Signal;
import Server.Market.Buyable;
import Server.Market.BuyableObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Samuele on 11/05/2016.
 */
public class NetworkSocket implements NetworkInterface,Runnable {
    private Snapshot snapshot;
    //Socket's required information
    private int port = 20000;
    private Socket socket;
    private PrintWriter writer;
    private BufferedReader reader;
    private ClientManager clientManager;

    private ExecutorService executor = Executors.newCachedThreadPool();




    @Override
    public void connetti(String ip,String nickname) throws IOException {
        socket = new Socket(ip,port);
        writer = new PrintWriter(socket.getOutputStream(),true);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer.println(nickname); //Invio il nickname al Server al momento della connessione.
        executor.execute(this); //start .run
    }

    @Override
    public void setSnapshot(Snapshot snapshot) {
        clientManager.setSnapshotToModel(snapshot);
    }

    @Override
    public void serverException(OurException ourException) throws RemoteException {
        //USELESS
    }

    @Override
    public void chooseMap() throws RemoteException {
        clientManager.callChooseMapUI();
    }

    @Override
    /**Invia la mappa scelta*/
    public void sendChosenMap(String map) throws RemoteException {
        Signal signal = new Signal(Signal.USER_CHOOSE,map);
        String signalToSend = Jsonifier.serialize(signal);
        writer.println(signalToSend);
    }

    @Override
    public void setManager(ClientManager client) {
        this.clientManager = client;
    }

    @Override
    public void sendAction(GenericAction action) throws RemoteException {
        Signal signal = new Signal(Signal.IS_ACTION, Jsonifier.serialize(action));
        writer.println(Jsonifier.serialize(signal)); //Dispatch
    }

    @Override
    public void setChosenMap(String map) throws RemoteException {

    }

    @Override
    public void skipTurn() throws RemoteException {
        Signal signal = new Signal(Signal.SKIP_TURN,null);
        String signalToSend = Jsonifier.serialize(signal);
        writer.println(signalToSend);
    }

    @Override
    public void setWhoIsInTurn(String user) throws RemoteException {
        clientManager.setWhoIsInTurn(user);
    }

    @Override
    public void run() {
        String readed;
        System.out.println("NetworkSocket running...");
        while (true){
            try {
                readed = reader.readLine();
                System.out.println("Received Signal from server: unpacking...");
                Signal receivedSignal = Jsonifier.deserializeSignal(readed);
                onSignalReceived(receivedSignal);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }

        }
    }

    public void onSignalReceived(Signal signalReceived){
        String header = signalReceived.getHeader();
        System.out.println("SEGNALE SOCKET: "+header);
        String serializedPayload = signalReceived.getSerializedPayload();
        switch (header) {
            case Signal.IS_SNAPSHOT :{
                Snapshot snapshot = (Snapshot) Jsonifier.deserializePayload(signalReceived);
                clientManager.setSnapshotToModel(snapshot);
            }break;

            case Signal.IS_CHOOSE_MAP :{
                clientManager.callChooseMapUI();
            }break;

            case Signal.IS_CHOSEN_MAP: {
                clientManager.changeToGameUI(serializedPayload);
            }break;

            case Signal.IS_ACTION_NOT_POSSIBILE: {
                ActionNotPossibleException e =(ActionNotPossibleException)
                        Jsonifier.deserializePayload(signalReceived);
                    clientManager.actionIsNotPossibile(e);
                }
            case Signal.WHO_IS_IN_TURN: {
                try {
                    setWhoIsInTurn(serializedPayload);}
                catch (RemoteException e) {e.printStackTrace();}
            }break;
            case Signal.IS_MARKET:{

                //TODO: SOCKET ERROR
                ClientMarket clientMarket = (ClientMarket) Jsonifier.deserializePayload(signalReceived);
                clientManager.setMarketToModel(clientMarket);
            }
        }
    }

    //Invia il riferimento dell'elemento che si vuole comprare
    @Override
    public void sendPurchase(int idItem) throws RemoteException {
        String header = Signal.BUY_ITEM;
        String payLoad = String.valueOf(idItem);
        Signal signal = new Signal(header,payLoad);
        String signalToSend = Jsonifier.serialize(signal);
        writer.println(signalToSend);
    }

    //Invia prezzo e oggetto che si vuole vendere
    @Override
    public void sendItemToSell(Buyable buyable, int price) throws RemoteException {
        String header = Signal.SELL_ITEM;
        BuyableObject objectToSell = new BuyableObject(buyable,price,clientManager.getClientMatch().getUser().getName());
        String objectSerialized = Jsonifier.serialize(objectToSell);
        Signal signal = new Signal(header,objectSerialized);
        String signalToSend = Jsonifier.serialize(signal);
        writer.println(signalToSend);
    }

    @Override
    public void updateMarket(ClientMarket clientMarket) throws RemoteException {
        clientManager.setMarketToModel(clientMarket);
    }

    @Override
    public void disconnect() throws RemoteException {
        String header = Signal.DISCONNECT;
        Signal signal = new Signal(header,null);
        String signalToSend = Jsonifier.serialize(signal);
        writer.println(signalToSend);
    }

}
