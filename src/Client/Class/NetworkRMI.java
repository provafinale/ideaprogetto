package Client.Class;

import Client.ClientManager;
import Client.ClientMarket;
import Client.Inteface.NetworkInterface;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.OurException;
import Server.Comunicazione.Inteface.RMIDedicatedInterface;
import Server.Comunicazione.Inteface.RMIServerInterface;
import Server.Market.Buyable;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Samuele on 11/05/2016.
 */
public class NetworkRMI implements NetworkInterface {
    private RMIDedicatedInterface myObj;
    private int i=0;
    private ClientManager clientManager;
    private ExecutorService executorService = Executors.newCachedThreadPool();

    public NetworkRMI() throws RemoteException {
        UnicastRemoteObject.exportObject(this,0);
    }

    /** Una volta selezionato RMI come metodo di connessione, tento di connetermi al server RMI aggiungendomi ai suoi clients
     * @param ip server-RMI ip address
     * @param nickname il nickname del giocatore selezionato da interfaccia*/
    @Override
    public void connetti(String ip,String nickname) throws RemoteException, NotBoundException, AlreadyBoundException {
        Registry registry = LocateRegistry.getRegistry(ip); //get registry from local Server
        RMIServerInterface stub = (RMIServerInterface) registry.lookup("Server");

        String nameOfmyObject = stub.connection(); //Ritorna nome da cercare nel registro dell'oggetto dedicato.
        myObj = (RMIDedicatedInterface) registry.lookup(nameOfmyObject);

        myObj.setClient(this); // Così posso chiamare dei metodi del client, dal serverRMI.
        System.out.println("Client RMI Connesso.");
        myObj.addUser(nickname);
    }

    /** passa al manager lo snapshot appena ricevuto
     * @param snapshot lo snapshot appena ricevuto*/
    @Override
    public void setSnapshot(Snapshot snapshot) {
      //  executorService.execute(()->{
            clientManager.setSnapshotToModel(snapshot);
       // });
    }

    /** comunica al manager che vi è un'aggiornamento del market
     * @param market il market aggiornato dal server*/
    @Override
    public void updateMarket(ClientMarket market) throws RemoteException {
        executorService.execute(()->{
            System.out.println("AGGIORNO MARKET");
            clientManager.setMarketToModel(market);});
    }

    @Override
    public void disconnect() throws RemoteException {
        myObj.disconnect();
    }

    /** comunica al manager che è stata ricevuta un'eccezione dal server
     * @param ourException La nostra eccezione generica, contiene a runtime l'eccezione specifica*/
    @Override
    public void serverException(OurException ourException) throws RemoteException {
        executorService.execute(()->{
            System.out.println("In CLIENTRMI - SERVEREXCEPT");
            if (ourException.getClass().equals(ActionNotPossibleException.class)) {
                clientManager.actionIsNotPossibile((ActionNotPossibleException)ourException);
            }
        });

    }

    @Override
    public void chooseMap() throws RemoteException {
        clientManager.callChooseMapUI();
    }


    @Override
    public void sendChosenMap(String map) throws RemoteException {
        myObj.setMapToLogic(map);
    }

    @Override
    public void setManager(ClientManager client) {

        this.clientManager = client;
    }

    @Override
    public void sendAction(GenericAction action) throws RemoteException {
        executorService.execute(()->{
            try {
                myObj.receiveAction(action); //TODO: TEST
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public void setChosenMap(String map) throws RemoteException {
        System.out.println("Mappa Scelta: "+map);
        clientManager.changeToGameUI(map);
    }

    @Override
    public void skipTurn() throws RemoteException {
      //  executorService.execute(()->{
            try {

                myObj.skipTurn();

            } catch (RemoteException e) {
                e.printStackTrace();
            }
       // });
    }


    @Override
    public void setWhoIsInTurn(String user) throws RemoteException {
      //  executorService.execute(()->
                clientManager.setWhoIsInTurn(user);
    }

    //Invia l'elemento che si vuole comprare
    @Override
    public void sendPurchase(int idItem) throws RemoteException {
        myObj.buyElementFromMarket(idItem);
    }

    //Invia l'elemento che si vuole vendere
    @Override
    public void sendItemToSell(Buyable buyable, int price) throws RemoteException {
        myObj.addElemtentToMarket(buyable,price);
    }


}
