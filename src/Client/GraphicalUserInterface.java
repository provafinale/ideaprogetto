package Client;

import Client.GUI.ChooseMap.ChooseMapController;
import Client.GUI.Game.GameController;
import Client.GUI.Login.LoginController;
import Client.Inteface.GraphicInterface;
import Miscellaneous.Constant;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

/**
 * Created by Samuele on 11/06/2016.
 */

/** JAVAFX main-class*/
public class GraphicalUserInterface extends Application implements GraphicInterface{
    private Stage primaryStage;
    private ClientManager clientManager;
    private ClientMatch clientMatch;
    private GameController gameController;
    private FXMLLoader gameLoader = new FXMLLoader();
    private Scene gameScene;

    public GraphicalUserInterface(ClientManager clientManager,ClientMatch clientMatch) {
        this.clientManager = clientManager;
        this.clientMatch = clientMatch;
    }

    public GraphicalUserInterface(){
        clientManager = ClientManager.getMySelf();
        clientMatch = ClientMatch.getMySelf();
    }

    public void startInterface(){
        Application.launch();
    }

    @Override
    public void sendChosenMap(String map) {
        clientManager.sendChosenMap(map);
    }

    @Override
    public void printException(String msg) {
        gameController.showException(msg);
    }

    public void init() throws Exception {
        super.init();
        this.clientManager.setGraphicRef(this);

        System.out.println(clientManager);
    }

    /** punto di partenza di JAVAFX*/
    @Override
    public void start(Stage primaryStage) throws Exception {
        initializeGameController();

        this.primaryStage=primaryStage;
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("GUI/Login/LoginFXML.fxml"));
        primaryStage.setTitle("Log In");
        primaryStage.setScene(new Scene(loader.load()));

        LoginController loginController = loader.getController();
        loginController.setClientManager(clientManager);


        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);}});

        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /** Inizializza il controller della componente di gioco*/
    public void initializeGameController(){
        gameLoader.setLocation(getClass().getResource("GUI/Game/GameFXML.fxml"));
        try {
            gameScene = new Scene(gameLoader.load(), Constant.START_WIDTH, Constant.START_HEIGHT);
            gameScene.getStylesheets().add(getClass().getResource("GUI/Game/gameInterface.css").toExternalForm());
           // gameScene.getStylesheets().add(getClass().getResource("GUI/Game/jfoenix.css").toExternalForm());
        } catch (IOException e) {
            e.printStackTrace();}

        gameController = gameLoader.getController();
        gameController.setModel(clientMatch);
    }

    /**Wizard Scelta Mappa*/
    public void callChooseMapUI() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                FXMLLoader loader= new FXMLLoader();
                loader.setLocation(getClass().getResource("GUI/ChooseMap/ChooseMapFXML.fxml"));

                primaryStage.setTitle("Scegli la Mappa!");
                try {
                    Scene scene = new Scene(loader.load());
                    primaryStage.setScene(scene);
                    primaryStage.setX(10);
                    primaryStage.setY(10);
                    scene.getStylesheets().add(getClass().getResource("GUI/ChooseMap/chooser.css").toExternalForm());

                    primaryStage.focusedProperty().addListener((observable, oldValue, newValue) -> {
                        gameController.setFocus(newValue);});

                } catch (IOException e) {
                    e.printStackTrace();
                }
                ChooseMapController chooseMapController = loader.getController();
                chooseMapController.setClientManager(clientManager);
                primaryStage.setResizable(true);

                primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent t) {
                        disconnect();}});

                primaryStage.show();}});

    }

    /**Chiamata all'interfaccia di Gioco: inizia la partita. Setto interfaccia resizable
     * @param map la mappa selezionata in fase di avvio*/
    public void changeToGameUI(String map){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                primaryStage.setTitle("Consiglio dei Quattro");
                primaryStage.setScene(gameScene);
                gameController.setMap(map);


                Group group = gameController.getGroup(); //Il group che resizerà
                ChangeListener changeListenerDimension = (observable, oldValue, newValue) -> {
                    Double newWidth, newHeight, min, widthDiff, heightDiff;
                    Double xScale, yScale;
                    newWidth = gameScene.getWidth();
                    newHeight = gameScene.getHeight();
                    xScale = newWidth/Constant.START_WIDTH; //coefficiente di scalata X
                    yScale = newHeight/Constant.START_HEIGHT; //coefficiente di scalata y
                    group.setScaleX(xScale);
                    group.setScaleY(yScale);
                    min = Double.min(newWidth, newHeight);
                    System.out.println("min: " +min);
                    widthDiff = newWidth - Constant.START_WIDTH ; //Il pezzo di X che sto togliendo
                    heightDiff = newHeight - Constant.START_HEIGHT; //il pezzo di Y che sto togliendo
                    //trasla in alto a sinistra
                    group.setTranslateX(widthDiff/2);
                    group.setTranslateY(heightDiff/2);


                };
                ReadOnlyDoubleProperty widthProperty;
                widthProperty = gameScene.widthProperty();
                widthProperty.addListener(changeListenerDimension);


                ReadOnlyDoubleProperty heightProperty;
                heightProperty = gameScene.heightProperty();
                heightProperty.addListener(changeListenerDimension);


                gameScene.setFill(Color.valueOf("#383d3b"));
                primaryStage.setResizable(true); //YES

                primaryStage.setX(10);
                primaryStage.setY(10);


                primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent t) {
                        disconnect();}});

                primaryStage.show();
            }
        });

    }

    /** consente la disconnessione del client in modalita GUI */
    public void disconnect(){
        clientManager.disconnect();
        System.out.println("Chiusura in corso");
        Platform.exit();
        System.exit(0);

    }
}
