package Client;

import Client.Inteface.GraphicInterface;
import Client.Inteface.NetworkInterface;
import Client.Pattern.FactoryClientCommunication;
import Client.Pattern.FactoryClientGraphic;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.OurException;
import Server.Market.Buyable;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 * Created by edoar on 11/06/2016.
 */
/** Funge da punto di contatto tra network, UI, e situazione del match*/
public class ClientManager {

    private static ClientManager mySelf;
    private Scanner keyboard = new Scanner(System.in);
    private String choosenUI;
    private String choosenNetwork;
    private FactoryClientCommunication network;
    private NetworkInterface networkRef;
    private GraphicInterface graphicRef;
    private ClientMatch clientMatch = new ClientMatch(this);


    public void setMySelf(ClientManager clientManager){
        mySelf=clientManager;
    }

    public static ClientManager getMySelf(){
        return mySelf;
    }


    /** Inizializza UI e endpoint di connessione */
    public void initClient() throws RemoteException {
        clientMatch.setMySelf(clientMatch);
        choosenNetwork = chooseConnection();
        choosenUI = chooseGraphic();

        networkRef = FactoryClientCommunication.getClient(choosenNetwork);
        networkRef.setManager(this);

        graphicRef = FactoryClientGraphic.getClient(choosenUI,this,clientMatch); //Return GraphicalUserInterface o CLIUserInterface
        graphicRef.startInterface();
    }

    /** Aggiorna il riferimento alla grafica*/
    public void setGraphicRef(GraphicInterface graphicRef){
        this.graphicRef = graphicRef;
    }

    private String chooseGraphic(){
        System.out.println("Scegli la UI da utilizzare: \n 1: GUI, 2: CLI(experimental)");
        int ui = keyboard.nextInt();
        switch (ui){
            case 1: {
                return Constant.GUI;
            }
            case 2: {
                return Constant.CLI;
            }
        default: return Constant.GUI;}
    }


    /** @return  la costante del nome del network in base alla scelta utente*/
    private String chooseConnection(){
        System.out.println("Scegli la comunicazione da utilizzare: \n 1: RMI, 2: Socket");
        int net = keyboard.nextInt();
        switch (net){
            case 1: {
                return Constant.RMI;
            }
            case 2: {
                return Constant.SOCKET;
            }
            default: return Constant.RMI;
        }
    }


    /** La chiameranno i client specifici qualora il server generi una actionNotPossibileException. Principalmente segnalerà lato GUI o CLI che non è
     * stato possibile effettuare l'azione.
     * @param  actionExcept l'eccezione da visualizzare*/
    public void actionIsNotPossibile(ActionNotPossibleException actionExcept){
        System.out.println("Qualcosa è andato storto, non hai potuto completare l'azione richiesta!");
        if (actionExcept.isSubbed()) {
            OurException subException = actionExcept.getSingleSubException();
            System.out.println(subException.getNick()+" "+subException.getMsg());
            graphicRef.printException(subException.getMsg());
        } else {
            System.out.println(actionExcept.getMsg());
            graphicRef.printException(actionExcept.getMsg());
        }
        System.out.println("Ripristino lo stato della partita all'ultimo corretto...");
        clientMatch.unpackSnapshot(clientMatch.getLastSnap()); //RIPRISTINO
    }

    /** Tenta di inviare l'azione desiderata sull'endpoint assegnato a runtime
     * @param actionToTry l'azione da inviare*/
    public void sendAction(GenericAction actionToTry){
        try {
            networkRef.sendAction(actionToTry);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    //Invia l'id dell'elemento che si vuole comprare
    public void sendBuyItem(int id){
        try {
            networkRef.sendPurchase(id);
        } catch (RemoteException e) {
            e.printStackTrace();}
    }

    //Invia l'elemento da mettere in vendita e il prezzo di vendita
    public void sendItemToMarket(Buyable buyable,int price){
        try {
            networkRef.sendItemToSell(buyable, price);
        } catch (RemoteException e) {
            e.printStackTrace();}
    }

    /** Comunica all'endpoint di comunicazione che si vuole passare il turno*/
    public void skipTurn(){
        try {
            networkRef.skipTurn();}
        catch (RemoteException e) {e.printStackTrace();}
    }

    /**Invia la mappa scelta dall'utente.*/
    public void sendChosenMap(String map){
        try {
            networkRef.sendChosenMap(map);}
        catch (RemoteException e) {
            e.printStackTrace();}
    }

    /** Comunica la volontà di disconnettere il client*/
    public void disconnect(){
        System.out.println("mi disconnetto");
        try {
            networkRef.disconnect();
        } catch (RemoteException e) {e.printStackTrace();}
    }

    /** Effettua la conessione al server
     * @param ip ip address del Server
     * @param nick il nickname digitato*/
    public void createClient(String ip,String nick) throws NotBoundException, IOException, AlreadyBoundException {
        networkRef.connetti(ip,nick);
    }

    public void setSnapshotToModel(Snapshot snapshot){
        clientMatch.unpackSnapshot(snapshot);
    }

    public void setMarketToModel(ClientMarket clientMarket){
        clientMatch.updateMarket(clientMarket);
    }

    public void callChooseMapUI(){
        graphicRef.callChooseMapUI();
    }

    public void changeToGameUI(String map){
        graphicRef.changeToGameUI(map);
    }

    /**Aggiorna il Text dell'Interfaccia che indica è il giocatore in turno.
     * @param user nickname dell'utente*/
    public void setWhoIsInTurn(String user){
        clientMatch.updateUserInTurn(user);
    }

    public ClientMatch getClientMatch() {
        return clientMatch;
    }

    public GraphicInterface getGraphicRef(){
        return graphicRef;
    }



}
