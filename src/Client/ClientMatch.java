package Client;

import Client.CLI.CommandLineInterface;
import Client.GUI.Game.GaugeTimer;
import Client.GUI.Game.Listener;
import Client.Inteface.MarketInterface;
import Game.Actions.*;
import Game.Bank;
import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Enemy;
import Game.Pawn.King;
import Game.Places.Balcony.Balcony;
import Game.Places.Balcony.KingBalcony;
import Game.Places.City;
import Game.Places.GameMap;
import Game.Places.Region;
import Game.Places.RegionName;
import Game.Snapshot;
import Game.Stuff.Councilor;
import Game.Stuff.CouncilorColor;
import Game.User;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.EmptyBankException;
import Server.Market.Buyable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by Samuele on 16/05/2016.
 */

/**La situazione della partita lato client: contiene solo lo stretto necessario inviato dal Server.
 * Solo l'utente giocante su quel client ha accesso ai suoi dati sensibili (es: carte politiche),
 * dei nemici conosce solo dati sommari (es: numero di carte politiche)
 * */
public class ClientMatch {
    private static ClientMatch clientMatch;
    private Bank bank;
    private User user;
    private ArrayList<Enemy> enemies;
    private GameMap gameMap;
    private Listener listener;
    private ClientManager clientManager;
    private boolean firstSnapshot = true;
    private List<BuildingCard> seaBuildingCard;
    private List<BuildingCard> hillBuildingCards;
    private List<BuildingCard> mountainBuildingCards;
    private ActionContainer actionContainer = new ActionContainer(this);
    private Snapshot lastSnap; // lo useremo per ripristinare lo stato del turno nel caso, ad esempio, di eccezioni.
    //King
    private King king = King.createKing();
    private City cityOfKing;
    //Timer
    private GaugeTimer gaugeTimer;
    private ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
    private ScheduledFuture timerThread = null;

    //Market
    private MarketInterface marketListener;
    private ClientMarket clientMarket = new ClientMarket();
    private boolean firstCanBuy = true;
    private boolean firstCanSell = true;

    String myName;


    public ActionContainer getActionContainer(){
        return actionContainer;
    }
    public void setMySelf(ClientMatch clientMatch){
        this.clientMatch = clientMatch;
    }
    public static ClientMatch getMySelf(){
        return clientMatch;
    }
    public ClientMatch(ClientManager clientManager){
        this.clientManager = clientManager;
    }
    public void addListener (Listener listener){
        this.listener = listener;
    }
    public void addMarketListener(MarketInterface marketListener){
        this.marketListener = marketListener;
    }


    /**Metodo che aggiorna il Model quando riceve uno Snapshot Nuovo e lo notifica all'interfaccia
     * @param snapshot lo snapshot inviato dal Server e deserializzato*/
    public void unpackSnapshot(Snapshot snapshot){
        this.lastSnap = snapshot;
        this.bank = snapshot.getBank();
        this.user = snapshot.getMe();
        this.enemies = snapshot.getEnemies();
        this.gameMap = snapshot.getGameMap();
        this.seaBuildingCard = snapshot.getBuildingCardFromRegionName(RegionName.BEACH.name());
        this.hillBuildingCards = snapshot.getBuildingCardFromRegionName(RegionName.HILL.name());
        this.mountainBuildingCards = snapshot.getBuildingCardFromRegionName(RegionName.MOUNTAIN.name());
        this.cityOfKing = snapshot.getCityOfKing();
        //Se è il primo snapshot chiama il metodo che setta le città sulla mappa.
        if (firstSnapshot){
            setUsername();
            listener.setCitiesOnUI();
            myName=user.getName();
            firstSnapshot = false;
        }
        listener.onReceiveSnapshot(); //Aggiorna l'interfaccia
    }

    /** Routine di aggiornamento del Market, sollecitata dal Server qualora richiesto.
     * @param market la situazione attuale del market lato client*/
    public void updateMarket(ClientMarket market){
        this.clientMarket = market;
        marketListener.updateMarket();
        //Momento in cui vendere
        if (market.canBuy()) {
            if (firstCanSell) {
                if (timerThread != null)
                    timerThread.cancel(true);
                //Orologio del market
                gaugeTimer = new GaugeTimer(listener);
                timerThread = scheduledThreadPool.scheduleWithFixedDelay(gaugeTimer, 0, 1, TimeUnit.SECONDS);
                listener.startMarket();
                firstCanSell=false;
            }
        }
        //Momento in cui comprare
        if (market.canSell()) {
            if (firstCanSell) {
                if (timerThread != null)
                    timerThread.cancel(true);
                gaugeTimer = new GaugeTimer(listener);
                listener.startBuyMarket();
                timerThread = scheduledThreadPool.scheduleWithFixedDelay(gaugeTimer, 0, 1, TimeUnit.SECONDS);
                firstCanSell=false;
            }
        }
        //Fine market
        if (!market.canBuy() && !market.canSell()) {
            if (timerThread!=null)
                timerThread.cancel(true);

            firstCanBuy=true;
            firstCanSell=true;
            listener.endMarket();
        }
    }


    public ClientMarket getClientMarket(){
        return clientMarket;
    }

    public City getCityOfKing(){
        return cityOfKing;
    }

    public List<BuildingCard> getSeaBuildingCard() {
        return seaBuildingCard;
    }

    public List<BuildingCard> getHillBuildingCards() {
        return hillBuildingCards;
    }

    public List<BuildingCard> getMountainBuildingCards() {
        return mountainBuildingCards;
    }

    public Bank getBank() {
        return bank;
    }

    public User getUser() {
        return user;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public GameMap getGameMap() {
        return gameMap;
    }

    public ArrayList<Balcony> getAllBalconies(){
        ArrayList<Balcony> allBalconies = new ArrayList<>();
        for (Region region: gameMap.getRegions()) {
            allBalconies.add(region.getBalcony());
        }
        allBalconies.add(gameMap.getKingGangplank().getKingBalcony());
        return allBalconies;
    }

    /** Ordina al Manager di passare il turno*/
    public void skipTurn(){
        clientManager.skipTurn();}

    /** @param name il nome del nemico
     * @return il vero nemico*/
    public Enemy getEnemyFromName(String name){
        for (Enemy enemy : enemies){
            if (enemy.getUserName().equals(name))
                return enemy;
        }
        return null;
    }


// METODI PER INVIARE LE SINGOLE AZIONI AL SERVER CHE PROVERA' AD ESEGUIRLE

//MAIN ACTIONS

    /** Costruisce l'azione per eleggere un consigliere e la passa al manager per l'invio al server
     * @param councilorColor il colore del consigliere da eleggere
     * @param balconyName il nome del balcone su cui eleggerlo*/
    public void tryElectCouncilor(CouncilorColor councilorColor, String balconyName){
         Balcony trueBalcony = gameMap.getBalconyFromName(balconyName);
        Councilor councilor;
        try {
            councilor = bank.getCouncilor(councilorColor);
            ElectCouncilor action = new ElectCouncilor(councilor, trueBalcony);
            clientManager.sendAction(action);
        } catch (EmptyBankException e) {
            // SE NON HO ABBASTANZA CONSIGLIERI DI QUEL TIPO, NON MANDO NEMMENO AL SERVER MA RIPRISTINO LO STATO
            //DIRETTAMENTE LATO CLIENT.
            clientManager.actionIsNotPossibile(new ActionNotPossibleException(e));
           // e.printStackTrace();
        }
    }
    /** Costruisce l'azione per costruire un emporio e la passa al manager per l'invio al server
     * @param buildingCard la carta permesso che consente la costruzione
     * @param selectedCity la città su cui tentare di costruire*/
    public void tryBuildEmpory(BuildingCard buildingCard, City selectedCity){
        BuildEmpory action = new BuildEmpory(buildingCard , selectedCity);
        clientManager.sendAction(action);
    }

    /** Costruisce l'azione per acquistare una tessera permesso e la passa al manager per l'invio al server
     * @param selectedPoliticCards le carte politiche con cui tentare di soddisfare il consiglio
     * @param balconyToSatisfy il balcone che tenti di soddisfare
     * @param buildingCard la carta che cerco di acquistare*/
    public void tryBuyBuildingCard(ArrayList<PoliticCard> selectedPoliticCards, String balconyToSatisfy, BuildingCard buildingCard){
        Balcony trueBalcony = gameMap.getBalconyFromName(balconyToSatisfy);
        BuyBuildingCard action = new BuyBuildingCard(selectedPoliticCards, trueBalcony.getRegionName(), buildingCard);
        clientManager.sendAction(action);
    }
    /** Costruisce l'azione per costruire un emporio con il re e la passa al manager per l'invio al server
     * @param cards le carte per soddisfare il consiglio del re
     * @param kingCities le città su cui far avanzare il re*/
    public void tryBuildWithKing(ArrayList<PoliticCard> cards, ArrayList<City> kingCities) {
        KingBalcony kingBalcony = gameMap.getKingGangplank().getKingBalcony();
        BuildEmporyWithKing action = new BuildEmporyWithKing(cards, kingBalcony,kingCities);
        clientManager.sendAction(action);
    }

    //FAST ACTIONS
    /** Costruisce l'azione per ingaggiare un aiutante e la passa al manager per l'invio al server*/
    public void tryEngageHelper(){
        EngageHelper action = new EngageHelper();
        clientManager.sendAction(action);
    }
    /** Costruisce l'azione per aggiungere un'azione principale e la passa al manager per l'invio al server*/
    public void tryAnotherMainAction(){
        AnotherMainAction action = new AnotherMainAction();
        clientManager.sendAction(action);
    }

    /** Costruisce l'azione per cambiare le tessere permesso e la passa al manager per l'invio al server
     * @param selectedDeck il mazzo di carte permesso di cui cambiare*/
    public void tryChangeBuildingCards(String selectedDeck){
        ChangeBuildingCards action = new ChangeBuildingCards(selectedDeck);
        clientManager.sendAction(action);
    }
    /** Costruisce l'azione per eleggere con aiutanti e la passa al manager per l'invio al server
     * @param color il colore del consigliere da eleggere
     * @param balconyName il nome del balcone su cui eleggerlo*/
    public void tryElectCouncilorWithHelper(CouncilorColor color, String balconyName){
        Balcony trueBalcony = gameMap.getBalconyFromName(balconyName);
        Councilor councilor;
        try {
            councilor = bank.getCouncilor(color);
            ElectCouncilorWithHelper action = new ElectCouncilorWithHelper(councilor, trueBalcony);
            clientManager.sendAction(action);
        } catch (EmptyBankException e) {
            // SE NON HO ABBASTANZA CONSIGLIERI DI QUEL TIPO, NON MANDO NEMMENO AL SERVER MA RIPRISTINO LO STATO
            //DIRETTAMENTE LATO CLIENT.
            clientManager.actionIsNotPossibile(new ActionNotPossibleException(e));
            // e.printStackTrace();
        }
    }
    //MARKET
    public void sellItem(Buyable buyable,int price){
        if (clientMarket.canSell())
            clientManager.sendItemToMarket(buyable, price);
    }

    public void buyItem(int id){
        if (clientMarket.canBuy())
            clientManager.sendBuyItem(id);
    }



    public void updateUserInTurn(String user){
        //All'inizio il gauge potrebbe essere null: non ha senso stopparlo
        if (gaugeTimer != null && timerThread != null)
            timerThread.cancel(true);

        listener.updateTurn(user);
        //Se lo user in turno sono io, faccio partire il gauge
        if (user.equals(myName)){
            gaugeTimer = new GaugeTimer(listener);
            timerThread = scheduledThreadPool.scheduleWithFixedDelay(gaugeTimer,0,1, TimeUnit.SECONDS);
        }
        //Se stavo facendo un'azione e scade il mio turno, viene annullata
        actionContainer.clearSituation();
        listener.onTimerExceed();
    }

    public void setUsername(){
        listener.setUsernameText(user.getName());
    }


    public Snapshot getLastSnap() {
        return lastSnap;
    }

    @Override
    public String toString() {
        return "ClientMatch{" +
                "bank=" + bank +
                ", user=" + user +
                ", enemies=" + enemies +
                ", gameMap=" + gameMap +
                '}';
    }
    public String print(){
        CommandLineInterface cli = null;
        return cli.ANSI_BLUE+ "Match: " +
                "Situazione Banca: \n " +bank +"\n"+ cli.ANSI_RESET + cli.ANSI_CYAN+
                ", Utente: "+ user.print() + "\n"+ cli.ANSI_RESET + cli.ANSI_BLUE+
                ", I tuoi nemici : " + enemies + "\n" + cli.ANSI_RESET + cli.ANSI_CYAN+
                ", La mappa: " +gameMap.print() +"\n" +
                "Hai a disposizione: "
                +user.getPosGoldPawn() + " oro.";
    }
}
