package Server;

import Game.Game;
import Server.Comunicazione.RMI.RMIServerThread;
import Server.Comunicazione.Socket.SocketServer;

/**
 * Created by Samuele on 11/05/2016.
 */
public class ServerMain {

    /** Istanzia il Game e avvia il Server Socket e il Server RMI*/
    public static void main (String args[]){
        Game game = new Game();

        RMIServerThread RMIServerThread = new RMIServerThread(game);
        RMIServerThread.start();

        SocketServer socketServer = new SocketServer(game);
        socketServer.start(); //classic while(true) Socket Server
    }

}
