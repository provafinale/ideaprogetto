package Server.Comunicazione;

/**
 * Created by edoar on 28/05/2016.
 */
/** Consente di uniformare gli oggetti da passare mediante sockets.
 * E' composto principalmente da un Header e un payload*/
public class Signal {
    public static final String IS_SNAPSHOT = "IS_SNAPSHOT";
    public static final String IS_ACTION ="IS_ACTION";
    public static final String IS_CHOOSE_MAP="IS_CHOOSE_MAP";
    public static final String IS_CHOSEN_MAP="IS_MAP_CHOSEN";
    public static final String USER_CHOOSE = "USER_CHOOSE";
    public static final String SKIP_TURN = "SKIP_TURN";
    public static final String IS_LOGOUT = "IS_LOGOUT";
    public static final String IS_CONNECTION = "IS_CONNECTION"; //inviata da un nuovo Client Socket all'atto della connessione
    public static final String WHO_IS_IN_TURN = "WHO_IS_IN_TURN";
    public static final String BUY_ITEM = "BUY_ITEM";
    public static final String SELL_ITEM = "SELL_ITEM";
    public static final String IS_MARKET = "IS_MARKET";
    public static final String DISCONNECT = "DISCONNECT";


    //public static final String IS_EXCEPTION = "IS_EXCEPTION";
    public static final String IS_ACTION_NOT_POSSIBILE = "IS_ACTION_NOT_POSSIBILE"; //Per evitare lo sbattone della serializzazione dinamica dell'interfaccia


    private String header; //IDENTIFICATIVO
    private String payload; //CONTENUTO INFORMATIVO

    /** Costruisce un segnale con header e payload già serializzato
     * @param header il codice di header
     * @param payload il payload già serializzato*/
    public Signal(String header, String payload) {
        this.header = header;
        this.payload = payload;
    }

    public String getHeader() {
        return header;
    }

    public String getSerializedPayload(){
        return payload;
    }

}
