package Server.Comunicazione.Inteface;

import Client.ClientMarket;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Miscellaneous.Exceptions.OurException;
import Server.Market.Buyable;

import java.rmi.RemoteException;

/**
 * Created by Samuele on 23/05/2016.
 */
/** Metodi in comune tra SocketServer e RMIServer, realizza il disaccoppiamento sugli endpoints di comunicazione*/
public interface ServerGameInterface {
    void sendSnapshot(Snapshot snapshot) throws RemoteException;
    void addUser(String name) throws RemoteException;
    void sendException(OurException ourException) throws RemoteException;
    void youCanChooseMap() throws RemoteException;
    void setMapToLogic(String mapToLogic) throws RemoteException;
    void receiveAction(GenericAction actionToTry) throws RemoteException;
    void sendChosenMapToUser(String map)throws RemoteException;
    void skipTurn() throws RemoteException;
    void sendWhoIsInTurn(String user)throws RemoteException;
    void addElemtentToMarket(Buyable buyable,int price)throws RemoteException;
    void buyElementFromMarket(int id) throws RemoteException;
    void sendMarket(ClientMarket clientMarket) throws RemoteException;
    void disconnect() throws RemoteException;

}
