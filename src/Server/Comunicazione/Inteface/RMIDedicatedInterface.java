package Server.Comunicazione.Inteface;

import Client.Inteface.NetworkInterface;
import Game.Actions.GenericAction;
import Server.Market.Buyable;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Samuele on 11/05/2016.
 */
/** Definisce i metodi invocabili sull'oggetto dedicato al client*/
public interface RMIDedicatedInterface extends Remote {


    void setClient(NetworkInterface client) throws RemoteException;
    void addUser(String name) throws RemoteException;
    void setMapToLogic(String mapToLogic) throws RemoteException;
    void receiveAction(GenericAction actionToTry) throws RemoteException;
    void sendChosenMapToUser(String map)throws RemoteException;
    void skipTurn() throws RemoteException;
    void sendWhoIsInTurn(String user)throws RemoteException;
    void addElemtentToMarket(Buyable buyable, int price)throws RemoteException;
    void buyElementFromMarket(int id) throws RemoteException;
    void disconnect () throws RemoteException;
}
