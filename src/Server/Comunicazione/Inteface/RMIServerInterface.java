package Server.Comunicazione.Inteface;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Samuele on 11/05/2016.
 */
public interface RMIServerInterface extends Remote {
    String connection() throws RemoteException, AlreadyBoundException;
}
