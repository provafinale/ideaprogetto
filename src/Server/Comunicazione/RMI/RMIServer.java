package Server.Comunicazione.RMI;

import Game.*;
import Server.Comunicazione.Inteface.RMIDedicatedInterface;
import Server.Comunicazione.Inteface.RMIServerInterface;
import Server.ServerMatch;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

/**
 * Created by Samuele on 11/05/2016.
 */
public class RMIServer implements RMIServerInterface, Remote {
    private int index;
    private Game game;
    private ServerMatch serverMatch;

    Vector clientReference = new Vector();
    public RMIServer(Game game) {
        this.game = game;
    }

    @Override
    public String connection() throws RemoteException,AlreadyBoundException {
        serverMatch = game.checkAvaibleMatch();

        RMIDedicatedServer newObjRMI = new RMIDedicatedServer(serverMatch);
        clientReference.add(newObjRMI);

        RMIDedicatedInterface stub = (RMIDedicatedInterface) UnicastRemoteObject.exportObject(newObjRMI,0);
        String nameInRegistry = "connessione"+index;
        index++;

        Registry registry = LocateRegistry.getRegistry();
        registry.rebind(nameInRegistry,stub);

        return nameInRegistry;
    }

    public void sendSnapshotAllUser(Snapshot snapshot){

    }



}
