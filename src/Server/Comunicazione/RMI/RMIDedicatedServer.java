package Server.Comunicazione.RMI;

import Client.ClientMarket;
import Client.Inteface.NetworkInterface;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Game.User;
import Miscellaneous.Exceptions.OurException;
import Server.Comunicazione.Inteface.RMIDedicatedInterface;
import Server.Comunicazione.Inteface.ServerGameInterface;
import Server.Market.Buyable;
import Server.ServerMatch;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Samuele on 11/05/2016.
 */
/** Server RMI specifico per utente*/
public class RMIDedicatedServer implements RMIDedicatedInterface, Remote,ServerGameInterface {
    private ServerMatch serverMatch;
    private NetworkInterface client;
    private User user;

    public RMIDedicatedServer(ServerMatch serverMatch) {
        this.serverMatch = serverMatch;
    }

    public void setClient(NetworkInterface client)throws RemoteException{
        this.client=client;
    }

    @Override
    public void addUser(String name) throws RemoteException {
        user = new User(name);
        serverMatch.addUser(user,this);
        System.out.println("Client Connesso RMI: "+name);
    }

    @Override
    public void sendException(OurException ourException) throws RemoteException {
        client.serverException(ourException);
    }


    @Override
    public void sendSnapshot(Snapshot snapshot) throws RemoteException {
        client.setSnapshot(snapshot);
    }

    @Override
    public void sendMarket(ClientMarket clientMarket) throws RemoteException {
        client.updateMarket(clientMarket);
    }

    @Override
    /**Segnale che autorizza lo user a scegliere la mappa*/
    public void youCanChooseMap() throws RemoteException {
        client.chooseMap();
    }

    @Override
    /**Viene chiamato su tutti gli Users, dopo che il random User ha scelto la mappa.*/
    public void setMapToLogic(String mapToLogic) throws RemoteException {
        serverMatch.setMapToGameLogic(mapToLogic);
    }

    @Override
    public void sendChosenMapToUser(String map) throws RemoteException {
        client.setChosenMap(map);
    }

    @Override
    public void skipTurn() throws RemoteException {
        if (user.getIsMyTurn()) {
            synchronized (serverMatch) {
                serverMatch.notify();}
        }
    }

    @Override
    public void sendWhoIsInTurn(String user) throws RemoteException {
        client.setWhoIsInTurn(user);
    }



    @Override
    public void receiveAction(GenericAction actionToTry) throws RemoteException {
        serverMatch.tryAnAction(actionToTry,user);
    }

    @Override
    public void addElemtentToMarket(Buyable buyable, int price) throws RemoteException {
        serverMatch.getServerMarket().addElementToMarket(user,buyable,price);
    }


    @Override
    public void buyElementFromMarket(int id) throws RemoteException {
        serverMatch.getServerMarket().userBuyItem(user,id);
    }

    @Override
    public void disconnect() throws RemoteException {
        user.setConnected(false);
    }


}
