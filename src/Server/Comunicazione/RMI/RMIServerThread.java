package Server.Comunicazione.RMI;

import Game.Game;
import Server.Comunicazione.Inteface.RMIServerInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Samuele on 11/05/2016.
 */
public class RMIServerThread extends Thread  {
    private Game game;

    public RMIServerThread(Game game) {
        this.game=game;
    }

    /** Localizza il registro ottenendo lo stub*/
    public void run(){
        try{
            LocateRegistry.createRegistry(1099);
            RMIServer obj = new RMIServer(game);
            RMIServerInterface stub = (RMIServerInterface) UnicastRemoteObject.exportObject(obj,0);
            Registry registry = LocateRegistry.getRegistry();
            registry.bind("Server",stub);
            System.out.println("Server RMI Ready");
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
}
