package Server.Comunicazione.Socket;

import Game.Game;
import Server.ServerMatch;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Samuele on 11/05/2016.
 */
public class SocketServer extends Thread {
    private Game game;
    private ServerMatch serverMatch;

    public SocketServer (Game game){
        this.game=game;}

    public void run(){

        try {
            ServerSocket serverSocket = new ServerSocket(20000);
            System.out.println("Server Socket Ready");
            while (true){
                Socket socket = serverSocket.accept();
                //Ritorna match disponibile se esistente, oppure nuovo
                serverMatch = game.checkAvaibleMatch();
                //Al socketThread passiamo il riferimento al match a cui è associato.
                SocketThread socketThread = new SocketThread(socket,serverMatch);

                socketThread.start();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
