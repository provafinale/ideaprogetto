package Server.Comunicazione.Socket;

import Client.ClientMarket;
import Game.Actions.GenericAction;
import Game.Snapshot;
import Game.User;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.OurException;
import Miscellaneous.Jsonifier;
import Server.Comunicazione.Inteface.ServerGameInterface;
import Server.Comunicazione.Signal;
import Server.Market.Buyable;
import Server.Market.BuyableObject;
import Server.ServerMatch;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;

/**
 * Created by Samuele on 11/05/2016.
 */
public class SocketThread extends Thread implements ServerGameInterface {
    private Socket mySocket;
    private ServerMatch match;
    private BufferedReader reader;
    private PrintWriter writer;
    private Gson gson = new Gson();
    private User user;

    public SocketThread(Socket mySocket,ServerMatch match){
        this.match=match;
        this.mySocket=mySocket;

    }


    public void run(){
        String readed;

        try {
            reader = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
            writer = new PrintWriter(mySocket.getOutputStream(),true);

            String nickname = reader.readLine();
            addUser(nickname);
            System.out.println("Client Connesso Socket: " + nickname);

            while (true){
                readed = reader.readLine();
                Signal receivedSignal = Jsonifier.deserializeSignal(readed);
                String header = receivedSignal.getHeader();
                String serializedPayload = receivedSignal.getSerializedPayload();
                System.out.println(serializedPayload);
                switch (header){
                    //User sceglie la mappa e la invia
                    case Signal.USER_CHOOSE:{
                        match.setMapToGameLogic(serializedPayload);
                    }break;

                    case Signal.SKIP_TURN:{
                        skipTurn();
                    }break;

                    case Signal.IS_ACTION:{
                        GenericAction genericAction = (GenericAction) Jsonifier.deserializePayload(receivedSignal);
                        receiveAction(genericAction);
                    }break;

                    case Signal.BUY_ITEM:{
                        int idItem = Integer.parseInt(serializedPayload);
                        buyElementFromMarket(idItem);
                    }break;

                    case Signal.SELL_ITEM:{
                        BuyableObject buyableObject = (BuyableObject) Jsonifier.deserializePayload(receivedSignal);
                        Buyable buyable = buyableObject.getObject();
                        int price = buyableObject.getPrice();
                        addElemtentToMarket(buyable,price);
                    }break;

                    case Signal.DISCONNECT: {
                        user.setConnected(false);
                    }break;

                }
            }
        } catch (IOException e) {
            user.setConnected(false);
        }
    }

    @Override
    public void sendSnapshot(Snapshot snapshot) {
        if (user.getisConnected()) {
            String snapToSend = Jsonifier.serialize(snapshot);
            Signal signal = new Signal(Signal.IS_SNAPSHOT, snapToSend);
            String signalToSend = Jsonifier.serialize(signal);
            writer.println(signalToSend);
        }
    }


    @Override
    public void sendMarket(ClientMarket clientMarket) throws RemoteException {
        if (user.getisConnected()) {
            String marketSerialized = Jsonifier.serialize(clientMarket);
            Signal signal = new Signal(Signal.IS_MARKET, marketSerialized);
            String signalToSend = Jsonifier.serialize(signal);
            writer.println(signalToSend);
        }
    }

    @Override
    public void disconnect() throws RemoteException {
        user.setConnected(false);
    }


    @Override
    public void addUser(String name) {
        user = new User(name);
        match.addUser(user,this);
    }

    /** Crea un segnale di errore e lo dispatcha al client.
     * @param ourException*/

    @Override
    public void sendException(OurException ourException) throws RemoteException {
        if (user.getisConnected()) {
            if (ourException.getClass().equals(ActionNotPossibleException.class)) { //Se è una actionNotPossible
                Signal actionNotPossibileSignal = new Signal(Signal.IS_ACTION_NOT_POSSIBILE, Jsonifier.serialize(ourException));
                writer.println(Jsonifier.serialize(actionNotPossibileSignal));
            }
        }
    }

    @Override
    /**Segnale che autorizza lo user a scegliere la mappa*/
    public void youCanChooseMap() throws RemoteException {
        Signal signal = new Signal(Signal.IS_CHOOSE_MAP,"Mha..");
        String signalToSend = Jsonifier.serialize(signal);
        writer.println(signalToSend);
    }

    @Override
    /**Viene chiamato su tutti gli Users, dopo che il random User ha scelto la mappa.*/
    public void setMapToLogic(String mapToLogic) throws RemoteException {
        match.setMapToGameLogic(mapToLogic);
    }

    @Override
    public void receiveAction(GenericAction actionToTry) throws RemoteException {
        match.tryAnAction(actionToTry,user);
    }

    @Override
    public void sendChosenMapToUser(String map) throws RemoteException {
        if (user.getisConnected()) {
            Signal signal = new Signal(Signal.IS_CHOSEN_MAP, map);
            String signalToSend = Jsonifier.serialize(signal);
            writer.println(signalToSend);
        }
    }

    @Override
    public void skipTurn() throws RemoteException {
        if (user.getIsMyTurn()) {
            synchronized (match) {
                match.notify();}
        }
    }

    @Override
    public void sendWhoIsInTurn(String user) throws RemoteException {
        if (this.user.getisConnected()) {
            Signal signal = new Signal(Signal.WHO_IS_IN_TURN, user);
            String signalToSend = Jsonifier.serialize(signal);
            writer.println(signalToSend);
        }
    }

    @Override
    public void addElemtentToMarket(Buyable buyable, int price) throws RemoteException {
        match.getServerMarket().addElementToMarket(user,buyable,price);
    }

    @Override
    public void buyElementFromMarket(int id) throws RemoteException {
        match.getServerMarket().userBuyItem(user,id);
    }


    //thread che serve il client
}
