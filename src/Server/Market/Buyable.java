package Server.Market;

/**
 * Created by Samuele on 28/06/2016.
 */
public interface Buyable {

    Buyable getTypeOfObject();
    String getNameOfObject();
    String getDetails();

}
