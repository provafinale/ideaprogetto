package Server.Market;

import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.GameLogic;
import Game.Stuff.Helper;
import Game.User;
import Miscellaneous.Exceptions.PoorUserException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Samuele on 28/06/2016.
 */
public class ServerMarket {
    private HashMap<User,ArrayList<BuyableObject>> shopWindow = new HashMap<User,ArrayList<BuyableObject>>();
    private GameLogic gameLogic;
    private int sequentialID;
    private boolean canBuy = false;
    private boolean canSell = false;

    public ServerMarket(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
        sequentialID = 0;
    }

    //Crea gli arrayList che ospiteranno gli oggetti in vendita
    public void startMarket(){
        ArrayList<User> users = gameLogic.getUsers();
        for (User user : users){
            shopWindow.put(user,new ArrayList<>());}
    }

    //E' il market da inviare, che toglie il riferimento allo User e lascia solo il suo nome
    public HashMap<String,ArrayList<BuyableObject>> getMarketToSend(){
        HashMap<String,ArrayList<BuyableObject>> marketToSend = new HashMap<String, ArrayList<BuyableObject>>();

        for (Map.Entry<User,ArrayList<BuyableObject>> market : shopWindow.entrySet()){
            String userName = market.getKey().getName();
            ArrayList<BuyableObject> userMarket = market.getValue();
            marketToSend.put(userName,userMarket);
        }
        return marketToSend;
    }


    public void addElementToMarket(User user, Buyable itemToSell,int price){
        if (canSell) {
            BuyableObject item = null;
            switch (itemToSell.getNameOfObject()) {
                case PoliticCard.POLITIC_CARD: {
                    PoliticCard castItem = (PoliticCard) itemToSell;
                    PoliticCard politicCard = gameLogic.removePoliticCards(castItem.getColorPoliticCard(), user);
                    if (politicCard != null)
                        item = new BuyableObject(politicCard, price, user.getName());
                }
                break;

                case BuildingCard.BUILDING_CARD: {
                    BuildingCard castItem = (BuildingCard) itemToSell;
                    BuildingCard buildingCard = user.getAndRemoveBuildingCard(castItem);
                    if (buildingCard != null) {
                        item = new BuyableObject(buildingCard, price, user.getName());
                    }
                }
                break;

                case Helper.HELPER: {
                    if (user.getHelpers()>=1) {
                        user.alterHelpers(-1);
                        Helper helper = (Helper) itemToSell;
                        item = new BuyableObject(helper, price, user.getName());
                    }
                }
                break;
            }
            if (item != null) {
                item.setId(sequentialID);
                shopWindow.get(user).add(item);
                sequentialID++;
            }
        }
        gameLogic.getServerMatch().createAndSendSnapshots();
        gameLogic.getServerMatch().createAndSendMarket();
    }

    public synchronized void userBuyItem(User newUser, int idItem){
        if (canBuy) {

            BuyableObject item = getObjectFromId(idItem);
            User oldUser = getUserFromItemId(item.getId());

            ArrayList<BuyableObject> oldUserShop = shopWindow.get(oldUser);
            //Se l'elemento è contenuto davvero nello shop dell'old user e se il new User ha abbastanza soldi
            if (oldUserShop != null && item.isObjInMarket(oldUserShop) && newUser.getPosGoldPawn() >= item.getPrice()) {
                try {
                    gameLogic.alterGolds(-item.getPrice(), newUser);
                    gameLogic.alterGolds(item.getPrice(), oldUser);
                    addPurchase(newUser, item.getObject());

                    ArrayList<BuyableObject> updateOldUserShop = new ArrayList<>();
                    for (BuyableObject buyableObject : oldUserShop){
                        if (buyableObject.getId()!=idItem)
                            updateOldUserShop.add(buyableObject);
                    }
                    shopWindow.put(oldUser, updateOldUserShop);
                } catch (PoorUserException e) {
                    e.printStackTrace();
                }
            }

        }
        gameLogic.getServerMatch().createAndSendSnapshots();
        gameLogic.getServerMatch().createAndSendMarket();
    }


    public ArrayList<BuyableObject> getElementFromMarket(User user){
        return shopWindow.get(user);
    }
    public void removeElementFromMarket(User user, Buyable sold){
        shopWindow.get(user).remove(sold);
    }


    //Dato un id dell'oggetto nel market, restiutiusce l'oggetto con quell'id
    private BuyableObject getObjectFromId(int id){
        ArrayList<User> users = gameLogic.getUsers();
        for (User user : users){
            ArrayList<BuyableObject> buyableObjects = shopWindow.get(user);
            for (BuyableObject buyableItem : buyableObjects) {
                if (buyableItem.getId() == id)
                    return buyableItem;
            }
        }
        return null;
    }

    //Dato un id dell'oggetto nel market, ritorna il proprietario.
    private User getUserFromItemId(int id){
        ArrayList<User> users = gameLogic.getUsers();
        for (User searchUser : users){
            ArrayList<BuyableObject> buyableObjects = shopWindow.get(searchUser);

            for (BuyableObject buyableObject : buyableObjects){
                if (buyableObject.getId() == id){
                    return searchUser;}}
        }
        return null;
    }

    private void addPurchase(User user, Buyable buyable){
        switch (buyable.getNameOfObject()){
            case PoliticCard.POLITIC_CARD:{
                user.addPoliticCard((PoliticCard) buyable);}
                break;
            case BuildingCard.BUILDING_CARD:{
                gameLogic.takeBuildingCardForClient(user, (BuildingCard) buyable);}
                break;
            case Helper.HELPER:{
                user.alterHelpers(1);}
                break;
        }
    }

    public boolean isCanBuy() {
        return canBuy;
    }

    public void setCanBuy(boolean canBuy) {
        this.canBuy = canBuy;
    }

    public boolean isCanSell() {
        return canSell;
    }

    public void setCanSell(boolean canSell) {
        this.canSell = canSell;
    }


}
