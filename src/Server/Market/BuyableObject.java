package Server.Market;

import javafx.beans.value.ObservableValue;
import javafx.beans.value.ObservableValueBase;
import javafx.collections.FXCollections;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Samuele on 28/06/2016.
 */
public class BuyableObject implements Serializable {
    private int id;
    private Buyable buyable;
    private int price;
    private boolean isSetted = false;
    private String userName;
    private String objectName;

    public BuyableObject(Buyable buyable, int price, String userName) {
        this.buyable = buyable;
        this.price = price;
        this.userName = userName;
        objectName = buyable.getNameOfObject();
    }

    public Buyable getObject() {
        return buyable;
    }

    public int getPrice() {
        return price;
    }

    public int getId(){
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getDetails(){
        return buyable.getDetails();
    }


    public void setId(int id){
        if (!isSetted) {
            this.id = id;
            isSetted=true;
        }
    }

    public boolean isObjInMarket(ArrayList<BuyableObject> buyableObjects){
        for (BuyableObject buyableObject : buyableObjects){
            if (buyableObject.getId()==this.getId())
                return true;
        }
        return false;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuyableObject that = (BuyableObject) o;

        if (id != that.id) return true;

        return false;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (buyable != null ? buyable.hashCode() : 0);
        result = 31 * result + price;
        result = 31 * result + (isSetted ? 1 : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (objectName != null ? objectName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BuyableObject{" +
                "id=" + id +
                ", buyable=" + buyable +
                ", price=" + price +
                ", isSetted=" + isSetted +
                ", userName='" + userName + '\'' +
                ", objectName='" + objectName + '\'' +
                '}';
    }
}
