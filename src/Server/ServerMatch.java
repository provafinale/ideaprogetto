package Server;

import Client.ClientMarket;
import Game.Actions.GenericAction;
import Game.*;
import Game.Cards.BuildingCard;
import Game.Cards.PoliticCard;
import Game.Places.*;
import Miscellaneous.Constant;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.EmptyBankException;
import Server.Comunicazione.Inteface.ServerGameInterface;
import Server.Market.ServerMarket;

import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by edoar on 13/05/2016.
 */

/** Gestisce la partita*/
public class ServerMatch implements Runnable {
    private HashMap<User,ServerGameInterface> userConnection = new HashMap<>(); //Mappa fra User e tipo di connessione
    private boolean isOpened = true;
    private GameLogic gameLogic = new GameLogic(this);
    private ServerMarket serverMarket = new ServerMarket(gameLogic);
    private GameMap gameMap;
    private Bank bank;
    int i=0;
    private ExecutorService executor = Executors.newCachedThreadPool();
    private boolean statusMatch = true;
    public GameLogic getGameLogic(){
        return gameLogic;
    }

    /**Constructor */
    public ServerMatch(){
        System.out.println("Creato nuovo Match.");
    }

    @Override
    public void run() {
        System.out.println("Start Thread ServerMatch.");
        setUserPawsAndBank();
        gameLogic.equipUsers();
        ArrayList<User> users = gameLogic.getUsers();
        serverMarket.startMarket();

        while (statusMatch){
            for (int i = 0; i < users.size(); i++){
                System.out.println("Turno di: "+users.get(i).getName());
                resetCityTree(); //Riporta l'albero di visita allo stato vergine.
                PoliticCard politicCardForUserInTurn = gameLogic.getPoliticCard();
                users.get(i).addPoliticCard(politicCardForUserInTurn);
                users.get(i).setNumMainAction(1);
                users.get(i).setNumFastAction(1);
                users.get(i).setIsMyTurn(true);
                gameLogic.setBuildingCardsToTable();

                createAndSendSnapshots();
                sendWhoIsInTurn(users.get(i).getName());

                synchronized (this){
                    try {wait(Constant.DURATION_TURN);}
                    catch (InterruptedException e) {e.printStackTrace();}
                }
                users.get(i).setIsMyTurn(false);
                users.get(i).setNumFastAction(0);
                users.get(i).setNumMainAction(0);

                if (i == users.size()-1) {
                    System.out.println("Start Market");
                    usersCanSell();
                    usersCanBuy();
                    System.out.println("Fine Market");

                }
            }
        }
    }

    public void sendWhoIsInTurn(String userInTurn){

        for (Entry<User,ServerGameInterface> user : userConnection.entrySet()) {
            User thisUser = user.getKey();
            if (thisUser.getisConnected()) {
                ServerGameInterface server = user.getValue();
                try {
                    server.sendWhoIsInTurn(userInTurn);
                } catch (RemoteException e) {
                    thisUser.setConnected(false);
                }
            }
        }
    }

    /**Funzione chiamata alla scadenza del Timer.*/
    public void startMatch(){
        isOpened = false;
        userCanChooseMap(); //Invia segnale "Scelta Mappa" ad un user scelto casualmente.
    }

    /**Setta la mappa alla Logica. Viene chiamata o dal ServerRMI o dal ServerSocket quando lo user ha scelto.*/
    public void setMapToGameLogic(String map){
        setConfigurationMap(map);
        executor.execute(this); //Dopo che la mappa è stata impostata, parte il thread di gioco.
        System.out.println("Mappa Scelta: "+map);
        //Setta la mappa a tutti gli user
        setMapToAllUsers(map);
    }

    /** Comunica a tutti i clients qual è la mappa scelta, inviandogliela
     * @param  map  la mappa scelta */
    private void setMapToAllUsers(String map){
        for (Entry<User,ServerGameInterface> user : userConnection.entrySet()) {
            User thisUser = user.getKey();
            if (thisUser.getisConnected()) {
                ServerGameInterface server = user.getValue();
                try {
                    server.sendChosenMapToUser(map);
                } catch (RemoteException e) {
                    thisUser.setConnected(false);
                }
            }
        }
    }



    /**Invia l'autorizzazione ad uno User a caso di poter scegliere la mappa*/
    private void userCanChooseMap(){
        ServerGameInterface randomUserForMap = randomUser();
        try {
            randomUserForMap.youCanChooseMap();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**Sceglie uno User a caso dalla Hashmap, che si occuperà di scegliere la mappa*/
    private ServerGameInterface randomUser(){
        ServerGameInterface server = null;
        int sizeUsers = userConnection.size();
        int random = new Random().nextInt(sizeUsers);
        int i = 0;
        for (Entry<User,ServerGameInterface> entry : userConnection.entrySet()){
            if (i==random)
                return entry.getValue();
            else
                i++;
        }
        return server;
    }

    /**Assegna 3 pedine ad ogni Utente connesso.*/
    public void setUserPawsAndBank(){
        for (Entry<User,ServerGameInterface> userInMap : userConnection.entrySet()) {
            User user = userInMap.getKey();
            gameLogic.setUserPawns(user);
            gameLogic.setUserBank(user);
        }
    }



    /**Setter*/
    public void setBank(Bank bank){
        this.bank=bank;
    }

    public void setGameMap(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    /**CHIAMATA AI METODI PER CONFIGURARE LA MAPPA ||| map deve essere del tipo: map1,map2...*/
    private void setConfigurationMap(String map){
        setCityColor(map);
        setNearCitiesForAll(map);}


        /**SET COLORI DA FILE DI CONFIGURAZIONE*/
    private void setCityColor(String map){
        GameMap gameMap = gameLogic.getGameMap();
        InputStream readFile = null;
        Properties readConf = new Properties();
        ArrayList<Region> regions= gameLogic.getGameMap().getRegions();
        try {
            String pathToFile = "maps/config/color/"+map+".properties";
            readFile = new FileInputStream(pathToFile);
            readConf.load(readFile);

            for (CitiesName cityName : CitiesName.values()) {
                String name = cityName.name();
                String cityColor = readConf.getProperty(name);

                City cityToSet = getCityFromName(name);
                CityColor colorToSet = CityColor.getCityColorFromString(cityColor);

                cityToSet.setColor(colorToSet);
            }
        }catch (Exception e) {
            System.out.println(e);
        }
    }


        /**SET CITTA' VICINE*/
    private void setNearCitiesForAll(String map){
        GameMap gameMap = gameLogic.getGameMap();
        InputStream readFile = null;
        Properties readConf = new Properties();
        ArrayList<Region> regions= gameLogic.getGameMap().getRegions();
        try {
            String pathToFile = "maps/config/link/"+map+".properties";
            readFile = new FileInputStream(pathToFile);
            readConf.load(readFile);

            for (CitiesName cityName : CitiesName.values()) {
                String name = cityName.name();
                String[] nearCities = readConf.getProperty(name).split(",");

                setNearCities(name,nearCities);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        /**Imposta le città vicine ad una città*/
    private void setNearCities(String cityName, String[] nearCities){
        City city = getCityFromName(cityName); //città a cui settare le vicine
        for (int i = 0; i < nearCities.length; i++){
            City nearCity = getCityFromName(nearCities[i]);
            city.addNearCity(nearCity);
        }
    }


    /**Dalle varie Regioni, ritorna il riferimento alla Città con nome cityName*/
    public City getCityFromName(String cityName){
        City city=null;
        ArrayList<Region> regions = gameLogic.getGameMap().getRegions();

            for (Region region : regions){
                city = region.getCityFromName(cityName);

                if (city!=null) {
                    return city;
                }
            }
        return city;
    }



    /**Aggiunge Riferimento User-Connessione, e nella logica del gioco.*/
    public void addUser(User user,ServerGameInterface server){

        for (User gameUser : gameLogic.getUsers()) {
            if (user.getName().equals(gameUser.getName())) {
                user.setUserName(user.getName() + i);
                i++;
            }
        }
        userConnection.put(user,server);
        gameLogic.addUser(user);

        //Se gli utenti connessi sono almeno 2, parte il timer della partita.
        if (userConnection.size()==2) {
            System.out.println("Timer Partito.");
            Timer timer = new Timer();
            timer.schedule(new GameTimer(this), Constant.TIMER_START_MATCH);
        }
    }



    public boolean isOpened() {
        return isOpened;
    }

    public int numPlayersOnline(){
        return userConnection.size();}

    public void inizializzaPartita(){
        //Distribuisco carte agli user (POPOLO USER).
    }



    public void usersCanSell(){
        System.out.println("Si può vendere");
        serverMarket.setCanSell(true);
        createAndSendMarket();
        synchronized (this) {
            try {
                wait(Constant.MARKET_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        serverMarket.setCanSell(false);
        createAndSendMarket();
    }

    public void usersCanBuy(){
        System.out.println("Si può comprare");
        serverMarket.setCanBuy(true);
        createAndSendMarket();
        synchronized (this) {
            try {
                wait(Constant.MARKET_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        serverMarket.setCanBuy(false);
        createAndSendMarket();
    }

    public void createAndSendMarket(){
        ArrayList<User> users = gameLogic.getUsers();
        ClientMarket clientMarket = new ClientMarket(serverMarket.getMarketToSend(),serverMarket.isCanBuy(),serverMarket.isCanSell());
        for (User user : users) {

            if (user.getisConnected()) {
                ServerGameInterface server = userConnection.get(user);
                try {
                    server.sendMarket(clientMarket);
                } catch (RemoteException e) {
                    user.setConnected(false);}
            }
        }
    }


    /**Snapshot**/
    //Invoca il metodo che crea lo snapshot per ogni utente.
    public void createAndSendSnapshots(){
        Snapshot snapshot;
        User user;
        for (Entry<User,ServerGameInterface> userInMap : userConnection.entrySet()) {
            user = userInMap.getKey();
            snapshot = generatePersonalizedSnapshot(user);

            if (user.getisConnected()) {
                ServerGameInterface server = userConnection.get(user);
                try {
                    server.sendSnapshot(snapshot);
                } catch (RemoteException e) {
                    user.setConnected(false);}
            }
        }
    }

    //Passato un User, popola l'array dei suoi nemici e ritorna il suo Snapshot.
    private Snapshot generatePersonalizedSnapshot(User userForSnapshot){
        ArrayList<Enemy> personalizedEniemes = new ArrayList<>();
        User user;

        HashMap<String,List<BuildingCard>> buildingCardsOnTable = new HashMap<>();
        buildingCardsOnTable.put(RegionName.BEACH.name(),gameLogic.getBuildingCardsFromRegion(RegionName.BEACH.name()));
        buildingCardsOnTable.put(RegionName.HILL.name(),gameLogic.getBuildingCardsFromRegion(RegionName.HILL.name()));
        buildingCardsOnTable.put(RegionName.MOUNTAIN.name(),gameLogic.getBuildingCardsFromRegion(RegionName.MOUNTAIN.name()));

        for (Entry<User,ServerGameInterface> userInMap: userConnection.entrySet()) { //Scorro tutti gli utenti
            user = userInMap.getKey();
            if (!user.equals(userForSnapshot)) {
                // Get Information of User's Enemy:
                String userName = user.getName();
                int numPoliticCards = user.getNumberOfPoliticCards();
                int numHelpers = user.getHelpers();
                ArrayList<BuildingCard> buildingCards = user.getBuildingCards();
                int posVictoryPawn = user.getPosVictoryPawn();
                int posNobilityPawn = user.getPosNobilityPawn();
                int posGoldPawn = user.getPosGoldPawn();

                Enemy singleEnemy = new Enemy(userName,numPoliticCards,numHelpers,buildingCards,posVictoryPawn,posNobilityPawn,posGoldPawn);
                personalizedEniemes.add(singleEnemy); //Aggiungo l'oggetto Enemy ad una lista di nemici.
            }
        }
        //Dopo aver ottenuto tutte le informazioni sui miei nemici, genero lo snapshot.
        return new Snapshot(userForSnapshot,personalizedEniemes, bank, gameMap,buildingCardsOnTable,gameLogic.getPosKing());
    }
    /** TENTA DI ESEGUIRE L'AZIONE CREATA SUL CLIENT, PER LO USER. CATCHA LE ECCEZIONI NESTATE
     * E NEL CASO GENERA UNA ACTIONNOTPOSSIBLEEXCEPTION DA INVIARE AL CLIENT.*/

    public void tryAnAction(GenericAction action,User user){
        System.out.println("ServerMatch trying to exec "+action +" for user "+ user.getName() + "...");
        try {
            if (user.getIsMyTurn()) {
                action.executeAction(gameLogic, user);
                System.out.println("Azione eseguita da" +user.getName());
            }
            else {
                System.out.println("non è il turno di " + user.getName());
                ServerGameInterface network = userConnection.get(user);

                try {
                    network.sendException(new ActionNotPossibleException("Non è il tuo turno!"));
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        } catch (ActionNotPossibleException e) { //contiene la sottoeccezione specifica oppure il solo msg.
            ServerGameInterface communication = userConnection.get(user);
            try {
                System.out.println("Sto inviando l'eccezione "+e +"ai clients");
                communication.sendException(e);
            } catch (RemoteException e1) {
                e1.printStackTrace();}
        } catch (EmptyBankException e) {
            e.printStackTrace();
        }

    }

    private void resetCityTree(){
        for (Region region: gameMap.getRegions()
             ) {
            for (City city: region.getCities().values()
                 ) {
                city.setVisited(false);
            }
        }
    }

    public ServerMarket getServerMarket(){
        return serverMarket;
    }
}
