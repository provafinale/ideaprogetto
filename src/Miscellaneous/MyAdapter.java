package Miscellaneous;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by edoar on 01/06/2016.
 */
public final class MyAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {

    //THANKS TO devmanu.xyz and Gson DOC

    @Override
    public T deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject myWrapper = (JsonObject) jsonElement;
        final JsonElement typeName = get(myWrapper, "type");
        final JsonElement data = get(myWrapper, "data");
        final Type actualType = typeForName(typeName);
        return context.deserialize(data, actualType);

    }


    @Override
    public JsonElement serialize(T object, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject myWrapper = new JsonObject();
        myWrapper.addProperty("type", object.getClass().getName());
        myWrapper.add("data", context.serialize(object));
        return myWrapper;
    }


    private Type typeForName(final JsonElement typeElem) {
        try {
            return Class.forName(typeElem.getAsString());
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }

    //RETURN JSON OBJECT'S MEMBER
    private JsonElement get(final JsonObject wrapper, String memberName) {
        final JsonElement elem = wrapper.get(memberName);
        if (elem == null) throw new JsonParseException("no '" + memberName + "' member found.");
        return elem;
    }
}
