package Miscellaneous.Exceptions;

import Game.Places.Tracks.GoldTrack;

/**
 * Created by edoar on 28/05/2016.
 */
public class MoveNotPossibleException extends OurException {
    public MoveNotPossibleException(String msg, String nick){
        this.userNickname = nick;
      if (msg.equalsIgnoreCase(GoldTrack.class.getName())){ //Se è stato generato da GoldTrack
          this.msg = nick+ " non ha abbastanza Monete.";
      }
    }
}
