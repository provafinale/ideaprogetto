package Miscellaneous.Exceptions;

/**
 * Created by edoar on 30/05/2016.
 */

/** Se ci sono già empori di quel colore*/
public class EmporyException extends OurException {
    public EmporyException(String userName, String cityName){
        this.userNickname = userName;
        this.msg = ("L'utente "+userName + "ha già un emporio del suo colore in" + cityName);
    }
    public EmporyException(String userName, String cityName, String msg){
        this.userNickname = userName;
        this.msg = msg;
    }
}
