package Miscellaneous.Exceptions;

/**
 * Created by Samuele on 25/05/2016.
 */
public class EmptyBankException extends OurException {
    public EmptyBankException(String message, String userName){
        this.userNickname = userName;
        this.msg = message;
    }
    public EmptyBankException(String msg){
        this.msg = msg;
    }
}
