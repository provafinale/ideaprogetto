package Miscellaneous.Exceptions;

/**
 * Created by Samuele on 22/06/2016.
 */
public class BuildingDeckIsEmptyException extends Exception {
    public BuildingDeckIsEmptyException(String message) {
        super(message);
    }
}
