package Miscellaneous.Exceptions;

/**
 * Created by edoar on 20/05/2016.
 */

/** Segnala un'azione non possibile*/
public class ActionNotPossibleException extends OurException {
    private OurException singleSubException;
    private boolean isSubbed = false;

    /** Costruisce l'eccezione incapsulando una sottoeccezione. Utile nelle azioni complesse che prevedono multipli punti di fallimento
     * @param subException la sottoeccezione incapsulata*/
    public ActionNotPossibleException(OurException subException){
        isSubbed = true;
        this.singleSubException = subException;
    }

    /** Costruisce un'eccezione semplice con solamente un messaggio
     * @param  msg il messaggio*/
    public ActionNotPossibleException(String msg){
        isSubbed = false;
        this.msg = msg;
    }

    public OurException getSingleSubException() {
        return singleSubException;
    }

    public boolean isSubbed() {
        return isSubbed;
    }

    @Override
    public String toString() {
        return "ActionNotPossibleException{" +
                "singleSubException=" + singleSubException +
                ", isSubbed=" + isSubbed +
                "msg: "+msg +
                '}';
    }
}



