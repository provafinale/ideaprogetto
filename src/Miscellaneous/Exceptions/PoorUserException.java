package Miscellaneous.Exceptions;

/**
 * Created by edoar on 28/05/2016.
 */
/** L'eccezione generata quando l'utente non ha abbastanza monete*/
public class PoorUserException extends OurException {
    OurException subException;
    /** Incapsula una sottoeccezione */
    public PoorUserException(String nick, OurException subException){
        this.userNickname = nick;
        this.subException = subException;
    }
    public PoorUserException(String nick, String msg){
        this.userNickname = nick;
        this.msg = nick + msg;
    }
}
