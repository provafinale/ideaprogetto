package Miscellaneous.Exceptions;

import java.io.Serializable;

/**
 * Created by edoar on 09/06/2016.
 */
/** La nostra eccezione. contiene messaggio e nickname dell'utente a cui fa riferimento*/
public abstract class OurException extends Exception implements Serializable{
      String msg;
      String userNickname;

    protected OurException() {
    }
    public String getMsg(){
        return msg;
    }
    public String getNick(){
        return userNickname;
    }
}