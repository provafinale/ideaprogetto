package Miscellaneous.Exceptions;

/**
 * Created by edoar on 20/06/2016.
 */
public class NoMoreActionsException extends OurException {
    public NoMoreActionsException(String nick) {
        this.userNickname = nick;
        this.msg = nick+ " , non hai più azioni disponibili.";
    }
}
