package Miscellaneous;

import Game.Recompenses.Rewards.Reward;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by edobe on 23/05/2016.
 */
/** Utility class, consente di ritornare comodamente quantità random utili al business*/
public class Randomizer {

    public static ArrayList<Reward> getRandomCityRewards(){

        return null;
    }

        /** Ritorna numero random di rewards per riempire carte e città*/
    public static int randomNumberOFRewards(){
        Random random = new Random();
        int min = 1;
        int num = ((Constant.MAX_CITY_BONUS-min)+1);
        int result = random.nextInt(num) + 1;
        return result;
    }

    /** Lancia dado e torna un numero che rappresenta il tipo di reward*/
    public static int getRandomTypeOfReward(){
        Random random = new Random();
        int number = random.nextInt(6);
        return  (number);
    }

    public static int getRandomSingleReward(int min, int max){
        Random random = new Random();
        int num =(max-min) +1;
        return random.nextInt(num) +1;
    }

    public static int randomNumberOfNobilityRewards(){
        Random random = new Random();
        int min = 0;
        int num = ((Constant.MAX_NOBILITY_BONUS-1)+1);
        int result = random.nextInt(num) + 1;
        return result;
    }


    public static int getRandom(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max+1);

    }
}
