package Miscellaneous;

import Client.ClientMarket;
import Game.Actions.GenericAction;
import Game.Places.Balcony.Balcony;
import Game.Recompenses.Rewards.Reward;
import Game.Snapshot;
import Miscellaneous.Exceptions.ActionNotPossibleException;
import Miscellaneous.Exceptions.OurException;
import Server.Comunicazione.Signal;
import Server.Market.Buyable;
import Server.Market.BuyableObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by edoar on 31/05/2016.
 */
/** Classe funzionale che consente di serializzare e deserializzare comodamente i Segnali per le comunicazioni socket*/
public class Jsonifier  {

    public  static Gson gson = new Gson(); //istanzio un Gson by Google

    public Jsonifier() {
    }

    /** serializza nei casi semplici*/
    public static String serialize(Object object){
        return gson.toJson(object);
    }

    /** Serializza un clientMarket istruendo il typeAdapter
     * @param clientMarket il market da serializzare*/
    public static String serialize(ClientMarket clientMarket){
        Gson marketGson = new GsonBuilder().registerTypeAdapter(Buyable.class,new MyAdapter<Buyable>())
                .registerTypeAdapter(Reward.class,new MyAdapter<Reward>())
                .create();
        return marketGson.toJson(clientMarket);
    }

    /** Serializza l'oggetto comprabile istruendo il typeAdapter
     * @param buyableObject l'oggetto comprabile*/
    public static String serialize(BuyableObject buyableObject){
        Gson buyableGson = new GsonBuilder().registerTypeAdapter(Buyable.class,new MyAdapter<Buyable>())
                .registerTypeAdapter(Reward.class,new MyAdapter<Reward>())
                .create();
        return buyableGson.toJson(buyableObject);
    }

    /** Serializza lo snapshot istruendo il typeAdapter
     * @param  snapshot lo snapshot da serializzare*/
    public static String serialize(Snapshot snapshot){
        Gson actionGson = new GsonBuilder().registerTypeAdapter(Reward.class, new MyAdapter<Reward>())
                .registerTypeAdapter(Balcony.class,new MyAdapter<Balcony>())
                .create();
        return actionGson.toJson(snapshot, Snapshot.class);
    }

    /** *Serializzatore delle nostre custom Exception
     * @param exc l'eccezione da serializzare*/
    public static String serialize(OurException exc){
        Gson exceptionGson = new GsonBuilder().registerTypeAdapter(OurException.class, new MyAdapter<OurException>()).create();
        return exceptionGson.toJson(exc, OurException.class);
    }
    /** Deserializza il segnale
     * @param json il segnale completamente serializzato il Json */
    public static Signal deserializeSignal(String json){
        return gson.fromJson(json, Signal.class);

        /** Serializza l'azione generica istruendo il typeAdapter su tutte le possibile classi astratte e interfacce
         * @param action l'azione da serializzare*/
    }
    public static String serialize(GenericAction action){
        Gson actionGson = new GsonBuilder().registerTypeAdapter(GenericAction.class, new MyAdapter<GenericAction>())
                .registerTypeAdapter(Reward.class,new MyAdapter<Reward>())
                .registerTypeAdapter(Balcony.class,new MyAdapter<Balcony>())
                .create();
        String json = actionGson.toJson(action, GenericAction.class);
        return json;
    }

    /** Deserializza il payload del segnale.
     * @param signal il segnale di cui deserializzare il payload in modo differenziato
     * in base all'header*/
    public static Object deserializePayload(Signal signal){
        String header = signal.getHeader();
        String payload = signal.getSerializedPayload();
        switch (header){
            case Signal.IS_SNAPSHOT : {
               // return gson.fromJson(payload, Snapshot.class);
               Gson gson = new GsonBuilder().registerTypeAdapter(Reward.class, new MyAdapter<Reward>())
                       .registerTypeAdapter(Balcony.class,new MyAdapter<Balcony>())
                       .create();
               // Reward reward = gson.fromJson()
                return gson.fromJson(payload, Snapshot.class);

            }
            case Signal.SELL_ITEM:{
                Gson sellGson = new GsonBuilder().registerTypeAdapter(Buyable.class,new MyAdapter<Buyable>())
                        .registerTypeAdapter(Reward.class, new MyAdapter<Reward>())
                        .create();
                return sellGson.fromJson(payload,BuyableObject.class);
            }
            case Signal.IS_MARKET:{
                Gson marketGson = new GsonBuilder().registerTypeAdapter(Buyable.class,new MyAdapter<Buyable>())
                        .registerTypeAdapter(Reward.class,new MyAdapter<Reward>())
                        .create();

                return marketGson.fromJson(payload,ClientMarket.class);
            }
            case Signal.IS_ACTION : {
                Gson actionGson = new GsonBuilder().registerTypeAdapter(GenericAction.class, new MyAdapter<GenericAction>())
                        .registerTypeAdapter(Reward.class,new MyAdapter<Reward>())
                        .registerTypeAdapter(Balcony.class,new MyAdapter<Balcony>())
                        .create();
                GenericAction genericAction = actionGson.fromJson(payload, GenericAction.class);
                System.out.println("Deserialized: "+genericAction + "type: "+genericAction.getClass());
                return genericAction;
            }
            case Signal.IS_ACTION_NOT_POSSIBILE : {
                return gson.fromJson(payload, ActionNotPossibleException.class);
                /*
                Gson gson = new GsonBuilder().registerTypeAdapter(OurException.class, new MyAdapter<OurException>()).create();
                OurException ourException = gson.fromJson(payload, OurException.class);
                System.out.println("Deserialized "+ ourException + "type: "+ourException.getClass());
                return ourException;
                */
            }
        default: return null;}
    }

}
