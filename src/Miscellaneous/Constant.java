package Miscellaneous;

import java.io.Serializable;

/**
 * Created by edoar on 16/05/2016.
 */
/** Le costanti utili*/
public class Constant implements Serializable {
    public static final int numPoliticCards = 90;
    public static final int numPlayersMatch = 4;
    public static final int START_GOLD_TRACK_POS = 10;

    public static final int NUM_HELPERS = 39;
    public static final int START_NUM_POLITIC_CARDS = 6;

    public static final int DURATION_TURN = 60000; //ms
    public static final int DURATION_TURN_MIN = DURATION_TURN/1000;

    public static final int MARKET_TIME = 20000; //ms in cui si può comprare e vendere
    public static final int MARKET_TIME_MIN = MARKET_TIME/1000;

    /**ACTIONS */
    public static final String MAIN_ACTION = "MAIN_ACTION";
    public static final String FAST_ACTION = "FAST_ACTION";
    public static final int MAIN_ACTION_ALLOWED = 1;
    public static final int FAST_ACTION_ALLOWED = 1;
    public static final int GOLD_EARNED_FOR_MAIN_ELECTION = 4;

    public static final String END_TURN = "END_TURN";

    /**BONUS*/
    public static final int MAX_CITY_BONUS = 2;
    public static final int MAX_NOBILITY_REWARDS = 2;
    public static final int MIN_HELPERS_REWARD = 1;
    public static final int MAX_HELPERS_REWARD = 3;
    public static final int MIN_NOBILITY_BONUS = 1;
    public static final int MAX_NOBILITY_BONUS = 2;
    public static final int MIN_COINS_BONUS = 1;
    public static final int MAX_COINS_BONUS = 3;
    public static final int MIN_VICTORY_BONUS = 1;
    public static final int MAX_VICTORY_BONUS = 5;
    public static final int MIN_POLITIC_BONUS = 1;
    public static final int MAX_POLITIC_BONUS = 3;
    public static final int BONUS_REGION_VALUE = 5;
    public static final int BLUE_CITY_REWARD = 5;
    public static final int ORANGE_CITY_REWARD = 8;
    public static final int GREY_CITY_REWARD = 12;
    public static final int YELLOW_CITY_REWARD = 20;
    //25,18,7,12,3
    public static final int FIRST_KING_BONUS_CARD = 25;
    public static final int SECOND_KING_BONUS_CARD = 18;
    public static final int THIRD__KING_BONUS_CARD = 12;
    public static final int FOURTH__KING_BONUS_CARD = 7;
    public static final int FIFTH__KING_BONUS_CARD = 3;


    /**PLACES*/
    public static final int NUM_REGION = 3;
    public static final int NUM_CITY_IN_REGION = 5;
    public static final int TOT_NUM_CITY=15;
    public static final int NUM_COUNCILORS = 4;


    /**TIMER*/
    //Quando ci sono almeno 2 giocatori Online, la partita inizia dopo questo tempo.
    public static final int TIMER_START_MATCH = 9000;

    /** GUI */
    public static final double START_WIDTH = 1800;
    public static final double START_HEIGHT = 968;

    public static final String CLI = "CLI";
    public static final String GUI = "GUI";
    public static final String RMI = "RMI";
    public static final String SOCKET = "Socket";
   // public static final HashMap<Integer, Reward> rewardsMap = new HashMap<>()
}
